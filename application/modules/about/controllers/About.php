<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class About extends MX_Controller {

     private $_grpContent = 'about';

    public function __construct() {
        parent::__construct();
       $this->load->model('front/front_m');

    }

    public function index() {
        Modules::run('track/front','');
        $this->load->module('front');
        $data['about_act'] = 'current';
        $data['contentView'] = 'about/index';
        $data['pageHeader'] = 'เกี่ยวกับเรา';
        
        //echo CI_VERSION ; exit();
        $input['type']=$this->_grpContent;
        $input['variable']="detail";
        $data['info']=$this->front_m->getConfig($input)->row();
        //arr($data['info']->value);exit();
        $share['ogTitle']="";
        $share['ogDescription']="";
        $share['ogUrl']= 'about';
        $share['ogImage']= config_item('metaOgImage');
        $this->_social_share($share);
        
        $this->front->layout($data);
    }

    

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  

}

        <section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>เกี่ยวกับเรา</h1>
				<span>ฮอนด้าพรีเมียร์แพรกษา</span>
				<ol class="breadcrumb">
					<li><a href="<?php echo site_url(); ?>">หน้าแรก</a></li>
					<li class="active">เกี่ยวกับเรา ฮอนด้าพรีเมียร์แพรกษา</li>
				</ol>
			</div>
		</section>

		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_full" style="padding-bottom:30px;">
						<?php echo isset($info->value)? html_entity_decode($info->value) : "";?>
					</div>
				</div>
			</div>
		</section>
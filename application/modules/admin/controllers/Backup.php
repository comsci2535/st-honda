<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Backup extends MX_Controller {

    private $_title = "สำรองข้อมูล";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับสำรองข้อมูล";
    private $_grpContent = "backup";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("backup_m");
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $export = array(
            'excel' => site_url("admin/{$this->router->class}/excel"),
            'pdf' => site_url("admin/{$this->router->class}/pdf"),
        );
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
//        $action[1][] = action_custom(site_url("admin/{$this->router->class}/schedule"), 'btn-success', 'datetime', 'ตั้งเวลา', 'fa-clock-o');
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;
        $info = $this->backup_m->get_rows($input);
        $infoCount = $this->backup_m->get_count($input);
        $column = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->repoId);
            $action = array();
            $action[1][] = action_custom(site_url("admin/{$this->router->class}"), 'btn-info', 'restore', '', 'fa-recycle');
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title = "รายการสำรองประจำวันที่";
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() {
        $this->load->module('admin/admin');

        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post();

        $value = $this->_build_data($input);
        $result = $this->backup_m->insert($value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['repoId'] = $id;
        $info = $this->backup_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post();
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->backup_m->update($id, $value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        
        $value['title'] = $input['title'];
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        if ( $input['mode'] == 'create' ) {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    public function excel(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->backup_m->get_rows($input);
        $fileName = "backup";
        $sheetName = "Sheet name";
        $sheetTitle = "Sheet title";
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'createDate',            
            'D' => 'updateDate',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'createDate' || $field == 'updateDate' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf(){
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info = $this->backup_m->get_rows($input);
        $data['info'] = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font' => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "backup.pdf";
        $pathFile = "uploads/pdf/backup/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->backup_m->get_rows($input);
        $infoCount = $this->backup_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->repoId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->backup_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->backup_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->backup_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->backup_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  
    
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MX_Controller {

    private $_title = "แบนเนอร์";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับป้ายโฆษณาหน้าหลัก";
    private $_grpContent = "banner";
    private $_permission;
    private $_type;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("banner_m");
        $this->_type = [
            //'' => 'เลือกรายการ',
            'hero' => 'ป้ายโฆษณาหน้าหลัก',
            //'service' => 'บริการ',
            //'social' => 'Social link',
        ];

      $this->load->model("upload_m");
      $this->load->library('image_moo');
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[1][] = action_add(site_url("admin/{$this->router->class}/create"));
        $action[2][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[2][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $input['type'] = 'hero';
        $info = $this->banner_m->get_rows($input);
        $infoCount = $this->banner_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->bannerId);

            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $dateRang ="ไม่จำกัดเวลา";
            if($rs->startDate!="0000-00-00" && $rs->endDate!="0000-00-00"){
                 $dateRang = date('d-m-Y', strtotime($rs->startDate)).' ถึง '.date('d-m-Y', strtotime($rs->endDate));
            }
            $page="";
            if($rs->page=="home"){
                $page="หน้าหลักเว็บไซต์";
            }
            if($rs->page=="course"){
                $page="หน้าคอร์สเรียน";
            }
            if($rs->page=="activity"){
                $page="หน้าสอนสด";
            }
           
            $column[$key]['DT_RowId'] = $rs->bannerId;
            $column[$key]['dragDrop'] = '<label style="font-size:16px;width:10px" class="handle"><i class="fa fa-arrows-v text-muted"></i></label>';
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'><input type='hidden' value='{$rs->order}' name='order'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['order'] = $rs->order;
            $column[$key]['page'] = $page;
            $column[$key]['type'] = $dateRang;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

     public function update_order(){
        $input = $this->input->post(null, true);
        //arr($input); exit();
        $this->db->update_batch('banner', $input['sortOrder'], 'bannerId');
    }
    
    public function create() {
        $this->load->module('admin/admin');
        
        $info = new stdClass;
        $info->rtl = 0;
        $info->loop = 1;
        $info->priority = 2;
        $data['info'] = $info;
        $data['ddType'] = $this->_type;

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post(null, true);
        $images=$this->upload_ci('input-file-preview');
        $input['coverImageId']=$images['insertId'];

        // $images2=$this->upload_ci2('input-file-preview-2');
        // $input['contentImageId']=$images2['insertId'];

        $value = $this->_build_data($input);
        $id = $this->banner_m->insert($value);
        if ( $id ) {
            $value = $this->_build_upload_content($id, $input);
            Modules::run('admin/upload/update_content', $value);
            Modules::run('admin/utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['bannerId'] = $id;
        $info = $this->banner_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect(site_url("admin/{$this->router->class}"));
        }
        $info = $info->row();
        $data['info'] = $info;
        
        $data['coverImage'] = Modules::run('admin/upload/get_upload_tmpl2', $info->bannerId, $this->_grpContent, 'coverImage');
        $data['contentImage'] = Modules::run('admin/upload/get_upload_tmpl2', $info->bannerId, $this->_grpContent, 'contentImage');
        
       //arr($data['bannerImage']);
       //arrx($bannerImage->result());
        if($info->startDate!="0000-00-00" && $info->endDate!="0000-00-00"){
                 $data['dateRang'] = date('d-m-Y', strtotime($info->startDate)).' ถึง '.date('d-m-Y', strtotime($info->endDate));
        }
         
         $data['startDate'] = $info->startDate;
         $data['endDate'] = $info->endDate;
               
       
        
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
            $images=$this->upload_ci('input-file-preview');
            $input['coverImageId']=$images['insertId'];
        }
        if(isset($_FILES['input-file-preview-2'])&&$_FILES['input-file-preview-2']['tmp_name']){
            $images2=$this->upload_ci2('input-file-preview-2');
            $input['contentImageId']=$images2['insertId'];
        }
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->banner_m->update($id, $value);
        if ( $result ) {
            if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name'] || isset($_FILES['input-file-preview-2'])&&$_FILES['input-file-preview-2']['tmp_name']){
                $value = $this->_build_upload_content($id, $input);
                Modules::run('admin/upload/update_content', $value);
            }
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        $value['title'] = $input['title'];
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        $value['link'] = $input['link'];
        $value['type'] = 'hero';
        $value['page'] =  $input['page'];
        $value['startDate'] = $input['startDate'];        
        $value['endDate'] = $input['endDate'];  
        if ( $input['mode'] == 'create' ) {
            $value['order'] = $this->banner_m->get_latest_order()+1;
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    
    
    private function _build_upload_content($id, $input) {
        //   print"<pre>";print_r($id);exit();
        $value = array();
        $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent
            );
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle']
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle']
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key1 => $rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key1]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key1 => $rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key1]
                );
            }
        }
        return $value;
    }   
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $input['type'] = 'hero';
        $info = $this->banner_m->get_rows($input);
        $infoCount = $this->banner_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->bannerId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['type'] = isset($this->_type[$rs->type]) ? $this->_type[$rs->type]: null;
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->banner_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->banner_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->banner_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->banner_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function upload_ci($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 
        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $this->_grpContent,
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
            $this->resizeImage($result['file_name'],'1336','500');
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

    public function upload_ci2($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 
        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $this->_grpContent,
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
             $this->resizeImage($result['file_name'],'730','487');
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

    public function resizeImage($filename,$width,$height){
      //   print_r($filename);exit();
      $source_path = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/' . $filename;
      //$target_path =  'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/thumbnail/';
      $config_manip = array(
          'image_library' => 'GD2',
          'quality'=> '90%',
          'source_image' => $source_path,
          //'new_image' => $target_path,
          'create_thumb' => FALSE,
          'maintain_ratio' => FALSE,
          //'thumb_marker' => '_thumb',
          'width' => $width,
          'height' => $height
      );


      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }


      $this->image_lib->clear();
   }   
    
    private function _get_config(){

        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
        $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|image/png';
        $config['resizeOverwrite'] = true;
        $config['quality']= '90%';
        $config['resize'] = true;
        $config['width'] = 1336;
        $config['height'] = 300;
    
        
        return $config;
    } 
    
}

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {
    
    private $_title = 'หมวดหมู่';
    private $_pageExcerpt = 'การจัดการหมวดหมู่';
    private $_grpContent = 'category';
    private $_treeDropDown= array('' => 'เลือกรายการ', '0' => 'ไม่กำหนด');
    private $_treeData;
    private $_orderData = array();
    
    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->_treeData = new stdClass();
        $this->load->model("category_m");
        $this->load->library('ckeditor');

        $this->load->model("upload_m");
        $this->load->library('image_moo');
    }
    
    public function type($categoryType="")
    {
        $this->load->module('admin/admin');
        
        $data['categoryType'] = $categoryType;
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}/type/{$categoryType}"));
        //$action[1][] = action_filter();
        $action[1][] = action_add(base_url("admin/{$this->router->class}/create/{$categoryType}"));
        //$action[2][] = action_order(base_url("admin/{$this->router->class}/order/{$categoryType}"));
        $action[2][] = action_trash_multi("admin/{$this->router->class}/action/trash/".$categoryType);
        $action[2][] = action_trash_view(base_url("admin/{$this->router->class}/trash/".$categoryType));

        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        
        if ( $categoryType == "article"){
        $data['breadcrumb'][] = array("บทความ", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        }
        if ($categoryType == "products"){
        $data['breadcrumb'][] = array("สินค้า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        }


        // page detail
        if ( $categoryType == "article") {
            $this->_pageExcerpt = $this->_pageExcerpt."บทความ";
        }
        if ( $categoryType == "products") {
            $this->_pageExcerpt = $this->_pageExcerpt."สินค้า";
        }
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/{$categoryType}/index";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/{$categoryType}/index.js";

        $this->admin->layout($data);
    }
    
    public function data_index() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->category_m->get_rows($input);
        $infoCount = $this->category_m->get_count($input);
        $tree = $this->_build_tree($info);
        $treeData = $this->_print_tree($tree);
        $column = array();
        $key = 0;
        foreach ($treeData as $rs) {
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rs->name, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }
            if ( $found ) {
                $id = encode_id($rs->info->categoryId);
                $action = array();
                $action[] = table_edit(site_url("admin/{$this->router->class}/edit/{$input['categoryType']}/{$id}"));
                $action[] = table_trash("admin/{$this->router->class}/action/trash");
                $active = $rs->info->active ? "checked" : null;
                $column[$key]['DT_RowId'] = $rs->info->categoryId;
                $column[$key]['dragDrop'] = '<label style="font-size:16px;width:10px" class="handle"><i class="fa fa-arrows-v text-muted"></i></label>';
                $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'><input type='hidden' value='{$rs->info->order}' name='order'>";
                $column[$key]['order'] = $rs->info->order;
                $column[$key]['name'] = $rs->name;
                $column[$key]['excerpt'] = $rs->info->excerpt;
                $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
                $column[$key]['createDate'] = datetime_table($rs->info->createDate);
                $column[$key]['updateDate'] = datetime_table($rs->info->updateDate);
                $column[$key]['action'] = implode($action," ");
                $key++;
            }
        }        
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function update_order_index(){
        $input = $this->input->post(null, true);
        //arr($input); exit();
        $this->db->update_batch('category', $input['sortOrder'], 'categoryId');
    }

    public function trash($categoryType="")
    {
        $this->load->module('admin/admin');
        
        $data['categoryType'] = $categoryType;
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}/type/{$categoryType}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        
        if ( $categoryType == "article"){
        $data['breadcrumb'][] = array("บทความ", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/type/{$categoryType}"));
        $data['breadcrumb'][] = array('รายการถังขยะ', base_url("admin/{$this->router->class}/create"));
        }

        if ($categoryType == "products"){
        $data['breadcrumb'][] = array("สินค้า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/type/{$categoryType}"));
        $data['breadcrumb'][] = array('รายการถังขยะ', base_url("admin/{$this->router->class}/create"));
        }


        // page detail
        if ( $categoryType == "article") {
            $this->_pageExcerpt = $this->_pageExcerpt."บทความ";
        }
        if ( $categoryType == "products") {
            $this->_pageExcerpt = $this->_pageExcerpt."สินค้า";
        }
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/{$categoryType}/trash";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/{$categoryType}/trash.js";

        $this->admin->layout($data);
    }
    
    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->category_m->get_rows($input);
        $infoCount = $this->category_m->get_count($input);
        $tree = $this->_build_tree($info);
        $treeData = $this->_print_tree($tree);
        $column = array();
        $key = 0;
        foreach ($treeData as $rs) {
            if ( isset($input['search']['value']) && $input['search']['value'] != "" ) {
                if ( mb_stripos($rs->name, $input['search']['value']) !== false ) {
                    $found = true;
                } else {
                    $found = false;
                }
            } else {
                $found = true;
            }
            if ( $found ) {
                $id = encode_id($rs->info->categoryId);

                $action = array();
              
                $action[] = table_restore("admin/{$this->router->class}/action/restore");
                
                $active = $rs->info->active ? "checked" : null;
                $column[$key]['DT_RowId'] = $id;
                $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
                $column[$key]['order'] = $rs->info->order;
                $column[$key]['name'] = $rs->name;
                $column[$key]['excerpt'] = $rs->info->excerpt;
                
                $column[$key]['recycleDate'] = datetime_table($rs->info->recycleDate);
                $column[$key]['action'] = implode($action," ");
                $key++;
            }
        }        
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    
    public function create($categoryType="") {
        $this->load->module('admin/admin');
        
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        $data['categoryType'] = $categoryType;
        $data['categoryDropDown'] = $this->category_drop_drown(true,$categoryType);
        
        // breadcrumb
        if ( $categoryType == "article"){
            $data['breadcrumb'][] = array("บทความ", "javascript:void(0)");
            $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/type/{$categoryType}"));
            $data['breadcrumb'][] = array('สร้าง', base_url("admin/{$this->router->class}/create"));
         }
        if ($categoryType == "products"){
            $data['breadcrumb'][] = array("สินค้า", "javascript:void(0)");
            $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/type/{$categoryType}"));
            $data['breadcrumb'][] = array('สร้าง', base_url("admin/{$this->router->class}/create"));
           
         }

        // page detail
        if ( $categoryType == "article") {
            $this->_pageExcerpt = $this->_pageExcerpt."บทความ";
        }
        if ( $categoryType == "products") {
            $this->_pageExcerpt = $this->_pageExcerpt."สินค้า";
        }
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/{$categoryType}/form";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/{$categoryType}/form.js";
        
        $this->admin->layout($data);
    }
    public function edit($categoryType="",$id) {
        $this->load->module('admin/admin');
        $id = decode_id($id);
        $info = $this->category_m->get_by_id($id);
        $data['info'] = $info;
        //print_r($info); exit();
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        $data['categoryType'] = $categoryType;
        $data['categoryDropDown'] = $this->category_drop_drown(true,$categoryType);
        
        // breadcrumb
       
        
        if ( $categoryType == "article"){
            $data['breadcrumb'][] = array("บทความ", "javascript:void(0)");
            $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/type/{$categoryType}"));
            $data['breadcrumb'][] = array('แก้ไข', base_url("admin/{$this->router->class}/create"));
         }
        if ($categoryType == "products"){
            $data['breadcrumb'][] = array("สินค้า", "javascript:void(0)");
            $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/type/{$categoryType}"));
            $data['breadcrumb'][] = array('แก้ไข', base_url("admin/{$this->router->class}/create"));
           
         }

        // page detail
        if ( $categoryType == "article") {
            $this->_pageExcerpt = $this->_pageExcerpt."บทความ";
        }
        if ( $categoryType == "products") {
            $this->_pageExcerpt = $this->_pageExcerpt."สินค้า";
        }

         $data['coverImage'] = Modules::run('admin/upload/get_upload_tmpl2', $id, $this->_grpContent, 'coverImage');

        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/{$categoryType}/form";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/{$categoryType}/form.js";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post();
        $images=$this->upload_ci('input-file-preview');
        $input['coverImageId']=$images['insertId'];

        $value = $this->_build_data($input);
        $insertId = $this->category_m->insert($value);
        if ( $insertId ) {
            $input['categoryId'] = $insertId;
            $value = $this->_build_data_lang($input);
            $this->category_m->insert_lang($value);
            $value = array();

             $value = $this->_build_upload_content($insertId, $input);
            Modules::run('admin/upload/update_content', $value);
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/type/{$input['categoryType']}"));
    }

     public function update() {

        $input = $this->input->post();

        if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
            $images=$this->upload_ci('input-file-preview');
            $input['coverImageId']=$images['insertId'];
        }

        $value = $this->_build_data($input);
       // $input['id'] = decode_id($input['id']);
        //print_r($input); exit();
        $result = $this->category_m->update($input['id'],$value);
         if ( $result ) {
            $input['categoryId'] = $input['id'];
            $value = $this->_build_data_lang($input);
            $this->category_m->update_lang($input['categoryId'],$value);
            $value = array();

            if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
                $value = $this->_build_upload_content($input['id'], $input);
                Modules::run('admin/upload/update_content', $value);
            }
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/type/{$input['categoryType']}"));
    }
    
    private function _build_data($input) {
        
        $value['type'] = $input['categoryType'];
        $value['parentId'] = $input['parentId'];
        if ( $input['mode'] == 'create' ) {
            $value['order'] = $this->category_m->get_latest_order()+1;
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_data_lang($input) {
        
        $value['categoryId'] = $input['categoryId'];
        $value['name'] = str_replace(","," ",$input['name']); //$input['name'];
        $value['excerpt'] = isset ($input['excerpt']) ? $input['excerpt'] : NULL;
        $value['content'] = isset ($input['content']) ? $input['content'] : NULL;
        $value['lang'] = config_item('language_abbr');
        $value['nameLink'] = $this->clean($input['name']);

        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = str_replace(","," ",$input['name']);
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['excerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = str_replace(","," ",$input['name']);
        }

        return $value;
    }
    function clean($string) {
       
      // $string = preg_replace("/[^a-zก-ฮ\d]/i", '-', $string);
       $string = str_replace(' ', '-', $string);
       //$string = str_replace('-', ' ', $string);
       $string = str_replace('&', '', $string);
       $string = str_replace('+', '', $string);
       $string = str_replace(':', '', $string);
       $string = str_replace('!', '', $string);
       $string = str_replace('(', '', $string);
       $string = str_replace(')', '', $string);
       $string = str_replace('.', '', $string);
       $string = str_replace('[', '', $string);
       $string = str_replace(']', '', $string);
       $string = str_replace('/', '', $string);
       $string = str_replace('#', '', $string);
       $string = str_replace('@', '', $string);
       $string = str_replace('?', '', $string);
       $string = str_replace(',', '', $string); // Replaces all spaces with hyphens.
       $string = str_replace('%', '', $string); // Removes special chars.
       return strip_tags($string ,'-' ); // Replaces multiple hyphens with single one.
    }

    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle'],
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle'],
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key=>$rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key]
                );
            }
        }
        return $value;
    }   
    
    public function order($categoryType="") {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}/order/{$categoryType}"));
        $action[1][] = action_list_view(base_url("admin/{$this->router->class}/type/{$categoryType}"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        $input['recycle'] = 0;
        $input['order'][0]['column'] = 1;
        $input['order'][0]['dir'] = 'asc';
        $input['categoryType'] = $categoryType;
        $info = $this->category_m->get_rows($input);
//        arrx($info->result());
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->categoryId];
            $thisref['id'] = $rs->categoryId;
            $thisref['content'] = $rs->name;
            if ($rs->parentId != 0) {
                $refs[$rs->parentId]['children'][] = &$thisref;
            } else {
                $treeData[] = &$thisref;
            }
        }
//        arr($treeData);
        $data['treeData'] = $treeData;
        $data['categoryType'] = $categoryType;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update_order");
        
        // breadcrumb
        if ( $categoryType == "article")
            $data['breadcrumb'][] = array("บทความ", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}/type/{$categoryType}"));
        $data['breadcrumb'][] = array('จัดลำดับ', base_url("admin/{$this->router->class}/order"));
        
        // page detail
        if ( $categoryType == "article") {
            $this->_pageExcerpt = $this->_pageExcerpt."บทความ";
        }
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/{$categoryType}/order";
        $data['pageScript'] = "assets/scripts/admin/{$this->router->class}/{$categoryType}/order.js";

        $this->admin->layout($data);
    }
    
    public function update_order(){
        $input = $this->input->post();
        $order = json_decode($input['order']);
        $value = $this->_build_data_order($order);
        $result = $this->db->update_batch('category', $value, 'categoryId');
        if ( $result ) {
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}/type/{$input['categoryType']}"));
    }
    
    private function _build_data_order($info, $parentId=0){
        $order = 0;
        foreach ($info as $rs) {
            $order++;
            $this->_orderData[$rs->id]['categoryId'] = $rs->id; 
            $this->_orderData[$rs->id]['order'] = $order;
            $this->_orderData[$rs->id]['parentId'] = $parentId;
            if ( isset($rs->children) )
                $this->_build_data_order ($rs->children, $rs->id);
        }
        return $this->_orderData;
    }
    
    public function action($type=""){
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ($input['id'] as &$rs) $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->category_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->category_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 1;
                $value['recycle'] = 0;
                $result = $this->category_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->category_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
    public function category_drop_drown($active=true,$categoryType) {
        $input['recycle'] = 0;
        if ( $active ) 
            $input['active'] = 1;
        
        $input['categoryType'] = $categoryType;
        $info = $this->category_m->get_rows($input);
        $tree = $this->_build_tree($info);
        $dropDown = $this->_print_drop_down($tree);
        return $dropDown;
    }
    
    private function _build_tree($info, $parentId=0) {
        $tree = new stdClass();
        foreach ($info->result() as $d) {
            if ($d->parentId == $parentId) {
                $children = $this->_build_tree($info, $d->categoryId);
                if ( !empty($children) ) {
                    $d->children = $children;
                }
                $tree->{$d->categoryId} = $d;
            }
        }
        return $tree;
    }
    
    private function _print_tree($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parentId == 0) ? '' : $d . ' &#8594; ';
            $this->_treeData->{$t->categoryId} = new stdClass();
            $this->_treeData->{$t->categoryId}->name = $dash . $t->name;
            $this->_treeData->{$t->categoryId}->info = $t;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_tree($t->children, $level, $r + 1, $t->parentId, $dash . $t->name);
                }
            }
        }
        return $this->_treeData;
    }
    
    private function _print_drop_down($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parentId == 0) ? '' : $d . ' &#8594; ';
            $this->_treeDropDown[$t->categoryId] = $dash . $t->name;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_drop_down($t->children, $level, $r + 1, $t->parentId, $dash . $t->name);
                }
            }
        }
        return $this->_treeDropDown;
    } 

    public function checkTitle()
    {
        $input = $this->input->post();
        $input['recycle'] = array(0,1);
        $input['categoryType'] = $input['categoryType'];
        $info = $this->category_m->get_rows($input);
       // arr($info); exit();
        if ( $info->num_rows() > 0 ) {
            if ($input['mode'] == 'create') {
                $rs = FALSE;
            } else {
                $row = $info->row();
                if ($row->categoryId == decode_id($input['id'])) {
                    $rs = TRUE;
                } else {
                    $rs = FALSE;
                }
            }
        } else {
            $rs =  TRUE;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($rs));
    }    

    public function upload_ci($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 
        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $this->_grpContent,
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
            // $this->resizeImage($result['file_name'],'730','487');
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

   

    public function resizeImage($filename,$width,$height){
      //   print_r($filename);exit();
      $source_path = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/' . $filename;
      //$target_path =  'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/thumbnail/';
      $config_manip = array(
          'image_library' => 'GD2',
          'quality'=> '90%',
          'source_image' => $source_path,
          //'new_image' => $target_path,
          'create_thumb' => FALSE,
          'maintain_ratio' => FALSE,
          //'thumb_marker' => '_thumb',
          'width' => $width,
          'height' => $height
      );


      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }


      $this->image_lib->clear();
   } 
    
    private function _get_config(){

        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
        $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|image/png|svg';
        $config['resizeOverwrite'] = true;
        $config['quality']= '90%';
        $config['resize'] = true;
        $config['width'] = 730;
        $config['height'] = 487;
    
        
        return $config;
    } 


}

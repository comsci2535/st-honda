<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class News extends MX_Controller {

    private $_title = "ข่าวสาร";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับข่าวสาร";
    private $_grpContent = "news";
    private $_requiredExport = true;
    private $_permission;

     private $_treeDropDown= array('' => 'เลือกรายการ', '0' => 'ไม่กำหนด');

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("news_m");
        $this->load->model("category_m");
        $this->load->model("tags_m");
        $this->load->model("config_m");
        $this->load->model("upload_m");
        $this->load->library('image_moo');
    }
    
    public function index() {
        $this->load->module('admin/admin');

        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}"));
        $action[1][] = action_filter();
        $action[2][] = action_add(base_url("admin/{$this->router->class}/create"));
        $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[3][] = action_trash_view(base_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        $data['categoryDropDown'] = $this->category_drop_drown(true,$this->_grpContent);
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";

        $this->admin->layout($data);
    }

    public function data_index() {
        $input = $this->input->post();
        parse_str($_POST['frmFilter'], $frmFilter);
        if ( !empty($frmFilter) ) {
            foreach ( $frmFilter as $key => $rs )
                $input[$key] = $rs;
        }
        $input['recycle'] = 0;
        $input['grpContent'] = $this->_grpContent;

        //print_r($input); exit();
        $info = $this->news_m->get_rows($input);
       // print_r($info);exit();
        $infoCount = $this->news_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->contentId);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
             $column[$key]['title'] = $rs->title;//.' <font color="red">(อ่าน '.number_format($rs->view).')</font>';
            $column[$key]['view'] = number_format($rs->view);
            //$column[$key]['category'] = $rs->name;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function create() {
        $this->load->module('admin/admin');

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        $data['categoryDropDown'] = $this->category_drop_drown(true,$this->_grpContent);

        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));

        /****Tags******/
        $param_t['tagsType']=$this->_grpContent;
        $param_t['tagsActive']=1;
        $param_t['tagsRecycle']=0;
        $tags=$this->tags_m->get_rows($param_t)->result();
        
        if(!empty($tags)){
            foreach($tags as $index=>$tages_data_){
              $JSON_arr[]=array(
                      'id'=>$tages_data_->tagsID,
                      'text'=>$tages_data_->tagsName
              );    
            }
        }else{
            $JSON_arr='';
        }
         
        $data['JSON_arr']=json_encode($JSON_arr);
        $data['JSON_arrEdit']=json_encode('');
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";

        $this->admin->layout($data);
    }

    public function save() {
        $input = $this->input->post();
        $images=$this->upload_ci('input-file-preview');
        $input['coverImageId']=$images['insertId'];
        
        $value = $this->_build_data($input);
        $id = $this->news_m->insert($value);
        if ( $id ) {
            $value = $this->_build_upload_content($id, $input);
            Modules::run('admin/upload/update_content', $value);
            Modules::run('admin/utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }

    private function _build_data($input) {
        $value['recommend'] = isset($input['recommend']) ? $input['recommend'] : 0;
        $value['title'] = str_replace(","," ",$input['title']);
        $value['categoryId'] = $input['categoryId'];
        $value['excerpt'] = $input['excerpt'];
        $value['detail'] = $input['detail'];
        $value['grpContent'] = $this->_grpContent;

        $value['title_link'] = $this->clean($input['title']);
       
        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
             $value['metaTitle'] = str_replace(","," ",$input['title']);
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = $input['excerpt'];
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
             $value['metaKeyword'] = str_replace(","," ",$input['title']);
        }
        

        $contentTags=$input['tagsId'][0];
        $array_text=[];
        if($contentTags!=''){
            $arr_tags=explode(',',$contentTags);
            foreach($arr_tags as $arr_tags_){
              if(is_numeric($arr_tags_)) {
                  $array_text[]=$arr_tags_;
              }else{
                $datainsertTags=array(
                 "tagsName"=>$this->clean($arr_tags_),
                 "tagsType"=>$this->_grpContent,
                  'tagsActive'=>1,
                  'tagsDateCreate'=>date('Y-m-d H:i:s'),
                  'tagsAuthor'=>$this->session->user['userId'],
                  'tagsEditor'=>''
                );
                $idNewTage=$this->tags_m->insert($datainsertTags);
                
                $array_text[]=$idNewTage;
              }
              
            }
             $newtagsdata=implode(',',$array_text);
             $value['tagsId']=$newtagsdata;
        }else{
             $value['tagsId']='';
        }
        if ($input['mode'] == 'create') {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }

    function clean($string) {
      // $string = preg_replace("/[^a-zก-ฮ\d]/i", '-', $string);
       $string = str_replace(' ', '-', $string);
       //$string = str_replace('-', ' ', $string);
       $string = str_replace('&', '', $string);
       $string = str_replace('+', '', $string);
       $string = str_replace(':', '', $string);
       $string = str_replace('!', '', $string);
       $string = str_replace('(', '', $string);
       $string = str_replace(')', '', $string);
       $string = str_replace('.', '', $string);
       $string = str_replace('[', '', $string);
       $string = str_replace(']', '', $string);
       $string = str_replace('/', '', $string);
       $string = str_replace('#', '', $string);
       $string = str_replace('@', '', $string);
       $string = str_replace('?', '', $string);
       $string = str_replace(',', '', $string); // Replaces all spaces with hyphens.
       $string = str_replace('%', '', $string); // Removes special chars.
       return strip_tags($string ,'-' ); // Replaces multiple hyphens with single one.
    }
    
    private function _build_upload_content($id, $input) {
        //   print"<pre>";print_r($id);exit();
        $value = array();
        $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent
            );
        if ( isset($input['coverImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'coverImage',
                'uploadId' => $input['coverImageId'],
                'title' => $input['coverImageTitle']
            );
        if ( isset($input['contentImageId']) )
            $value[] = array(
                'contentId' => $id,
                'grpContent' => $this->_grpContent,
                'grpType' => 'contentImage',
                'uploadId' => $input['contentImageId'],
                'title' => $input['contentImageTitle']
            );
        if ( isset($input['galleryImageId']) ) {
            foreach ( $input['galleryImageId'] as $key1 => $rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'galleryImage',
                    'uploadId' => $rs,
                    'title' => $input['galleryImageTitle'][$key1]
                );
            }
        }
        if ( isset($input['docAttachId']) ) {
            foreach ( $input['docAttachId'] as $key1 => $rs ) {
                $value[] = array(
                    'contentId' => $id,
                    'grpContent' => $this->_grpContent,
                    'grpType' => 'docAttach',
                    'uploadId' => $rs,
                    'title' => $input['docAttachTitle'][$key1]
                );
            }
        }
        return $value;
    }

    public function edit($id = 0) {
        $this->load->module('admin/admin');

        $id = decode_id($id);
        $input['contentId'] = $id;
        $input['recycle'] = 0;
        $input['categoryId'] = '';
        $input['grpContent'] = $this->_grpContent;

        $info = $this->news_m->get_rows($input);
        if ($info->num_rows() == 0) {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'ขอภัยไม่พบข้อมูลที่ต้องการแก้ไข');
            redirect($this->agent->referrer());
        }
        $info = $info->row();

         /****Tags******/
        $param_t['tagsType']=$this->_grpContent;
        $param_t['tagsActive']=1;
        $param_t['tagsRecycle']=0;
        $tags=$this->tags_m->get_rows($param_t)->result();
        
        if(!empty($tags)){
            foreach($tags as $index=>$tages_data_){
              $JSON_arr[]=array(
                      'id'=>$tages_data_->tagsID,
                      'text'=>$tages_data_->tagsName
              );    
            }
        }else{
            $JSON_arr='';
        }
         
        $data['JSON_arr']=json_encode($JSON_arr);
        $data['JSON_arrEdit']=json_encode($info->tagsId);

        $data['categoryDropDown'] = $this->category_drop_drown(true,$this->_grpContent);

        $data['coverImage'] = Modules::run('admin/upload/get_upload_tmpl2', $info->contentId, $this->_grpContent, 'coverImage');
       //  $data['contentImage'] = Modules::run('admin/upload/get_upload_tmpl', $info->contentId, $this->_grpContent, 'contentImage');
       //  $data['galleryImage'] = Modules::run('admin/upload/get_upload_tmpl', $info->contentId, $this->_grpContent, 'galleryImage');
       //  $data['docAttach'] = Modules::run('admin/upload/get_upload_tmpl', $info->contentId, $this->_grpContent, 'docAttach');
        
        $data['info'] = $info;
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");

        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));

        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";

        $this->admin->layout($data);
    }

    public function update() {
        $input = $this->input->post(null, true);

        if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
            $images=$this->upload_ci('input-file-preview');
            $input['coverImageId']=$images['insertId'];
        }

        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->news_m->update($id, $value);

        if ( $result ) {
            if(isset($_FILES['input-file-preview'])&&$_FILES['input-file-preview']['tmp_name']){
                $value = $this->_build_upload_content($id, $input);
                Modules::run('admin/upload/update_content', $value);
            }
            Modules::run('admin/utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function trash() {
        $this->load->module('admin/admin');   
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $input['categoryId'] = '';
        $input['grpContent'] = $this->_grpContent;
        $info = $this->news_m->get_rows($input);
        $infoCount = $this->news_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->contentId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['excerpt'] = $rs->excerpt;
            $column[$key]['category'] = "";
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->news_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->news_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->news_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->news_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  


    public function category_drop_drown($active=true,$categoryType) {
        $input['recycle'] = 0;
        if ( $active ) 
            $input['active'] = 1;
        
        $input['categoryType'] = $categoryType;
        $info = $this->category_m->get_rows($input);
        $tree = $this->_build_tree($info);
        $dropDown = $this->_print_drop_down($tree);
        return $dropDown;
    }
    
    private function _build_tree($info, $parentId=0) {
        $tree = new stdClass();
        foreach ($info->result() as $d) {
            if ($d->parentId == $parentId) {
                $children = $this->_build_tree($info, $d->categoryId);
                if ( !empty($children) ) {
                    $d->children = $children;
                }
                $tree->{$d->categoryId} = $d;
            }
        }
        return $tree;
    }
    
    private function _print_tree($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parentId == 0) ? '' : $d . ' &#8594; ';
            $this->_treeData->{$t->categoryId} = new stdClass();
            $this->_treeData->{$t->categoryId}->name = $dash . $t->name;
            $this->_treeData->{$t->categoryId}->info = $t;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_tree($t->children, $level, $r + 1, $t->parentId, $dash . $t->name);
                }
            }
        }
        return $this->_treeData;
    }

    private function _print_drop_down($tree, $level = 5, $r = 1, $p = null, $d = NULL) {
        foreach ( $tree as $t ) {
            $dash = ($t->parentId == 0) ? '' : $d . ' &#8594; ';
            $this->_treeDropDown[$t->categoryId] = $dash . $t->name;
            if ( isset($t->children) ) {
                if ( $r < $level ) {
                    $this->_print_drop_down($t->children, $level, $r + 1, $t->parentId, $dash . $t->name);
                }
            }
        }
        return $this->_treeDropDown;
    }  

    public function upload_ci($inputFileName="userfile")  {
        $config = $this->_get_config();
        create_dir($config['upload_path']);
        $this->upload->initialize($config);
       // 
        if ( !$this->upload->do_upload($inputFileName) ) {
            $result['result'] = $this->upload->display_errors();
            $result['success'] = false;
        } else {
            $result = $this->upload->data();
            $result['success'] = true;
            $pathinfo = pathinfo("{$config['upload_path']}{$result['file_name']}");
            $result = array_merge($result, $pathinfo);      
            $value = array(
                'grpContent' => $this->_grpContent,
                'filename' => $result['file_name'],
                'path' => $result['dirname'].'/',
                'size' => $result['file_size']*1024,
                'extension' => $result['extension'],
                'mime' => $result['file_type'],
                'createDate' => db_date_now(),
                'createBy' => $this->session->user['userId']
            );
             $this->resizeImage($result['file_name']);
            $result['insertId'] = $this->upload_m->insert($value);
        }

        //print_r($result);exit();
        return $result;
    }  

    public function resizeImage($filename){
      //   print_r($filename);exit();
      $source_path = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/' . $filename;
      //$target_path =  'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/thumbnail/';
      $config_manip = array(
          'image_library' => 'GD2',
          'quality'=> '90%',
          'source_image' => $source_path,
          //'new_image' => $target_path,
          'create_thumb' => FALSE,
          'maintain_ratio' => FALSE,
          //'thumb_marker' => '_thumb',
          'width' => 730,
          'height' => 487
      );


      $this->load->library('image_lib', $config_manip);
      if (!$this->image_lib->resize()) {
          echo $this->image_lib->display_errors();
      }


      $this->image_lib->clear();
   }  
    
    private function _get_config(){

        $config['upload_path'] = 'uploads/';
        $config['max_size'] = file_upload_max_size();
        $config['resize'] = true;
        $config['upload_path'] = 'uploads/'.date('Y').'/'.date('m').'/'.$this->_grpContent.'/';
        $config['encrypt_name'] = true;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|image/png';
        $config['resizeOverwrite'] = true;
        $config['quality']= '90%';
        $config['resize'] = true;
        $config['width'] = 730;
        $config['height'] = 487;
    
        
        return $config;
    } 

    public function checkTitle()
    {
        $input = $this->input->post();
        $input['recycle'] = array(0,1);
        $input['grpContent'] = $this->_grpContent;
        //$input['categoryId'] = "";
        $info = $this->news_m->get_rows($input);
       // arr($info); exit();
        if ( $info->num_rows() > 0 ) {
            if ($input['mode'] == 'create') {
                $rs = FALSE;
            } else {
                $row = $info->row();
                if ($row->contentId == decode_id($input['id'])) {
                    $rs = TRUE;
                } else {
                    $rs = FALSE;
                }
            }
        } else {
            $rs =  TRUE;
        }
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($rs));
    } 
    
}

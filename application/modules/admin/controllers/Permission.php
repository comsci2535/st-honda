<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends MX_Controller {

    public function __construct() {
        parent::__construct();
         
        $this->load->model('module_m');
    }

    public function check() {
        
        if ( $this->session->user['type'] == 'developer' || in_array($this->router->class, array('user_profile')) )
            return true;
        $module = $this->module_m->row_by_class($this->router->class);
        $policy = $this->session->user['policy'];
        if (empty($policy) || !isset($policy->{$module->moduleId}))
            return false;
        
        $policy = $policy->{$module->moduleId};
        $modify = array('create', 'save', 'update', 'action', 'update_password', 'order', 
            'update_order', 'update_v2', 'pdf', 'word', 'excel', 'import', 'export');
        if ($policy->{2} == 0 && in_array($this->router->method, $modify)) {
            $permission = false;
        } else {
            $permission = true;
        }
        return $permission;
    }

}

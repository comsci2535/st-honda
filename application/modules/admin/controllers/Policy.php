<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Policy extends MX_Controller {

    private $_title = 'กลุ่มผู้ดูแลระบบ';
    private $_pageExcerpt = 'การจัดการกลุ่มผู้ดูแลระบบ';
    private $_grpContent = 'policy';

    public function __construct() {
        parent::__construct();
        $this->load->model('policy_m');
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_refresh(base_url("admin/{$this->router->class}"));
        $action[1][] = action_add(base_url("admin/{$this->router->class}/create"));
        $action[2][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[2][] = action_trash_view(base_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $info = $this->policy_m->get_rows($input);
        $infoCount = $this->policy_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->policyId);
            $action = array();
            $action = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['name'] = $rs->name;
            $column[$key]['remark'] = $rs->remark;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = $action;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() 
    {
        $this->load->module('admin/admin');
        
        $info = $this->policy_m->get_module();
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->moduleId];
            $thisref['moduleId'] = $rs->moduleId;
            $thisref['parentId'] = $rs->parentId;
            $thisref['title'] = $rs->title;
            $thisref['icon'] = $rs->icon;
            $thisref['type'] = $rs->type;
            $thisref['hasChild'] = false;
            if ( $rs->parentId != 0 ) {
                $refs[$rs->parentId]['hasChild'] = true;
                $refs[$rs->parentId]['children'][$rs->moduleId] = &$thisref;
            } else {
                $module[$rs->moduleId] = &$thisref;
            }
        }
        //arr($module);
        $data['module'] = $module;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', base_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post(null, true);
        if ( isset($input['policy'])) {
            foreach ( $input['policy'] as  $key => &$rs ){
                $policy[$key] = array(
                    "1" => isset($rs['1']) ? 1 : 0,
                    "2" => isset($rs['2']) ? 1 : 0,
                );
            }
            $input['policy'] = $policy;
        } else {
            $input['policy'] = null;
        }
        
        $value = $this->_build_data($input);
        $result = $this->policy_m->insert($value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id=0) {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $info = $this->policy_m->get_module();
        foreach ($info->result() as $rs) {
            $thisref = &$refs[$rs->moduleId];
            $thisref['moduleId'] = $rs->moduleId;
            $thisref['parentId'] = $rs->parentId;
            $thisref['title'] = $rs->title;
            $thisref['icon'] = $rs->icon;
            $thisref['type'] = $rs->type;
            $thisref['hasChild'] = false;
            if ($rs->parentId != 0) {
                $refs[$rs->parentId]['hasChild'] = true;
                $refs[$rs->parentId]['children'][$rs->moduleId] = &$thisref;
            } else {
                $module[$rs->moduleId] = &$thisref;
            }
        }
        $info = $this->policy_m->get_row($id);
        $data['info'] = $info;
        if ( $info->policy ) {
            $policy = json_decode($info->policy);
            foreach ($policy as $key => $rs) 
                foreach ( $rs as $key2 => &$rs2 )
                    if ($rs2 == 0) unset($rs->{$key2});
        } else {
            $policy = (object)array();
        }

        $data['policy'] = $policy;
        $data['module'] = $module;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', base_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        foreach ( $input['policy'] as  $key => &$rs ){
            $policy[$key] = array(
                "1" => isset($rs['1']) ? 1 : 0,
                "2" => isset($rs['2']) ? 1 : 0,
            );
        }
        $input['policy'] = $policy;
        $value = $this->_build_data($input);
        $result = $this->policy_m->update($id, $value);
        if ( $result ) {
           Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        
        $value['name'] = $input['name'];
        $value['remark'] = $input['remark'];
        $value['policy'] = json_encode($input['policy']);
        if ( $input['mode'] == 'create' ) {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(base_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array($this->_title, base_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", base_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $info = $this->policy_m->get_rows($input);
        $infoCount = $this->policy_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->policyId);
            $action = array();
            $action[] = table_restore("admin/{$this->router->class}/action/restore");
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['name'] = $rs->name;
            $column[$key]['remark'] = $rs->remark;
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = $action;
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type=""){
        $input = $this->input->post();
        foreach ($input['id'] as &$rs) 
            $rs = decode_id($rs);
        $dateTime = db_datetime_now();
        $value['updateDate'] = $dateTime;
        $value['updateBy'] = $this->session->user['userId'];
        $result = false;
        if ( $type == "active" ) {
            $value['active'] = $input['status'] == "true" ? 1 : 0;
            $result = $this->policy_m->update_in($input['id'], $value);
        }
        if ( $type == "trash" ) {
            $value['active'] = 0;
            $value['recycle'] = 1;
            $result = $this->policy_m->update_in($input['id'], $value);
        }
        if ( $type == "restore" ) {
            $value['active'] = 0;
            $value['recycle'] = 0;
            $result = $this->policy_m->update_in($input['id'], $value);
        }
        if ( $type == "delete" ) {
            $value['active'] = 0;
            $value['recycle'] = 2;
            $result = $this->policy_m->update_in($input['id'], $value);
        }  
        if ( $result ) {
            $toastr['type'] = 'success';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
        } else {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
        }
        $data['success'] = $result;
        $data['toastr'] = $toastr;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  
    
    public function dropdown() {
        $info = $this->policy_m->get_dropdown();
        $temp[''] = 'เลือกรายการ';
        foreach ( $info->result() as $rs ) $temp[$rs->policyId] = $rs->name;
        return $temp;
    }
    
    public function login($id=0) {
//        arrx($id);
        $info = $this->policy_m->get_row($id);
//        arrx($info);
        if ( $info->policy ) {
            $policy = json_decode($info->policy);
        } else {
            $policy = (object)array();
        }
        return $policy;
    }
    
}

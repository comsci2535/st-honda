<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report_website extends MX_Controller {

    private $_title = "รายงานสรุปการเข้าชม";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับรายงานสรุปการเข้าชม";
    private $_grpContent = "report_website";
    private $_requiredExport = true;
    private $_permission;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("report_website_m");
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        // $export = array(
        //     'excel' => site_url("admin/{$this->router->class}/excel"),
        //     'pdf' => site_url("admin/{$this->router->class}/pdf"),
        // );
        // $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        // $action[1][] = action_filter();
        // $action[2][] = action_add(site_url("admin/{$this->router->class}/create"));
        // $action[2][] = action_export_group($export);
        // $action[3][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        // $action[3][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        // $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);

        $data['ddYear'] = $this->report_website_m->get_year_dd();
        $data['ddMonth'] = $this->report_website_m->get_month_dd();
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    

    public function monthly_device_data()
    {
        $param['year'] = $this->input->post('year');
        
        for ( $i = 0; $i<=11; $i++ ) $temp[$i] = 0;
        $param['isMobile'] = 0;
        $info = $this->report_website_m->get_monthly_device_data($param);
        foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'Desktop', 'data' =>  $temp ); 
        
        for ( $i = 0; $i<=11; $i++ ) $temp[$i] = 0;
        $param['isMobile'] = 1;
        $info = $this->report_website_m->get_monthly_device_data($param);
        foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
        $data['series'][] = array('name' => 'Mobile','data' =>  $temp);
        
        $data['day'] = array ('มค.', 'กพ.', 'มีค.', 'มย.', 'พค.', 'มิย.', 'กค.', 'สค.', 'กย.', 'ตค.', 'พย.', 'ธค.');
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        
    }
    
    public function monthly_page_data()
    {
        for ( $i = 0; $i<=11; $i++ ) $temp[$i] = 0;
        $param['year'] = $this->input->post('year');
        
        $param['controller'] = 'home';
        $info = $this->report_website_m->get_monthly_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
        $data['series'][] = array('name' => 'หน้าแรก', 'data' =>  $temp);
        
        $param['controller'] = 'about';
        $info = $this->report_website_m->get_monthly_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
        $data['series'][] = array('name' => 'เกี่ยวกับเรา', 'data' =>  $temp);

        $param['controller'] = 'services';
        $info = $this->report_website_m->get_monthly_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
        $data['series'][] = array('name' => 'บริการและอะไหล่', 'data' =>  $temp);

       
        $param['controller'] = 'used_car';
        $info = $this->report_website_m->get_monthly_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
        $data['series'][] = array('name' => 'รถยนต์มือสอง', 'data' =>  $temp);

        for ( $i = 0; $i<=11; $i++ ) $temp[$i] = 0;
        $param['controller'] = 'news';
        $info = $this->report_website_m->get_monthly_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
        $data['series'][] = array('name' => 'ข่าวสาร','data' =>  $temp);

         $param['controller'] = 'contact';
        $info = $this->report_website_m->get_monthly_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
        $data['series'][] = array('name' => 'ติดต่อเรา', 'data' =>  $temp);

        
        
        $data['day'] = array ('มค.', 'กพ.', 'มีค.', 'มย.', 'พค.', 'มิย.', 'กค.', 'สค.', 'กย.', 'ตค.', 'พย.', 'ธค.');
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
        
    }
    
    public function monthly_department_data()
    {
        
        $param['year'] = $this->input->post('year');
        if ( !$this->input->post('year') ) {
            $param['year'] = date("Y");
        }

        $dep = $this->db
                        ->get('department')
                        ->result_array();
        
        foreach ( $dep as $rs1 ) {
            $param['depCode'] = $rs1['depCode']; 
            for ( $i = 0; $i<=11; $i++ ) $temp[$i] = 0;
            $info = $this->report_website_m->get_monthly_department_data($param);
            foreach ( $info as $rs )  $temp[$rs['month']-1] = (int)$rs['numRow'];
            $data['series'][] = array( 'name' => $rs1['depName'], 'data' =>  $temp );             
        }
        
        $data['day'] = array ('มค.', 'กพ.', 'มีค.', 'มย.', 'พค.', 'มิย.', 'กค.', 'สค.', 'กย.', 'ตค.', 'พย.', 'ธค.');
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));                
    }
    
    public function daily_department_data()
    {
        $input = $this->input->post();
        $d = cal_days_in_month(CAL_GREGORIAN,$input['month'],$input['year']);
        for ($i = 1; $i <= $d; $i++ ) $days[$i-1] = $i;
        $data['day'] = $days;
        
        $param['year'] = $input['year'];
        $param['month'] = $input['month'];  
        $dep = $this->db
                    ->get('department')
                    ->result_array();
        foreach ( $dep as $rs1 ) {
            $param['depCode'] = $rs1['depCode']; 
            for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
            $info = $this->report_website_m->get_daily_department_data($param);
            foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
            $data['series'][] = array( 'name' => $rs1['depName'], 'data' =>  $temp );             
        }
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));                
    } 
    
    public function daily_page_data()
    {
        $input = $this->input->post();
        $d = cal_days_in_month(CAL_GREGORIAN,$input['month'],$input['year']);
        for ($i = 1; $i <= $d; $i++ ) $days[$i-1] = $i;
        $data['day'] = $days;
        
        $param['year'] = $input['year'];
        $param['month'] = $input['month'];  
        
        $param['controller'] = 'home'; 
        for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
        $info = $this->report_website_m->get_daily_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'หน้าแรก', 'data' =>  $temp ); 

         $param['controller'] = 'about'; 
        for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
        $info = $this->report_website_m->get_daily_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'เกี่ยวกับเรา', 'data' =>  $temp ); 

         $param['controller'] = 'services'; 
        for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
        $info = $this->report_website_m->get_daily_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'บริการและอะไหล่', 'data' =>  $temp ); 

         $param['controller'] = 'used_car'; 
        for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
        $info = $this->report_website_m->get_daily_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'รถยนต์มือสอง', 'data' =>  $temp ); 
        
       $param['controller'] = 'news'; 
        for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
        $info = $this->report_website_m->get_daily_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'ข่าวสาร', 'data' =>  $temp ); 

         $param['controller'] = 'contact'; 
        for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
        $info = $this->report_website_m->get_daily_page_data($param);
        foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'ติดต่อเรา', 'data' =>  $temp ); 
        
        
               
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));                
    }   
    
    public function daily_device_data()
    {
        $input = $this->input->post();
        $d = cal_days_in_month(CAL_GREGORIAN,$input['month'],$input['year']);
        for ($i = 1; $i <= $d; $i++ ) $days[$i-1] = $i;
        $data['day'] = $days;
        
        $param['year'] = $input['year'];
        $param['month'] = $input['month'];  
        
        $param['isMobile'] = 0;   
        for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
        $info = $this->report_website_m->get_daily_device_data($param);
        foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'Desktop', 'data' =>  $temp ); 
        
        $param['isMobile'] = 1; 
        for ($i = 0; $i <= $d-1; $i++ ) $temp[$i] = 0;
        $info = $this->report_website_m->get_daily_device_data($param);
        foreach ( $info as $rs )  $temp[$rs['day']-1] = (int)$rs['numRow'];
        $data['series'][] = array( 'name' => 'Mobile', 'data' =>  $temp ); 
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));                
    }
    
}

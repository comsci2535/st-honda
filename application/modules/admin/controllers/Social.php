<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Social extends MX_Controller {

    private $_title = "โซเชียล";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับโซเชียล";
    private $_grpContent = "social";
    private $_permission;
    private $_type;

    public function __construct() 
    {
        parent::__construct();
        $this->_permission = Modules::run('admin/permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("banner_m");
        $this->_type = [
            //'' => 'เลือกรายการ',
            //'hero' => 'ป้ายโฆษณาหน้าหลัก',
            //'service' => 'บริการ',
            'social' => 'Social link',
        ];
    }
    
    public function index() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_refresh(site_url("admin/{$this->router->class}"));
        $action[1][] = action_add(site_url("admin/{$this->router->class}/create"));
        $action[2][] = action_trash_multi("admin/{$this->router->class}/action/trash");
        $action[2][] = action_trash_view(site_url("admin/{$this->router->class}/trash"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/index";
        
        $this->admin->layout($data);
    }    

    public function data_index() {
        $input = $this->input->post();
        $input['recycle'] = 0;
        $input['type'] = 'social';
        $info = $this->banner_m->get_rows($input);
        $infoCount = $this->banner_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->bannerId);
            $action = array();
            $action[1][] = table_edit(site_url("admin/{$this->router->class}/edit/{$id}"));
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['type'] = isset($this->_type[$rs->type]) ? $this->_type[$rs->type]: null;
            $column[$key]['active'] = toggle_active($active, "admin/{$this->router->class}/action/active");
            $column[$key]['createDate'] = datetime_table($rs->createDate);
            $column[$key]['updateDate'] = datetime_table($rs->updateDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_button_group', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create() {
        $this->load->module('admin/admin');
        
        $info = new stdClass;
        $info->rtl = 0;
        $info->loop = 1;
        $info->priority = 2;
        $data['info'] = $info;
        $data['ddType'] = $this->_type;

        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/save");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('สร้าง', site_url("admin/{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function save() {
        $input = $this->input->post(null, true);
        $value = $this->_build_data($input);
        $id = $this->banner_m->insert($value);
        if ( $id ) {
            $value = $this->_build_upload_content($id, $input);
            if ( isset($value['bannerImage']) )
                $this->banner_m->update_banner_image($id, $value['bannerImage']);
            Modules::run('admin/upload/update_content', $value['uploadContent']);
            Modules::run('admin/utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    public function edit($id="") {
        $this->load->module('admin/admin');
        
        $id = decode_id($id);
        $input['bannerId'] = $id;
        $info = $this->banner_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect(site_url("admin/{$this->router->class}"));
        }
        $info = $info->row();
        $data['info'] = $info;
        $data['bannerImage'] = Modules::run('admin/upload/get_upload_tmpl', $info->bannerId, $this->_grpContent, 'bannerImage', FALSE);
        $bannerImage = $this->banner_m->get_banner_image($id);
       //arr($data['bannerImage']);
       //arrx($bannerImage->result());
        if ( $bannerImage->num_rows() ) {
            foreach ( $data['bannerImage'] as $key => &$rs ) {
                $img = $bannerImage->row($key);
                $startDate = $endDate = $dateRang = null;
                if ( $img->startDate ) {
                    $dateRang = date('d-m-Y', strtotime($img->startDate)).' ถึง '.date('d-m-Y', strtotime($img->endDate));
                    $startDate = $img->startDate;
                    $endDate = $img->endDate;
                }
                $rs->link = $img->link;
                $rs->titleBanner = $img->titleBanner;
                $rs->excerpt = $img->excerpt;
                $rs->startDate = $startDate;
                $rs->endDate = $endDate;
                $rs->dateRange = $dateRang;
            }
        }
        $data['ddType'] = $this->_type;
        
        $data['grpContent'] = $this->_grpContent;
        $data['frmAction'] = site_url("admin/{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array('แก้ไข', site_url("admin/{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/form";
        
        $this->admin->layout($data);
    }
    
    public function update() {
        $input = $this->input->post(null, true);
        $id = decode_id($input['id']);
        $value = $this->_build_data($input);
        $result = $this->banner_m->update($id, $value);
        if ( $result ) {
            $value = $this->_build_upload_content($id, $input);
            if ( isset($value['bannerImage']) )
                $this->banner_m->update_banner_image($id, $value['bannerImage']);
            Modules::run('admin/upload/update_content', $value['uploadContent']);
            Modules::run('admin/utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('admin/utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("admin/{$this->router->class}"));
    }
    
    private function _build_data($input) {
        $value['title'] = $input['title'];
        $value['priority'] = $input['priority'];
        $value['duration'] = $input['duration'];
        $value['type'] = $input['type'];
        $value['loop'] = $input['loop'];        
        $value['rtl'] = $input['rtl'];  
        if ( $input['mode'] == 'create' ) {
            $value['createDate'] = db_datetime_now();
            $value['createBy'] = $this->session->user['userId'];
        } else {
            $value['updateDate'] = db_datetime_now();
            $value['updateBy'] = $this->session->user['userId'];
        }
        return $value;
    }
    
    private function _build_upload_content($id, $input) {
        $value = array();
        if ( isset($input['banner']) ) {
            foreach ( $input['banner'] as $key=>$rs ) {
                if ( isset($rs['uploadId']) ) {
                    foreach ( $rs['uploadId'] as $key1=>$rs1 ) {
                        $value['uploadContent'][] = array(
                            'contentId' => $id,
                            'grpContent' => $this->_grpContent,
                            'grpType' => 'bannerImage',
                            'uploadId' => $rs['uploadId'][$key1],
                            'lang' => $key,
                        );
                        $value['bannerImage'][] = array(
                            'bannerId' => $id,
                            'link' => $rs['link'][$key1],
                            'titleBanner' => $rs['titleBanner'][$key1],
                            'excerpt' => $rs['excerpt'][$key1],
                            'startDate' => $rs['startDate'][$key1] ? $rs['startDate'][$key1] : null,
                            'endDate' => $rs['endDate'][$key1] ? $rs['endDate'][$key1] : null,
                            'uploadId' => $rs['uploadId'][$key1],
                            'lang' => $key,
                        );
                    }
                }
            }
        }
        return $value;
    }    
    
    public function trash() {
        $this->load->module('admin/admin');
        
        // toobar
        $action[1][] = action_list_view(site_url("admin/{$this->router->class}"));
        $action[2][] = action_delete_multi(base_url("admin/{$this->router->class}/action/delete"));
        $data['boxAction'] = Modules::run('admin/utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("admin/{$this->router->class}"));
        $data['breadcrumb'][] = array("ถังขยะ", site_url("admin/{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "admin/{$this->router->class}/trash";
        
        $this->admin->layout($data);
    }

    public function data_trash() {
        $input = $this->input->post();
        $input['recycle'] = 1;
        $input['type'] = 'social';
        $info = $this->banner_m->get_rows($input);
        $infoCount = $this->banner_m->get_count($input);
        $column = array();
        foreach ($info->result() as $key => $rs) {
            $id = encode_id($rs->bannerId);
            $action = array();
            $action[1][] = table_restore("admin/{$this->router->class}/action/restore");         
            $active = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId'] = $id;
            $column[$key]['checkbox'] = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title'] = $rs->title;
            $column[$key]['type'] = isset($this->_type[$rs->type]) ? $this->_type[$rs->type]: null;
            $column[$key]['recycleDate'] = datetime_table($rs->recycleDate);
            $column[$key]['action'] = Modules::run('admin/utils/build_toolbar', $action);
        }
        $data['data'] = $column;
        $data['recordsTotal'] = $info->num_rows();
        $data['recordsFiltered'] = $infoCount;
        $data['draw'] = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }    
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type'] = 'error';
            $toastr['lineOne'] = config_item('appName');
            $toastr['lineTwo'] = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success'] = false;
            $data['toastr'] = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs = decode_id($rs);
            $dateTime = db_datetime_now();
            $value['updateDate'] = $dateTime;
            $value['updateBy'] = $this->session->user['userId'];
            $result = false;
            if ( $type == "active" ) {
                $value['active'] = $input['status'] == "true" ? 1 : 0;
                $result = $this->banner_m->update_in($input['id'], $value);
            }
            if ( $type == "trash" ) {
                $value['active'] = 0;
                $value['recycle'] = 1;
                $value['recycleDate'] = $dateTime;
                $value['recycleBy'] = $this->session->user['userId'];
                $result = $this->banner_m->update_in($input['id'], $value);
            }
            if ( $type == "restore" ) {
                $value['active'] = 0;
                $value['recycle'] = 0;
                $result = $this->banner_m->update_in($input['id'], $value);
            }
            if ( $type == "delete" ) {
                $value['active'] = 0;
                $value['recycle'] = 2;
                $result = $this->banner_m->update_in($input['id'], $value);
            }   
            if ( $result ) {
                $toastr['type'] = 'success';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type'] = 'error';
                $toastr['lineOne'] = config_item('appName');
                $toastr['lineTwo'] = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }
            $data['success'] = $result;
            $data['toastr'] = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  
    
}

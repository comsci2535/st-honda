<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.*')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->from('category a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.categoryId')
                        ->from('category a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query->num_rows();
    }

     public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('category a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->where('a.categoryId', $id)
                        ->get()
                        ->row();
        return $query; 
    }

    private function _condition($param) {
        
        $this->db->where('b.lang', $this->curLang);
        
        if ( isset($param['categoryType']))
                $this->db->where('a.type', $param['categoryType']);
        
        //$this->db->order_by('a.parentId', 'asc');
        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 9) $columnOrder = "a.order";
                $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        }
        
        if ( isset($param['name']) ) 
            $this->db->where('b.name', $param['name']);
        
        if ( isset($param['categoryId']) ) 
            $this->db->where('a.categoryId', $param['categoryId']);

        if ( isset($param['recycle']) ){
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            }
           
        }
        
         if ( isset($param['active']) )
            $this->db->where('a.active', $param['active']);
    }
    
    public function insert($value) {
        $this->db->insert('category', $value);
        return $this->db->insert_id();
    }
    
    public function insert_lang($value) {
        $query = $this->db->insert('category_lang', $value);
        return $query;
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('categoryId', $id)
                        ->update('category', $value);
        return $query;
    }
    public function update_lang($id, $value)
    {
        $query = $this->db
                        ->where('categoryId', $id)
                        ->update('category_lang', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('categoryId', $id)
                        ->update('category', $value);
        return $query;
    }

     public function get_latest_order()
    {
        $query = $this->db
                        ->select('order')
                        ->from('category')
                        ->order_by('order', 'desc')
                        ->get();
        if ( $query->num_rows() == 0 ) {
            $order = 0;
        } else {
            $row = $query->row();
            $order = $row->order;
        }
        return $order;
    }


}

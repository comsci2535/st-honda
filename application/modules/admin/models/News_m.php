<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*,b.name')
                         ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query;
    }
    

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*,b.name')
                        ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query->num_rows();
    }


    private function _condition($param) 
    {
        $this->db->where('a.grpContent', $param['grpContent']);


        if ( isset($param['keyword']) ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }

       if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.createDate,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updateDate,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }    

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.view";
            if ($param['order'][0]['column'] == 3) $columnOrder = "b.name";
            if ($param['order'][0]['column'] == 4) $columnOrder = "a.createDate";
            if ($param['order'][0]['column'] == 5) $columnOrder = "a.updateDate";
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        }

        
        
        if (isset($param['categoryId']) && $param['categoryId']!="") 
            $this->db->where('a.categoryId', $param['categoryId']);

       if ( isset($param['title']) ) 
            $this->db->where('a.title', $param['title']);
        
        if ( isset($param['contentId']) ) 
             $this->db->where('a.contentId', $param['contentId']);

        if ( !in_array($this->router->method, array("profile","check_password","check_email")))
            $this->db->where('a.contentId !=', $this->session->content['contentId']);

        if ( isset($param['recycle']) ){
            if (is_array($param['recycle'])) {
                $this->db->where_in('a.recycle', $param['recycle']);
            } else {
                $this->db->where('a.recycle', $param['recycle']);
            }
           
        }



    }
    
    public function insert($value) {
        $this->db->insert('content', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('contentId', $id)
                        ->update('content', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('contentId', $id)
                        ->update('content', $value);
        return $query;
    }

}

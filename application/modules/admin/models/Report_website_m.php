<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_website_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_year_dd()
        {
            $info = $this->db
                            ->select("DISTINCT(DATE_FORMAT(createDate, '%Y')) year")
                            ->where('type', 'front')
                            ->order_by('year')
                            ->get('track')
                            ->result_array();
            $temp = array();
            foreach ( $info as $rs ) $temp[$rs['year']] = $rs['year'];
            return $temp;
        }
        
        public function get_month_dd()
        {
            $month = array("", "มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม"); 
            unset($month[0]);
            return $month;
        }
        
        public function get_monthly_device_data($param)
        {
            $sql = "SELECT DATE_FORMAT(createDate, '%c') month, count(id) numRow
                    FROM track
                    WHERE type = 'front' AND DATE_FORMAT(createDate, '%Y') = ? AND isMobile = ?
                    GROUP BY DATE_FORMAT(createDate, '%m')
                    ORDER BY DATE_FORMAT(createDate, '%m')";
            $query = $this->db
                            ->query($sql, array($param['year'], $param['isMobile']))
                            ->result_array();
            return $query;
            
        }
        
        public function get_monthly_page_data($param)
        {
            $sql = "SELECT DATE_FORMAT(createDate, '%c') month, count(id) numRow
                    FROM track
                    WHERE type = 'front' AND DATE_FORMAT(createDate, '%Y') = ? AND controller = ? 
                    GROUP BY DATE_FORMAT(createDate, '%m')
                    ORDER BY DATE_FORMAT(createDate, '%m')";
            $query = $this->db
                            ->query($sql, array($param['year'], $param['controller']))
                            ->result_array();
            return $query;
            
        }
    
        public function get_monthly_department_data($param)
        {
            $sql = "SELECT DATE_FORMAT(createDate, '%c') month, count(a.id) numRow
                    FROM track a
                    INNER JOIN member b ON b.memberId = a.memberId
                    INNER JOIN department c ON c.depCode = b.memberDepCode                    
                    WHERE type = 'front' AND DATE_FORMAT(createDate, '%Y') = ? AND c.depCode = ?
                    GROUP BY DATE_FORMAT(createDate, '%m')
                    ORDER BY DATE_FORMAT(createDate, '%m')";
            $query = $this->db
                            ->query($sql, array($param['year'], $param['depCode']))
                            ->result_array();
            return $query;
        }
        
        public function get_daily_device_data($param)
        {
            $sql = "SELECT DATE_FORMAT(createDate, '%e') day, count(id) numRow
                    FROM track
                    WHERE type = 'front' AND DATE_FORMAT(createDate, '%Y') = ? AND DATE_FORMAT(createDate, '%c') = ? AND isMobile = ?
                    GROUP BY DATE_FORMAT(createDate, '%d')
                    ORDER BY DATE_FORMAT(createDate, '%d')";
            $query = $this->db
                            ->query($sql, array($param['year'], $param['month'], $param['isMobile']))
                            ->result_array();
            return $query;
            
        }   
        
        public function get_daily_page_data($param)
        {
            $sql = "SELECT DATE_FORMAT(createDate, '%e') day, count(id) numRow
                    FROM track
                    WHERE type = 'front' AND DATE_FORMAT(createDate, '%Y') = ? AND DATE_FORMAT(createDate, '%c') = ? AND controller = ?
                    GROUP BY DATE_FORMAT(createDate, '%d')
                    ORDER BY DATE_FORMAT(createDate, '%d')";
            $query = $this->db
                            ->query($sql, array($param['year'], $param['month'], $param['controller']))
                            ->result_array();
            return $query;
            
        }      
        
        public function get_daily_department_data($param)
        {
            $sql = "SELECT DATE_FORMAT(a.createDate, '%e') day, count(a.id) numRow
                    FROM track a
                    INNER JOIN member b ON b.memberId = a.memberId
                    INNER JOIN department c ON c.depCode = b.memberDepCode                    
                    WHERE type = 'front' AND DATE_FORMAT(createDate, '%Y') = ? AND DATE_FORMAT(createDate, '%c') = ? AND c.depCode = ?
                    GROUP BY DATE_FORMAT(createDate, '%d')
                    ORDER BY DATE_FORMAT(createDate, '%d')";
            $query = $this->db
                            ->query($sql, array($param['year'], $param['month'], $param['depCode']))
                            ->result_array();
            return $query;
            
        }   

}

<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Seo_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('seo a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('seo a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        if ( isset($param['grpContent']) ) 
            $this->db->where('a.grpContent', $param['grpContent']);
        
        if ( isset($param['seoId']) ) 
            $this->db->where('a.seoId', $param['seoId']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }
    
    public function insert($value) {
        $this->db->insert('seo', $value);
        return $this->db->insert_id();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('seoId', $id)
                        ->update('seo', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('seoId', $id)
                        ->update('seo', $value);
        return $query;
    }    

}

<?php 

class Tags_m extends MY_Model
{
	 public function __construct()

    {

        parent::__construct();

        // Your own constructor code

    }

    

    public function get_rows($param) 

    {


        if ( isset($param['tagsType']) ) 
            $this->db->where('tagsType', $param['tagsType']);
        
        if ( isset($param['tagsID']) ) 
            $this->db->where('tagsID', $param['tagsID']);

        $query = $this->db

                        ->select('a.*')
                        ->from('tags a')
                        ->get();

        return $query;

    }

    public function insert($value) {


        $this->db->insert('tags', $value);

        $id = $this->db->insert_id();

        return $id;

    }
	
} 
/*END*/
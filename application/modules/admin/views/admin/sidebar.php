<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo $this->session->user['image'] ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->user['name'] ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="">
            <?php foreach ($sidebar as $level0) : ?>
                <?php if ($level0['type'] == 2) : ?>
                <li style="font-size:14px" class="header"><i class="<?php echo $level0['icon'] ?>"></i> <span><?php echo $level0['title'] ?></span></li>
                <?php else: ?>
                    <?php if (!isset($level0['children'])) : ?>
                        <li class="<?php echo $level0['active'] ? "active" : null; ?>">
                            <a href="<?php echo $level0['href'] ?>">
                                <i class="<?php echo $level0['icon'] ?>"></i> <span><?php echo $level0['title'] ?></span>
                            </a>
                        </li>	
                    <?php else : ?>
                        <li class="treeview <?php echo $level0['active'] ? "active" : null; ?>">
                            <a href="#">
                                <i class="<?php echo $level0['icon'] ?>"></i> <span><?php echo $level0['title'] ?></span>
                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                            </a>
                            <ul class="treeview-menu">
                                <?php foreach ($level0['children'] as $level1) : ?>
                                    <?php if (!isset($level1['children'])) : ?>
                                        <li class="<?php echo $level1['active'] ? "active" : null; ?>">
                                            <a href="<?php echo $level1['href'] ?>">
                                                <i class="fa fa-circle-o"></i> <span><?php echo $level1['title'] ?></span>
                                            </a>
                                        </li>
                                    <?php else : ?>
                                        <li class="treeview <?php echo $level1['active'] ? "active" : null; ?>">
                                            <a href="#">
                                                <i class="<?php echo $level1['icon'] ?>"></i> <span><?php echo $level1['title'] ?></span>
                                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                            </a>
                                            <ul class="treeview-menu">
                                                <?php foreach ($level1['children'] as $level2) : ?>
                                                    <?php if (!isset($level2['children'])) : ?>
                                                        <li class="<?php echo $level2['active'] ? "active" : null; ?>">
                                                            <a href="<?php echo $level2['href'] ?>">
                                                                <i class="fa fa-circle-o"></i> <span><?php echo $level2['title'] ?></span>
                                                            </a>
                                                        </li>
                                                    <?php else : ?>
                                                        <li class="treeview">
                                                            <a href="#">
                                                                <i class="<?php echo $level2['icon'] ?>"></i> <span><?php echo $level2['title'] ?></span>
                                                                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
                                                            </a>
                                                            <ul class="treeview-menu">
                                                                <?php foreach ($level2['children'] as $level3) : ?>
                                                                    <li class="<?php echo $level3['active'] ? "active" : null; ?>">
                                                                        <a href="<?php echo $level3['href'] ?>">
                                                                            <i class="fa fa-circle-o"></i> <span><?php echo $level3['title'] ?></span>
                                                                        </a>
                                                                    </li>
                                                                <?php endforeach ?>
                                                            </ul>
                                                        </li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        </li> 
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                        </li>		
                    <?php endif; ?>
                <?php endif; ?>    
            <?php endforeach; ?>
            <li class="header">SYSTEM NAVIGATION</li>
            <li><a href="<?php echo site_url('admin/logout'); ?>"><i class="fa fa-sign-out text-red"></i> <span>ออกระบบ</span></a></li> 
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <div class="box-body filter">
        <form id="frm-filter" role="form">
             <div class="col-md-2 form-group">
                <label for="startRang">วันที่สร้าง</label>
                <input type="text" name="createDateRange" class="form-control" value="" />
                <input type="hidden" name="createStartDate" value="" />
                <input type="hidden" name="createEndDate" value="" />
            </div>    
            <div class="col-md-2 form-group">
                <label for="startRang">วันที่แก้ไข</label>
                <input type="text" name="updateDateRange" class="form-control" value="" />
                <input type="hidden" name="updateStartDate" value="" />
                <input type="hidden" name="updateEndDate" value="" />
            </div>  
                
        
            <div class="col-md-2 form-group">
                <label for="active">หมวดหมู่</label>
                <?php echo form_dropdown('categoryId', $categoryDropDown, null, 'class="from-control select2"') ?>
            </div>  
            <div class="col-md-2 form-group">
                <label for="active">สถานะ</label>
                <?php $activeDD = array(""=>'ทั้งหมด', 1=>'เปิด', 0=>'ปิด') ?>
                <?php echo form_dropdown('active', $activeDD, null, 'class="from-control select2"') ?>
            </div> 
            
            <div class="col-md-3 form-group">
                <label for="keyword">คำค้นหา</label>
                <input class="form-control" name="keyword" type="text">
            </div>           
            <div class="col-md-1 form-group">
                <button type="button" class="btn btn-primary btn-flat btn-block btn-filter"><i class="fa fa-filter"></i> ค้นหา</button>
            </div>
        </form>
    </div>
    <div class="box-body">
        <form  role="form">
            <table id="data-list" class="table table-hover dataTable" width="100%">
                <thead>
                    <tr>
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th>รายการ</th>
                        <th>หมวดหมู่</th>
                        <th>สร้าง</th>
                        <th>แก้ไข</th>
                        <th>สถานะ</th>
                        <th></th>
                    </tr>
                </thead>
            </table>
        </form>
    </div>  
    <div id="overlay-box" class="overlay">
        <i class="fa fa-refresh fa-spin"></i>
    </div>         
</div>

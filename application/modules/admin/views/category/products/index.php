<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">รายการ</h3>
        <div class="box-tools pull-right">
            <?php echo $boxAction; ?>
        </div>
    </div>
    <?php $this->load->view('filter') ?>
    <div class="box-body">
        <form  role="form">
            <table id="data-list" class="table table-bordered table-striped dataTable" width="100%">
                <thead>
                    <tr>
                        <th class="drag-drop"></th>
                        <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                        <th class="hidden">ลำดับ</th>
                        <th>รายการหมวดหมู่</th>
                        <th>คำอธิบายย่อ</th>
                        <th>สร้าง</th>
                        <th>แก้ไข</th>
                        <th>สถานะ</th>
                        <th></th>
                         <th></th>
                    </tr>
                </thead>
            </table>
        </form>
    </div>  
    <div id="overlay-box" class="overlay hidden">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>         
</div>
<script>
    var categoryType = "<?php echo $categoryType; ?>";
</script>

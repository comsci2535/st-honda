<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">พื้นฐาน</h3>
    </div>
    <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">
        <h4 class="block">ข้อมูลการทั่วไป</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >Google Analytics ID</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['googleAnalyticsId']) ? $info['googleAnalyticsId'] : NULL ?>" type="text" id="" class="form-control" name="googleAnalyticsId">
            </div>
        </div>
      
        <h4 class="block">ข้อมูล SEO</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อเว็บไซต์ (Title)</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info['siteTitle']) ? $info['siteTitle'] : NULL ?>" type="text" id="" class="form-control" name="siteTitle">
            </div>
        </div>  
        <div class="form-group">
            <label class="col-sm-2 control-label">คำอธิบายเว็บไชต์ (Description)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info['metaDescription']) ? $info['metaDescription'] : NULL ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">คำหลักเว็บไซต์ (Keyword)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info['metaKeyword']) ? $info['metaKeyword'] : NULL ?></textarea>
            </div>
        </div>          
        
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <?php echo form_close() ?>
</div>
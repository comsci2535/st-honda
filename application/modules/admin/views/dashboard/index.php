<!-- <div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green-gradient"><i class="fa fa-home"></i></span>
            <div class="info-box-content">
                <span class="info-box-number">บ้านมือสอง</span>
                <span class="info-box-text">5 รายการ</span>
                
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-yellow-gradient"><i class="fa fa-home"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">บ้านเช่า</span>
                <span class="info-box-text"> 6 รายการ</span>
            </div>
            
        </div>
        
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red-gradient"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">ที่ดิน</span>
                <span class="info-box-text"> 10 รายการ</span>
            </div>
            
        </div>
        
    </div>
    
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua-gradient"><i class="ion ion-ios-pie-outline"></i></span>

            <div class="info-box-content">
                <span class="info-box-number">สถิติเข้าชมเว็บไซต์</span>
                <span class="info-box-text">ทั้งหมด: 41,410 ครั้ง</span>
                <span class="info-box-text">เดสก์ท็อป: 41,410 ครั้ง </span>
                <span class="info-box-text">โมบาย: 760 ครั้ง</span>
            </div>
            
        </div>
        
    </div>
    
</div> -->

<?php echo Modules::run('admin/upload/clean2') ?>

<div class="row ">
    <div class="col-md-12">
        <div class="row connectedSortable">
            <div class="col-md-12">
                <div class="box box-solid bg-green-gradient">
                    <div class="box-header ">
                        <i class="fa fa-th"></i>
                        <h3 class="box-title">รายงานการเข้าชมเว็บไซต์รายเดือนประจำปี</h3>
                        <div class="box-tools pull-right">
                            <button type="button" class="btn bg-green btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>            
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- <div id="chart_economic"></div> -->
                                <?php echo form_dropdown('year', $ddYear, current(array_keys($ddYear)), "class='form-control monthly-year hidden'") ?>
                                 <div id="monthly-page" style="min-width: 310px; height: 500px; margin: 10px auto"></div>
                    
                            </div>
                        </div>                 
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>

       

</div>







<!-- Main content -->
	<div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable">
          <div class="box box-primary">
	            <div class="box-header">
	          		<i class="ion ion-clipboard"></i>
	          		<h3 class="box-title">ข้อมูลราคาสินค้าเกษตร</h3>
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
	            	<div id="chart_container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	            </div>
            </div>
          <!-- /.box -->
        </section>
        <!-- /.right col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
      	<!-- Right col -->
        <section class="col-lg-6 connectedSortable">
          	<div class="box box-primary">
	            <div class="box-header">
	          		<i class="ion ion-clipboard"></i>
	          		<h3 class="box-title">ข้อมูลราคาสินค้าเกษตร</h3>
	            </div>
	            <!-- /.box-header -->
	            <div class="box-body">
	            	<div id="chart_container_2" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
	            </div>
            </div>
          <!-- /.box -->
        </section>
        <!-- right col -->
  	</div>

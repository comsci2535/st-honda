<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
        <div class="box-tools pull-right">
        </div>
    </div>
    <div class="box-body">
        <?php echo form_open_multipart($frmAction, array('class' => 'form-horizontal frm-create', 'method' => 'post')) ?>
        <h4 class="block">ข้อมูลทั่วไป</h4>   
        <div class="form-group">
            <div class="col-sm-7 col-sm-offset-2">
                <label class="icheck-inline"><input type="checkbox" name="recommend" value="1" class="icheck" <?php echo isset($info->recommend) && $info->recommend == 1 ? "checked" : NULL ?>/> รายการแนะนำ</label>
            </div>
        </div>  
         <div class="form-group hidden">
            <label class="col-sm-2 control-label" for="title">หมวดหมู่</label>
            <div class="col-sm-7">
                <?php echo form_dropdown('categoryId', $categoryDropDown, isset($info->categoryId) ? $info->categoryId : NULL , 'class="form-control select2" required') ?>
            </div>
        </div>  
         
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title">รูปภาพหน้าปก</label>
            <div class="col-sm-7">
                <div class="file-loading">
                    <input id="file-1"  type="file" name="input-file-preview"  data-preview-file-type="any"
                           data-upload-url="#" data-theme="fas">

                </div>
                  <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="title"><font color=red>*</font> ชื่อเรื่อง</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control" name="title" id="input-title"  required>
            </div>
        </div>      
        <div class="form-group">
            <label class="col-sm-2 control-label" for="excerpt">คำโปรย</label>
            <div class="col-sm-7">
                <textarea name="excerpt" rows="3" class="form-control"><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
            </div>
        </div>  
        <div class="form-group">
            <label class="col-sm-2 control-label" for="detail">เนื้อหา</label>
            <div class="col-sm-7">
                <?php echo $this->ckeditor->editor("detail", isset($info->detail) ? $info->detail : NULL, "normal", 200); ?>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="tagsId">Tags</label>
            <div class="col-sm-7">
                <input value="" type="text" class="form-control" id="tagsId" name="tagsId[]" >
            </div>
        </div>

        

        <h4 class="block">ข้อมูล SEO</h4>
        <div class="form-group">
            <label class="col-sm-2 control-label" >ชื่อเรื่อง (Title)</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="" class="form-control" name="metaTitle">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">คำอธิบาย (Description)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
            </div>
        </div>   
        <div class="form-group">
            <label class="col-sm-2 control-label">คำหลัก (Keyword)</label>
            <div class="col-sm-7">
                <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?></textarea>
            </div>
        </div>          
        

        <div class="col-md-7 col-md-offset-2">
            <div class="form-group">

            </div>
        </div>  
    </div>

    <div class="box-footer">
        <div class="col-sm-7 col-sm-offset-3">
            <button class="btn btn-primary btn-flat" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->contentId) ? encode_id($info->contentId) : 0 ?>">
    <?php echo form_close() ?>
</div>           
<!--</div>-->

<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style="">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-4" id="action">
        <div class="btn-group">
            
           <!--  <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button> -->
        </div>
    </div>
    {% } %}
</script>

<script type="text/javascript">
  var arrJS = <?php echo $JSON_arr ?>;
   var arrEdit = <?php if(!empty($JSON_arrEdit)){echo $JSON_arrEdit;}?>;
</script>
<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">แบบฟอร์ม</h3>
    </div>
    <?php echo form_open($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>
    <div class="box-body">    
         <div class="form-group">
            <label class="col-sm-2 control-label">รูปภาพ</label>
            <div class="col-sm-7">
                <div class="row">
                    <div class="col-sm-12" style="margin-bottom: 10px">
                        <button onclick="set_container($('#cover-image'), 'single', 'tmpl-cover-image')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>
                       
                    </div>
                    <div class="col-sm-12" id="cover-image" style="padding-left: 0px;">
                        <script> var dataCoverImage = <?php echo isset($coverImage) ? json_encode($coverImage) : "{}" ?> </script>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Title</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->metaTitle) ? $info->metaTitle : NULL ?>" type="text" id="input-metaTitle" class="form-control" name="metaTitle" required>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label" for="metaDescription">Description</label>
            <div class="col-sm-7">
                <textarea name="metaDescription" rows="3" class="form-control"><?php echo isset($info->metaDescription) ? $info->metaDescription : NULL ?></textarea>
            </div>
        </div>    

        <div class="form-group">
            <label class="col-sm-2 control-label">Keyword</label>
            <div class="col-sm-7">
                <input value="<?php echo isset($info->metaKeyword) ? $info->metaKeyword : NULL ?>" type="text" id="input-metaKeyword" class="form-control" name="metaKeyword" required>
            </div>
        </div> 

        
    </div>
    <div class="box-footer">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-7">
            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 
        </div>
    </div>
    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->seoId) ? encode_id($info->seoId) : 0 ?>">
    <?php echo form_close() ?>
</div>
<?php echo Modules::run('admin/upload/modal', $grpContent) ?>
<?php echo Modules::run('admin/upload/modal_crop') ?>

<script type="text/x-tmpl" id="tmpl-cover-image">
    {% for (var i=0, obj; obj=o[i]; i++) { %}
    <div class="col-sm-2" id="image">
        <div style="">
            <img width="100%" src="{%=obj.thumbnailUrl%}">
        </div>
    </div>
    <div id="info">
        <input type="hidden" id="uploadId" name="coverImageId" value="{%=obj.uploadId%}">
    </div>
    <div class="col-sm-4" id="action">
        <div class="btn-group">
           
            <button onclick="remove_upload($(this))" type="button" title="ลบ" class="btn btn-danger btn-flat"><i class="fa fa-ban"></i> ลบภาพ</button>
        </div>
    </div>
    {% } %}
</script>


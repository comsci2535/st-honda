<div class="box">

    <div class="box-header with-border">

        <h3 class="box-title">แบบฟอร์ม</h3>

    </div>

    <?php echo form_open($frmAction, array('class' => 'form-horizontal frm-main', 'method' => 'post')) ?>

    <div class="box-body">    

        <h4>ข้อมูลทั่วไป</h4>

        <div class="form-group">

            <label class="col-sm-2 control-label">ชื่อ</label>

            <div class="col-sm-7">

                <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" id="input-title" class="form-control" name="title" required>

            </div>

        </div>

        <div class="form-group hidden">

            <label class="col-sm-2 control-label">หน่วงเวลาวินาที</label>

            <div class="col-sm-7">

                <input value="<?php echo isset($info->duration) ? $info->duration : 4 ?>" type="text" id="input-duration" class="form-control" name="duration" required>

            </div>

        </div>    

        

        <div class="form-group hidden">

            <label class="col-sm-2 control-label">ประเภท</label>

            <div class="col-sm-7">

                <?php echo form_dropdown('type', $ddType, isset($info->type) ? $info->type : NULL, 'class="form-control select2" required'); ?>

            </div>

        </div>    

        

        <div class="form-group hidden">

            <label class="col-sm-2 control-label" for="title">ความสำคัญ</label>

            <div class="col-sm-7">

                <label class="icheck-inline"><input <?php echo isset($info->priority) && $info->priority == 1 ? "checked" : NULL ?> type="radio" name="priority" value="1" class="icheck"/> ต่ำ</label>

                <label class="icheck-inline"><input <?php echo isset($info->priority) && $info->priority == 2 ? "checked" : NULL ?> type="radio" name="priority" value="2" class="icheck"/> ปกติ</label>

                <label class="icheck-inline"><input <?php echo isset($info->priority) && $info->priority == 3 ? "checked" : NULL ?> type="radio" name="priority" value="3" class="icheck"/> สูง</label>

            </div>

        </div>

        

        <div class="form-group hidden">

            <label class="col-sm-2 control-label" for="title">ทิศทาง</label>

            <div class="col-sm-7">

                <label class="icheck-inline"><input <?php echo isset($info->rtl) && $info->rtl == 0 ? "checked" : NULL ?> type="radio" name="rtl" value="0" class="icheck"/> ช้ายไปขวา</label>

                <label class="icheck-inline"><input <?php echo isset($info->rtl) && $info->rtl == 1 ? "checked" : NULL ?> type="radio" name="rtl" value="1" class="icheck"/> ขวาไปซ้าย</label>

            </div>

        </div>

        

        <div class="form-group hidden">

            <label class="col-sm-2 control-label" for="title">แสดงผล</label>

            <div class="col-sm-7">

                <label class="icheck-inline"><input <?php echo isset($info->loop) && $info->loop == 1 ? "checked" : NULL ?> type="radio" name="loop" value="1" class="icheck"/> วนซ้ำ</label>

                <label class="icheck-inline"><input <?php echo isset($info->loop) && $info->loop == 0 ? "checked" : NULL ?> type="radio" name="loop" value="0" class="icheck"/> เริ่มต้น</label>

            </div>

        </div>

  <style>

    .ui-state-highlight { border: 1px dotted #333; width: 100%; }

  </style>

        <!-- <h4>รูปภาพโฆษณา</h4> -->

        <!-- <div class="row"> -->
            <div class="form-group">

            <label class="col-sm-2 control-label" for="title">รูปภาพโฆษณา</label>
            <div class="col-sm-9">

                <div class="nav-tabs-custom">

               

                    <div class="tab-content" style="">

                        <?php foreach ($this->language as $keyLang=>$lang) : ?>

                        <div class="tab-pane <?php echo $keyLang=="th" ? 'active' : null ?>" id="tab_<?php echo $keyLang; ?>">

                            <div class="col-sm-2 text-right">

                                <button onclick="set_container($('#banner-image-<?php echo $keyLang; ?>'), 'multiple', 'tmpl-banner-image-<?php echo $keyLang; ?>', '<?php echo $keyLang; ?>')" type="button" class="btn btn btn-flat btn-default" data-toggle="modal" data-target="#modal-upload"><i class="fa fa-upload"></i> อัพโหลดไฟล์</button>

                            </div>

                            <div class="clearfix"></div>

                            <div class="" id="banner-image-<?php echo $keyLang; ?>">

                                <script> var dataBannerImage = <?php echo isset($bannerImage) ? json_encode($bannerImage) : "{}" ?></script>

                            </div>

                        </div>                        

                        <?php endforeach; ?>

                    </div>

                </div>

            </div>
       <!--  </div> -->

        </div>



    </div>

    <div class="box-footer">

        <div class="col-sm-2">

        </div>

        <div class="col-sm-7">

            <button class="btn btn-primary btn-flat pullleft" type="submit"><i class="fa fa-save"></i> บันทึก</button> 

        </div>

    </div>

    <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">

    <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->bannerId) ? encode_id($info->bannerId) : 0 ?>">

    <?php echo form_close() ?>

</div>





<script type="text/x-tmpl" id="tmpl-banner-image-th">

    {% for (var i=0, obj; obj=o[i]; i++) { %}

        {% if ( obj.lang == 'th') { %}    

        <div class="row item" style="margin-top:15px;">

            <div class="col-sm-2 text-right" id="image">

                <img width="90px" src="{%=obj.thumbnailUrl%}">

            </div>

            <div class="form-group col-sm-9" id="info">
                <div class="col-sm-10" style="margin-bottom:20px">

                    <input type="hidden" id="uploadId" name="banner[th][uploadId][]" value="{%=obj.uploadId%}">

                    <input placeholder="หัวข้อ" value="{%=obj.titleBanner%}" type="text" class="form-control" name="banner[th][titleBanner][]">

                </div> 
                
                <div class="clearfix"></div>
                <div class="col-sm-10" style="margin-bottom:20px">

                    <input placeholder="ลิงค์เชื่อมโยง" value="{%=obj.link%}" type="text" class="form-control" name="banner[th][link][]">

                </div> 
                <div class="clearfix"></div>
                <div class="col-sm-3">

                    <input type="text" name="dateRange" class="form-control" placeholder="ช่วงวันที่แสดงผล" value="{%=obj.dateRange%}" />

                    <input type="hidden" name="banner[th][startDate][]" value="{%=obj.startDate%}" />

                    <input type="hidden" name="banner[th][endDate][]" value="{%=obj.endDate%}" />

                </div>   

                <div class="col-sm-3" id="action">

                    <button onclick="remove_upload($(this))" type="button" class="btn btn btn-flat btn-danger"><i class="fa fa-trash"></i> ลบรายการ</button>

                </div>   

            </div>

        </div>  

        {% } %}

    {% } %}

</script>



<script type="text/x-tmpl" id="tmpl-banner-image-en">

    {% for (var i=0, obj; obj=o[i]; i++) { %}

        {% if ( obj.lang == 'en') { %}    

        <div class="row item" style="margin-top:15px;">

            <div class="col-sm-2 text-right" id="image">

                <img width="90px" src="{%=obj.thumbnailUrl%}">

            </div>

            <div class="form-group" id="info">

                <div class="col-sm-7" style="margin-bottom:20px">

                    <input type="hidden" id="uploadId" name="banner[en][uploadId][]" value="{%=obj.uploadId%}">

                    <input placeholder="ลิงค์เชื่อมโยง" value="{%=obj.link%}" type="text" class="form-control" name="banner[en][link][]">

                </div> 

                <div class="col-sm-3">

                    <input type="text" name="dateRange" class="form-control" placeholder="ช่วงวันที่แสดงผล" value="{%=obj.dateRange%}" />

                    <input type="hidden" name="banner[en][startDate][]" value="{%=obj.startDate%}" />

                    <input type="hidden" name="banner[en][endDate][]" value="{%=obj.endDate%}" />

                </div>   

                <div class="col-sm-3" id="action">

                    <button onclick="remove_upload($(this))" type="button" class="btn btn btn-flat btn-danger"><i class="fa fa-trash"></i> ลบรายการ</button>

                </div>   

            </div>

        </div>  

        {% } %}    

    {% } %}

</script>



<?php echo Modules::run('admin/upload/modal', $grpContent) ?>






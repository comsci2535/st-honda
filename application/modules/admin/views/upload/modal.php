  <style type="text/css">
            .preview video {
                width: 400px;
            }
        </style>
<div class="modal fade" id="modal-upload">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">อัพโหลด</h4>
            </div>
            <div class="modal-body">
                <br>
                <?php echo form_open_multipart("admin/upload/blueimp/{$module}/{$type}", 'id="fileupload"') ?>
                <div class="row fileupload-buttonbar">
                    <div class="col-lg-7">
                        <span class="btn btn-flat btn-success fileinput-button">
                            <i class="fa fa-plus"></i>
                            <span>เลือกไฟล์</span>
                            <input type="file" name="files[]" multiple>
                        </span>
                        <button type="submit" class="btn btn-flat btn-primary start">
                            <i class="fa fa-upload"></i>
                            <span>อัพโหลด</span>
                        </button>
                        <button type="reset" class="btn btn-flat btn-warning cancel">
                            <i class="fa fa-ban"></i>
                            <span>ยกเลิก</span>
                        </button>
                        <button type="button" class="btn btn-flat btn-danger delete">
                            <i class="fa fa-trash"></i>
                            <span>ลบไฟล์</span>
                        </button>
                        <!--<input type="checkbox" class="toggle">-->
                        <span class="fileupload-process"></span>
                    </div>
                    <div class="col-lg-5 fileupload-progress fade">
                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                        </div>
                        <div class="progress-extended">&nbsp;</div>
                    </div>
                </div>
                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                <?php echo form_close() ?>
                <br>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat btn-primary" data-dismiss="modal"><i class="fa fa-close"></i> ปิด</button>
            </div>
        </div>
    </div>
</div>
<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
    <td>
    <span class="preview"></span>
    </td>
    <td>
    <p class="name">{%=file.name%}</p>
    <p class="size">โปรดรอสักครู่...</p>    
    <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>    
    <strong class="error text-danger"></strong>
    </td>
    <td>
    
    </td>
    <td class="text-right">
    {% if (!i && !o.options.autoUpload) { %}
    <button class="btn btn-flat btn-primary start" disabled><i class="fa fa-upload"></i></button>
    {% } %}
    {% if (!i) { %}
    <button class="btn btn-flat btn-warning cancel"><i class="fa fa-ban"></i></button>
    {% } %}
    </td>
    </tr>
    {% } %}
</script>
<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
    {% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
    <td>
    <span class="preview">
    {% if (file.thumbnailUrl) { %}
    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img style="max-width:100px" src="{%=file.thumbnailUrl%}"></a>
    {% } %}
    </span>
    </td>
    <td>
    <p class="name">
    {% if (file.url) { %}
    <!--<a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>-->
    <span>{%=file.name%}</span>
    {% } else { %}
    <span>{%=file.name%}</span>
    {% } %}
    </p>
    <span class="size">{%=o.formatFileSize(file.size)%}</span>
    {% if (file.error) { %}
    <div><span class="label label-danger">Error</span> {%=file.error%}</div>
    {% } %}
    </td>
    <td>
    
    </td>
    <td class="text-right">
    {% if (file.deleteUrl) { %}
    <input type="checkbox" name="delete" value="1" class="toggle">
    <button class="btn btn-flat btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
    <i class="fa fa-trash"></i>
    </button>
    <button id="{%=file.uploadId%}"  data-url="{%=file.url%}" data-thumbnail="{%=file.thumbnailUrl%}" type="button" class="btn btn-flat btn-primary select">
    <i class="fa fa-plus"></i>
    </button>
    {% } else { %}
    <button class="btn btn-flat btn-warning cancel">
    <i class="fa fa-ban"></i>
    </button>
    {% } %}
    </td>
    </tr>
    {% } %}
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('banner_m');
    }
    
    public function hero()
    {
        $input['type']='hero';
        $input['active']='1';
        $input['recycle']='0';
        $info_ = $this->banner_m->get_list($input)->result();
        //print_r($info_);
        //arr($info_);exit();
        
        if ( !empty($info_) ) {

            foreach ( $info_ as $key=>&$rs ) {
                $input_u['grpContent'] = 'banner';
                $input_u['contentId'] = $rs->bannerId;
               

                $file_ = $this->banner_m->get_uplode($input_u)->result();
                   
                if (!empty($file_)) {
                    foreach ($file_ as $key => $value) {
                        if($value->grpType=='coverImage'){
                            $rs->image['coverImage'] = base_url($value->path.$value->filename);
                        }
                       
                    }
                    //$data['image'] = base_url($file['path'].$file['filename']);
                }

                //
            }

           
            $data['info'] = $info_;
            //arr($data['info']);exit();
            $this->load->view('hero', $data); 
            
        }

        //$this->load->view('hero', $data); 
       

        // print"<pre>";print_r($data);exit();
    }

    public function social()
    {
        $input['type']='social';
        $input['active']='1';
        $input['recycle']='0';

        $info = $this->banner_m->get_list2($input);
       
        // arr($info);exit();
        if ( !empty($info) ) {
            foreach ( $info as $key=>&$rs ) {
                $file = $this->banner_m->get_upload2($rs['uploadId']);

                if ( is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                } else {
                    unset($info[$key]);
                }
            }
            if ( !empty($info) ) {
                $data['info'] = $info;
                $this->load->view('social', $data); 
            }
        }

        // $data['info'] = $info;
        //  $this->load->view('hero', $data); 

        // print"<pre>";print_r($data['info']);exit();
    }
    public function social_contact()
    {
        $input['type']='social';
        $input['active']='1';
        $input['recycle']='0';

        $info = $this->banner_m->get_list2($input);
       
        // arr($info);exit();
        if ( !empty($info) ) {
            foreach ( $info as $key=>&$rs ) {
                $file = $this->banner_m->get_upload2($rs['uploadId']);

                if ( is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                } else {
                    unset($info[$key]);
                }
            }
            if ( !empty($info) ) {
                $data['info'] = $info;
                $this->load->view('social_contact', $data); 
            }
        }

        // $data['info'] = $info;
        //  $this->load->view('hero', $data); 

        // print"<pre>";print_r($data['info']);exit();
    }
}

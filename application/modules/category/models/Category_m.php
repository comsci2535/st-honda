<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Category_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->select('b.*')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->from('category a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.categoryId')
                        ->from('category a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query->num_rows();
    }

     public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('category a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->where('a.categoryId', $id)
                        ->get()
                        ->row();
        return $query; 
    }

    private function _condition($param) {
        
        //$this->db->where('b.lang', $this->curLang);
        
        if ( isset($param['categoryType']))
                $this->db->where('a.type', $param['categoryType']);
        
        $this->db->order_by('a.parentId', 'asc');
        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.order";
                $this->db->order_by($columnOrder, $param['order'][0]['dir']);
        }
        
        if ( isset($param['email']) ) 
            $this->db->where('a.email', $param['email']);
        
        if ( isset($param['categoryId']) ) 
            $this->db->where('a.categoryId', $param['categoryId']);

          if ( isset($param['nameLink']) ) 
            $this->db->where('b.nameLink', $param['nameLink']);

        //if ( isset($param['recycle']) )
         $this->db->where('a.recycle', 0);
        
         //if ( isset($param['active']) )
        $this->db->where('a.active', 1);

        $this->db->order_by('a.order', 'DESC');
    }
    
}

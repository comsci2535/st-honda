<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MX_Controller {

     private $_grpContent = 'contact';

    public function __construct() {
        parent::__construct();
      

    }

    public function index() {
        Modules::run('track/front','');
        $this->load->module('front');
        $data['contact_act'] = 'current';
        $data['contentView'] = 'contact/index';
        $data['pageHeader'] = 'เกี่ยวกับเรา';
        
        //echo CI_VERSION ; exit();

        $share['ogTitle']="";
        $share['ogDescription']="";
        $share['ogUrl']= 'contact';
        $share['ogImage']= config_item('metaOgImage');
        $this->_social_share($share);
        
        $this->front->layout($data);
    }

    

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  

}

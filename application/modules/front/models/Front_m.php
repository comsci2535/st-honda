<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_m extends CI_Model {
    
    public function __construct() 
    {
        parent::__construct();
    }
    public function getConfig($param)
   	{
      $this->_condition_config($param);
   		return $this->db
                ->select('a.*')
                ->from('config a')
                ->get();

               
   	}

   	private function _condition_config($param) 
    {
  
        if ( isset($param['type']) ) 
             $this->db->where('a.type', $param['type']);
        if ( isset($param['variable']) ) 
             $this->db->where('a.variable', $param['variable']);

    }

    public function getPrice($param) 
    {
  
        if ( isset($param['order']) ) 
            $this->db->order_by('price', $param['order']);

        $this->db->where('recycle',0);
        $this->db->where('active',1);
        $this->db->where('statusBuy',0);

        $this->db->where_in('grpContent',array('new_house','house','rental','land'));
        
        $query = $this->db
                        ->select('price')
                        ->from('content')
                        ->get();
        return $query;

    }

    public function getLocation($param) 
    {
  
        

        $this->db->where('recycle',0);
        $this->db->where('active',1);
        $this->db->where('statusBuy',0);

        $this->db->where('grpContent',$param['grpContent']);
        $this->db->group_by('location');
        
        $query = $this->db
                        ->select('*')
                        ->from('content')
                        ->get();
        return $query;

    }
    public function getType($param) 
    {
  
        

        $this->db->where('recycle',0);
        $this->db->where('active',1);
        $this->db->where('statusBuy',0);

        $this->db->where('location',$param['location']);
         $this->db->where_in('grpContent',array('house','rental','land'));
        $this->db->group_by('grpContent');
        
        $query = $this->db
                        ->select('*')
                        ->from('content')
                        ->get();
        return $query;

    }
}

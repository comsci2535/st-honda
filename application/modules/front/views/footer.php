<section id="content">

      <div class="content-wrap">

        <div class="col_full common-height">

          <div class="col-md-4 dark col-padding ohidden" style="background-color: #1abc9c;">
            <div>
              <h3 class="uppercase" style="font-weight: 600;">ข่าวสาร</h3>
              <p style="line-height: 1.8;">กิจกรรมดีๆกับฮอนด้า พรีเมียร์ แพรกษา บอกเลยว่าอย่าพลาดกับกิจกรรมดีๆ พร้อมของรางวัลมากมาย ... แล้วมาพบกันครับ</p>
              <a href="<?php echo site_url('news');?>" class="button button-border button-light button-rounded uppercase nomargin">ดูข่าวสารทั้งหมด</a>
              <i class="icon-bulb bgicon"></i>
            </div>
          </div>

          <div class="col-md-4 dark col-padding ohidden" style="background-color: #34495e;">
            <div>
              <h3 class="uppercase" style="font-weight: 600;">บริการและอะไหล่</h3>
              <p style="line-height: 1.8;">บริการซ่อมตัวถังและสี รอรับได้ใน 1 วัน<br>มั่นใจในบริการและการตรวจสอบคุณภาพ<font style="color:#c7161d;">"ตามมาตรฐานฮอนด้า"</font>
              </p>
              <a href="<?php echo site_url('services');?>" class="button button-border button-light button-rounded uppercase nomargin">บริการและอะไหล่</a>
              <i class="icon-cog bgicon"></i>
            </div>
          </div>

          <div class="col-md-4 dark col-padding ohidden" style="background-color: #e74c3c;">
            <div>
              <h3 class="uppercase" style="font-weight: 600;">ติดต่อเรา</h3>
             <!--  <p>
                <a href="https://facebook.com/hondapremier/"><img data-animate="fadeInLeftBig" src="<?php echo base_url("assets/website/") ?>images/fblike.png" alt="เฟสบุ๊ค ฮอนด้าพรีเมียร์ แพรกษา"></a>
                <a href="#"><img data-animate="fadeInLeftBig" src="<?php echo base_url("assets/website/") ?>images/line_add.png" alt="ไลน์ ฮอนด้าพรีเมียร์ แพรกษา"></a>
                <a href="#"><img data-animate="fadeInLeftBig" src="<?php echo base_url("assets/website/") ?>images/instagram.png" alt="อินสตาแกรม ฮอนด้าพรีเมียร์ แพรกษา"></a>
              </p> -->
               <?php echo Modules::run('banner/social_contact') ?>
              <a href="<?php echo site_url('contact');?>" class="button button-border button-light button-rounded uppercase nomargin">ติดต่อเรา</a>
              <i class="icon-thumbs-up bgicon"></i>
            </div>
          </div>

          <div class="clear"></div>

        </div>

      </div>

</section>

<footer id="footer2" class="dark" style="margin-top: 0px">
  <div id="copyrights">
    <div class="container clearfix">
      <div class="col_half">
        Copyrights &copy; <?php echo date("Y"); ?> All Rights Reserved by HONDY PREMIER PHRAEKSA<br>
        <i class="icon-envelope2"></i> hondapremier@gmail.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> 02 136 2155 <span class="middot">&middot;</span> <i class="icon-add"></i> @hondapremier
      </div>
      <div class="col_half col_last tright">
       <!--  <div class="fright clearfix">
          <a href="https://facebook.com/hondapremier/"><img src="<?php echo base_url("assets/website/") ?>images/fblike.png" alt="เฟสบุ๊ค ฮอนด้าพรีเมียร์ แพรกษา"></a>
          <a href="#"><img src="<?php echo base_url("assets/website/") ?>images/line_add.png" alt="ไลน์ ฮอนด้าพรีเมียร์ แพรกษา"></a>
          <a href="#"><img src="<?php echo base_url("assets/website/") ?>images/instagram.png" alt="อินสตาแกรม ฮอนด้าพรีเมียร์ แพรกษา"></a>
        </div> -->

        <?php echo Modules::run('banner/social') ?>

        <div class="clear"></div>
      </div>
    </div>
  </div>
</footer>
          <header>
            <div class="header clearfix mobile">
        
                 <!--search-->
                <div class=" searchbox-parent position_rel rsu_font displayNone">
                 <div class="form-group position_ab col-xs-12 bg-white color2 p-b-bs">    
                        <input type="text" class="form-control" aria-describedby="user" placeholder="Search">
                        <button class="fas fa-search form-control-feedback color3 btn-3" aria-hidden="true"></button>
                  <div class="clearfix">
                  </div>      
                </div>
                </div><!--search-->

                <div class=" bg-white">
                    
                        <div class="container">
                            
                                <div class="row">

                                    <div class="col-xs-3 rsu_font">
                                            <ul id="gn-menu" class="gn-menu-main">
                                                    <li class="gn-trigger">
                                                        <a class="gn-icon gn-icon-menu"><span>Menu</span></a>
                                                        <nav class="gn-menu-wrapper ">
                                                            <div class="gn-scroller color2">
                                                                <ul class="gn-menu">
                                                                    
                                                                    <li>
                                                                      <a href="<?php echo site_url('');?>">
                                                                          <span class="fas fa-home">
                                                                          </span>
                                                                          หน้าหลัก
                                                                      </a>
                                                                       
                                                                    </li>
                                                                    <li>
                                                                        <a href="<?php echo site_url('media');?>">
                                                                            <span class="fas fa-video">
                                                                            </span>
                                                                            คลังสื่อ
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="<?php echo site_url('news');?>">
                                                                            <span class="fas fa-list">

                                                                            </span>
                                                                            ข่าวสาร
                                                                        </a>

                                                                    </li>
                                                                    
                                                                </ul>
                                                            </div><!-- /gn-scroller -->
                                                        </nav>
                                                    </li>
                                                    
                                            </ul>
                                    </div>
                                    <div class="col-xs-3 news-2">
                                        
                                        <a href="<?php site_url('');?>" id="logo">
                                                <img src="<?php echo base_url("assets/website/") ?>include/img/i-con/pr9logo.png" class="img-responsive" alt="icon image">
                                        </a>
                                        
                                    </div>
                                    <div class="col-xs-3 news-2 color2">
                                        <span class="fas fa-search b2">
                                        </span>
                                    </div>

                                    <div class="col-xs-3 news-2 font_2">
                                                
                                             <?php if ( !$isLogin) : ?>
                                        <button class="btn-2 color1"  data-toggle="modal" data-target="#myModal">
                                                Login
                                            </button>
                                        <?php else : ?>
                                        <?php //echo $this->session->user['name'] ?>
                                        <a href="<?php echo site_url("logout") ?>" ><button  class="btn-2 color1 tahoma " >Logout</button></a>
                                         <?php endif; ?>                            
                                    </div>
                                
        
                                    
                                </div>
                           
                        </div>
                </div>
        
               
            </div>
            

            <div class="header clearfix destop">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3 col-lg-4 ">
                                        
                            <a href="<?php site_url('');?>" id="logo">
                                    <img src="<?php echo base_url("assets/website/") ?>include/img/i-con/pr9logo.png" class="img-responsive" alt="icon image">
                            </a>
                            
                        </div>

                        <div class="col-sm-9 col-lg-8 p-t-ts">
                                        
                                <div class="p-t-ts display-lg-2">
                                </div>
                                <div class=" col-sm-2 col-lg-2 rm-xs1 rsu_font news-2">

                                    <div id='desk_Menu' class="color2">
                                     <span class=" glyphicon glyphicon-th">

                                     </span>&nbsp;
                                     <span>
                                         MENU
                                     </span>

                                       <div class=" dropdown-menu p-t-ts">
                                           <span class="tangle1"> 
                                           </span>
                                        <ul class="ul-listnone">
                                            
                                            <li class=" news-2">
                                              <a href="<?php echo site_url();?>">
                                                  <span class="fas fa-home">
                                                  </span>&nbsp;
                                                  หน้าหลัก
                                              </a>
                                               
                                            </li>
                                            <li class=" news-2">
                                                <a href="<?php echo site_url('media');?>">
                                                    <span class="fas fa-video">
                                                    </span>&nbsp;
                                                    คลังสื่อ
                                                </a>
                                            </li>
                                            <li class=" news-2">
                                               <a href="<?php echo site_url('news');?>">
                                                    <span class="fas fa-list">

                                                    </span>&nbsp;
                                                    ข่าวสาร
                                                </a>

                                            </li>
                                            
                                        </ul>
                                       </div>
                                    </div>
                                </div>
                                
                                <div class=" col-sm-8 col-lg-9 rm-xs1 color2 position_rel desk_fm rsu_font">
                                  
                                  <input type="text" class="form-control" id="Name2" placeholder="Search">
                                  <button class="fas fa-search form-control-feedback btn-3" aria-hidden="true"></button>
                                  <!-- <button type="submit" class="btn-2 color1 tahoma">Login</button> -->
                                </div>
                                <div class=" col-sm-2 col-lg-1 rm-xs1 ">
                                       <?php if ( !$isLogin) : ?>
                                        <button  class="btn-2 color1 tahoma "  data-toggle="modal" data-target="#myModal">Login</button>
                                        <?php else : ?>
                                        <?php //echo $this->session->user['name'] ?>
                                         <a href="<?php echo site_url("logout") ?>" ><button  class="btn-2 color1 tahoma " >Logout</button></a>
                                         <?php endif; ?>
                                </div>
                                
                                
                                
                                
                             
                            
                        </div>


                    </div>
                </div>
            </div>
    </header>
    <div>
        <hr>
    </div>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
       <base href="<?php echo site_url(); ?>">
        <title><?php echo $pageTitle; ?></title>
        <meta name="description" content="<?php echo $metaDescription; ?>" />
        <meta name="keywords" content="<?php echo $metaKeyword; ?>" />
        <meta name="author" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="">

         <?php echo Modules::run('social/share'); ?>
      
        <?php $this->load->view('meta'); ?>

          <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=59816b9dabd6b200117460b3&product=inline-share-buttons"></script>
    </head>

    <body class="stretched">

      <div id="wrapper" class="clearfix">


            
            <?php $this->load->view('topmenu'); ?>


            <?php  $this->load->view($contentView); ?>


            <?php $this->load->view('footer'); ?>

      </div>

      <?php $this->load->view('footer_script'); ?>


         <?php echo $pageScript; ?>
      <script>

   
          
          function get_cookie(name) {

                var nameEQ = name + "=";
                var ca = document.cookie.split(';');
                for (var i = 0; i < ca.length; i++) {
                    var c = ca[i];
                    while (c.charAt(0) == ' ')
                        c = c.substring(1, c.length);
                    if (c.indexOf(nameEQ) == 0)
                        return c.substring(nameEQ.length, c.length);
                }
                return null;
            }
            var csrfToken = get_cookie('csrfCookie');
            var siteUrl = "<?php echo site_url(); ?>";
            var baseUrl = "<?php echo base_url(); ?>";
            var controller = "<?php echo $this->router->class ?>";
            var method = "<?php echo $this->router->method ?>";
            $(document).ready(function () {
                <?php if ($this->session->toastr) : ?>
                    setTimeout(function () {
                        toastr.<?php echo $this->session->toastr['type']; ?>('<?php echo $this->session->toastr['lineTwo']; ?>', '<?php echo $this->session->toastr['lineOne']; ?>');
                    }, 500);
                    <?php $this->session->unset_userdata('toastr'); ?>
                <?php endif; ?>

               
  
            });            
        </script>
    </body>
</html>

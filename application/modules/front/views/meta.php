<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>style.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/swiper.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/dark.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/font-icons.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/animate.css" type="text/css" />
<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/magnific-popup.css" type="text/css" />

<link rel="stylesheet" href="<?php echo base_url("assets/website/") ?>css/responsive.css" type="text/css" />


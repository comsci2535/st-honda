

  <!-- search -->
    <div id="search" class="overlay2 search">
        <a href="javascript:void(0)" class="closebtn colorWhite" onclick="closeSearch()">×</a>
        <div class="overlay-content">
            <div class="col-xs-12">
              <form action="<?php echo site_url('search');?>" id="search_form"> 
                <h3 class="txt-search">Search</h3>
      <p>ค้นหาสิ่งที่คุณสนใจ</p>
      <div class="">
        <div class=" ">
          <span id="validateSearchFilter" class="txt-error" style="position: relative;display: block;">
            โปรดเลือก เงื่อนไข เพื่อค้นหา
          </span>
          
        </div>
        <div class="clearfix pt"></div>
      <div class="col s12 m7 l9 xl9 rightside">
      
     
       
        <div class="input-field second-font" >
          <select class="wide type-select" name="type" id="type">
            <option value="" selected>เลือกประเภทสินค้า</option>
            <option value="new_house">บ้านใหม่</option>
            <option value="house">บ้านมือสอง</option>
            <option value="rental">บ้านเช่า</option>
            <option value="land">ที่ดิน</option>
          </select>
        </div>
         <div class="clearfix pt"></div>
        <div class="input-field second-font " >
          <select class="wide location-select" name="location" id="location">
            <option value="" selected>เลือกทำเลที่ตั้ง</option>
            <option value="ยะลา">ยะลา</option>
            <option value="ปัตตานี">ปัตตานี</option>
            <option value="นราธิวาส">นราธิวาส</option>
          </select>
        </div>
       
       <div class="clearfix pt" ></div>
        <div class="col s12 m6 l6 xl6 rightside">
             <span>ระดับราคา</span>
             <div class="slider-1">
             <input id="range" type="text" name="range" value="" >
           </div>
        </div> 

         

        <div style="margin-top: 30px; text-align: center;">
              <button class="btn-search"><i></i> ค้นหา</button>
        </div>

      
        
      
    
      </div>

         
      </div>
           </form>
            </div>
        </div>
    </div>
    <!-- search -->
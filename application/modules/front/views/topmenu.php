       <header id="header">
            <div id="header-wrap">
                <div class="container clearfix">
                    <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                    <div id="logo">
                        <a href="<?php echo site_url() ?>" class="standard-logo" data-dark-logo="<?php echo base_url("assets/website/") ?>images/logo-dark.png" data-sticky-logo="<?php echo base_url("assets/website/") ?>images/logo-media.png" data-mobile-logo="<?php echo base_url("assets/website/") ?>images/logo-media.png">
                            <img src="<?php echo base_url("assets/website/") ?>images/logo@2x.png" alt="Canvas Logo" >
                        </a>
                        <a href="<?php echo site_url() ?>" class="retina-logo" data-dark-logo="<?php echo base_url("assets/website/") ?>images/logo-dark@2x.png" data-sticky-logo="images/logo-media@2x.png" data-mobile-logo="<?php echo base_url("assets/website/") ?>images/logo-media@2x.png"><img src="<?php echo base_url("assets/website/") ?>images/logo-media@2x.png" alt="Canvas Logo"></a>
                    </div>
                    <nav id="primary-menu">
                        <ul>
                            <li class="<?php if(!empty($home_act)){ echo $home_act; } ?>"><a href="<?php echo site_url() ?>"><div>ฮอนด้าพรีเมียร์</div></a></li>
                            <li class="<?php if(!empty($about_act)){ echo $about_act; } ?>"><a href="<?php echo site_url('about') ?>"><div>เกี่ยวกับเรา</div></a></li>
                            <li class="<?php if(!empty($services_act)){ echo $services_act; } ?>"><a href="<?php echo site_url('services') ?>"><div>บริการและอะไหล่</div></a></li>
                            <li class="<?php if(!empty($used_car_act)){ echo $used_car_act; } ?>"><a href="<?php echo site_url('used_car') ?>"><div>รถยนต์มือสอง</div></a></li>
                            <li class="<?php if(!empty($news_act)){ echo $news_act; } ?>"><a href="<?php echo site_url('news') ?>"><div>ข่าวสาร</div></a></li>
                            <li class="<?php if(!empty($contact_act)){ echo $contact_act; } ?>"><a href="<?php echo site_url('contact') ?>"><div>ติดต่อเรา</div></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>

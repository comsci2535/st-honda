<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("home_m");
        


    }

    public function index() {
        Modules::run('track/front','');
        $this->load->module('front');
        $data['home_act'] = 'current';
        $data['contentView'] = 'home/index';
        $data['pageHeader'] = 'หน้าหลัก';
        
        //echo CI_VERSION ; exit();

        $share['ogTitle']="";
        $share['ogDescription']="";
        $share['ogUrl']= 'home';
        $share['ogImage']= config_item('metaOgImage');
        $this->_social_share($share);
        $this->front->layout($data);
    }

    public function interesting() {

         $type = $this->db
                        ->select('grpContent')
                        ->from('content')
                        ->where('recycle',0)
                        ->where('active',1)
                        ->where('statusBuy',0)
                        ->where_in('grpContent',array('new_house','house','rental','land'))
                        ->group_by('grpContent')
                        ->order_by('grpContent')
                        ->get();
        $type_= $type->result();

        foreach ($type_ as $key => $value) {
            if($value->grpContent=='new_house'){
                $t[$value->grpContent]="บ้านใหม่";
            }
            if($value->grpContent=='house'){
                $t[$value->grpContent]="บ้านมือสอง";
            }
            if($value->grpContent=='rental'){
                $t[$value->grpContent]="บ้านเช่า";
            }
            if($value->grpContent=='land'){
                $t[$value->grpContent]="ที่ดิน";
            }
            
        }
        $data['type_']=$t;
        //arr($data['type']);exit();


        $input['length']=12;
        $input['start']=0;
        $input['statusBuy'] = 0;

        $data['info'] = '';
        $info = $this->home_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $input_u['contentId'] = $rs['contentId'];
                //$input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->home_m->get_uplode($input_u)->result_array();
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    foreach ($uplode as $key => $value) {
                        if($value['grpType']=='coverImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'] = base_url($coverImage_path.'thumbnail/'.$coverImage_filename);
                        }
                    }
                }
                
                $Today = date("Y-m-d H:i:s");
                $NewDate = date ("Y-m-d H:i:s", strtotime("+15 day", strtotime($rs['createDate'])));
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');

                
                if($Today < $NewDate){
                    $rs['IconNew'] = ' <div class="ribbon3">NEW</div>';
                }else{
                    $rs['IconNew'] = "";
                }
                $promotion = $this->home_m->get_promotion_by_id($rs['contentId'])->row_array();
                $rs['discount'] ="";
                $rs['newPrice'] = '<strong>ราคา '.price_th(str_replace(",","",$rs['price'])).'</strong>';
                if(count($promotion) > 0){
                    

                    if($promotion['discount']!=0 && $promotion['type']==1 ){
                        $rs['promotion'] = '<div class="ribbon"><span>Sale</span></div>';
                        $rs['newPrice'] = '<strong style="text-decoration: line-through; color:#ccc;">ราคา '.price_th(str_replace(",","",$rs['price'])).'</strong>';
                        $rs['discount'] = '<br> ราคา '.(price_th(str_replace(",","",$rs['price'])-str_replace(",","",$promotion['discount'])));
                    }else if($promotion['discount']!=0 && $promotion['type']==2){
                         $rs['promotion'] = '<div class="ribbon"><span>ลด '.$promotion['discount'].'%</span></div>';
                        $rs['newPrice'] = '<strong style="text-decoration: line-through; color:#ccc;">ราคา '.price_th(str_replace(",","",$rs['price'])).'</strong>';
                        $n= ($promotion['discount']*$rs['price'])/100;
                        $rs['discount'] = '<br> ราคา '.(price_th(str_replace(",","",$rs['price'])-$n));

                    }else{
                          $rs['promotion'] = '<div class="ribbon"><span style="font-size:16px;">โปรของแถม</span></div>';
                       
                    }
                }else{
                    $rs['promotion'] = "";
                }
                 //$rs['promotion'] = $promotion;
            }

            $data['info'] = $info;
            $this->load->view('interesting', $data); 
        }
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  

}

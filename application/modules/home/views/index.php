
      
      <?php echo Modules::run('banner/hero') ?>

     <div class="section nobottommargin">
		<div class="container clearfix">
			<div class="col_three_fifth topmargin-sm bottommargin">
				<img data-animate="fadeInLeftBig" src="<?php echo base_url("assets/website/") ?>images/about.jpg" alt="ฮอนด้าพรีเมียร์ แพรกษา">
			</div>
			<div class="col_two_fifth topmargin-sm bottommargin-lg col_last">
				<div class="heading-block">
					<h2><span>ฮอนด้าพรีเมียร์</span> <font style="color:#c7161d;">แพรกษา</font></h2>
					<span>บริการด้วยรอยยิ้มและความใส่ใจ</span>
				</div>
				<p>
					<font style="color:#28358b; font-size: 20px;">ขายรถใหม่</font><br>
					รถใหม่โปรโมชั่นพิเศษเฉพาะที่นี่เท่านั้น น้องๆเซลยินดีให้บริการเสมอ<br>
					<font style="color:#28358b; font-size: 20px;">ซื้อขายรถมือสอง</font><br>
					รับซื้อขายรถยนต์มือสองทุกรุ่นทุกยี่ห้อให้ราคาพิเศษไม่เหมือนใครและไม่มีใครเหมือนด้วยสถานที่บริการอย่างดี<br>
					<font style="color:#28358b; font-size: 20px;">ซ่อมตัวถังและสี</font><br>
					บริการซ่อมตัวถังและสีรถยนต์ครบวงจรด้วยอุปกรณ์ทันสมัย คุณภาพเงางามชั้นเลิศเหมือนรถใหม่<br>
					<font style="color:#28358b; font-size: 20px;">เช็คระยะ</font><br>
					เปลี่ยนถ่ายน้ำมันเครื่อง เช็คระยะ เปลี่ยนอะไหล่ แก้ไขปัญหาการใช้งาน ด้วยความรวดเร็วแม่นยำและอุปกรณ์สุดล้ำ
				</p>
				<a href="<?php echo site_url('about');?>" class="button button-border button-rounded button-large noleftmargin topmargin-sm">รู้จักฮอนด้าพรีเมียร์ แพรกษา</a>
			</div>
		</div>
	</div>

    <?php echo Modules::run('used_car/product_home') ?>
            

           


 

 <section id="portfolio" class="portfolio">
                <div class="container">
                    <div class="row">
                        <div class="main_portfolio">
                            <div class="col-sm-4">
                                <div class="head_title">
                                    <h2>รายการที่น่าสนใจ</h2>
                                    <div class="separator"></div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-sm-offset-2">
                                

                                 <div class="project-menu toolbar mb2 mt2">
                                    <button class="btn-md fil-cat filter active" data-filter="*">ทั้งหมด</button>
                                    <?php foreach ($type_ as $key => $value) {?>
                                 
                                    <button class="btn-md fil-cat filter " data-filter=".<?=$key;?>"><?=$value;?></button>
                                <?php }?>
                                   <!--  <button class="btn-md fil-cat filter " data-filter=".house">บ้านมือสอง</button>
                                    <button class="btn-md fil-cat filter " data-filter=".rental">บ้านเช่า</button>
                                    <button class="btn-md fil-cat filter " data-filter=".land">ที่ดิน</button> -->
                                    <div class="separator2"></div>
                                </div> 

                            </div>

                            <div style="clear:both;"></div> 

                                   
                            <div class="row grid">
                             <div class="main_features_content">
                                <div class="col-sm-12">
                                <?php if(!empty($info)){ foreach ($info as $key => $rs) { ?>
                                <div class="blog-box <?php echo $rs['grpContent'] ?> project col-md-4 col-sm-6 col-xs-12 " >
                                    <div class="blog-wrap">
                                       
        
                                        <?php echo $rs['IconNew']; ?>
                                        <?php echo $rs['promotion']; ?>
                                        <div class="blog-img container-img">
                                            <a href="<?php echo site_url("{$rs['grpContent']}/detail/{$rs['linkId']}");?>">
                                                <img src="<?php echo $rs['image'] ?>" class="responsive-img container_blog image_h" alt="">
                                            <div class="overlay">
                                                <div class="text"><?php echo $rs['title']?></div>
                                            </div>
                                            </a>
                                            <h3><a href="<?php echo site_url("{$rs['grpContent']}/detail/{$rs['linkId']}");?>"><?php echo $rs['title'] ?></a></h3>
                                        </div>
                                        <div class="blog-content">

                                            <ul class="blog-meta">
                                                <!-- <li><a href="#"><i class="fa fa-clock-o"></i> <?php echo $rs['createDate'] ?></a></li> -->
                                                <li><a href="<?php echo site_url("search?type={$rs['grpContent']}&location={$rs['location']}");?>"><i class="fa fa-map-marker"></i> <?php echo $rs['location'] ?></a></li>
                                                 <!--<li><a href="#"><i class="fa fa-comments"></i> 4</a></li> -->
                                            </ul>
                                            
                                            
                                            <span class="txt start"><?php echo $rs['newPrice'] ?>  <strong style=" color:red;"><?php echo $rs['discount'];?> </strong></span>
                                            <p><?php if($rs['excerpt']!=""){ echo iconv_substr($rs['excerpt'],0,127, "UTF-8").'...'; ?><span class="read-more"><a href="<?php echo site_url("{$rs['grpContent']}/detail/{$rs['linkId']}");?>" >อ่านต่อ</a></span><?php } ?></p>
                                            <span class="more">
                                            <a  href="<?php echo site_url("{$rs['grpContent']}/detail/{$rs['linkId']}");?>">รายละเอียด <i class="fa fa-long-arrow-right"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                 
                                 <?php  if(($key+1)%3==0){ 
                                    echo $clearfix="<div class='clearfix'></div>"; 
                                 } ?>

                                 <?php } } ?> 
                                </div>
                            </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                </div>
            </section> 
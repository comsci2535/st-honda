<?php
defined('BASEPATH') or exit('No direct script access allowed');

class News_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*,b.name,c.username')
                        ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->join('user c', 'a.createBy = c.userId', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*,b.name')
                        ->from('content a')
                        ->join('category_lang b', 'a.categoryId = b.categoryId', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {
        $this->db->where('a.grpContent', 'news');
        //$this->db->where_in('a.grpContent',array('news'));
        $this->db->where('a.recycle',0);
        $this->db->where('a.active',1);


        if ( isset($param['keyword']) ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }

        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $param['createStartDate'] = $param['createStartDate']." 00:00";
            $param['createEndDate'] = $param['createEndDate']." 23:59";
            
            $this->db->where('a.createDate >=', $param['createStartDate']);
            $this->db->where('a.createDate <=', $param['createEndDate']);

        }
        
        if (isset($param['categoryId'])){ 
           if ($param['categoryId']!=""){ 
                $this->db->where('a.categoryId', $param['categoryId']);
            }
        }
       if ( isset($param['title_link']) ) {
            $this->db->where('a.title_link', $param['title_link']);
        }
        if ( isset($param['contentId']) ) 
             $this->db->where('a.contentId', $param['contentId']);
         
         if ( isset($param['statusBuy']) ) 
             $this->db->where('a.statusBuy', $param['statusBuy']);

        if ( !in_array($this->router->method, array("profile","check_password","check_email")))
            $this->db->where('a.contentId !=', $this->session->content['contentId']);
        
        if (isset($param['exclude'])) {
            $this->db
                    ->where('a.contentId !=', $param['exclude']);
        }

        $this->db->order_by('recommend', 'DESC');
        $this->db->order_by('updateDate', 'DESC');
        $this->db->order_by('createDate', 'DESC');


    }

    public function get_uplode($param) 
    {
        $this->_condition_uplode($param);
        $query = $this->db
                        ->select('a.*,b.*')
                        ->from('upload a')
                        ->join('upload_content b', 'a.uploadId = b.uploadId', 'left')
                        ->get();
        return $query;
    }

    private function _condition_uplode($param) 
    {

        //$this->db->where('a.grpContent', $param['grpContent']);
         $this->db->where_in('a.grpContent',array('news'));
        if ( isset($param['contentId']) ) 
             $this->db->where('b.contentId', $param['contentId']);

        if ( isset($param['grpType']) ) 
             $this->db->where('b.grpType', $param['grpType']);

    }

    public function get_promotion_by_id($id) 
    {
        $this->db->where('b.active',1);
        $this->db->where('b.recycle',0);
        $this->db->group_start()
                            ->where('b.startDate',NULL)
                            ->or_where('NOW() between DATE_FORMAT(b.startDate, "%Y-%m-%d %H:%i:%s") AND DATE_FORMAT(b.endDate, "%Y-%m-%d %H:%i:%s")')
                            ->or_where('DATE_FORMAT(b.startDate, "%Y-%m-%d %H:%i:%s") = "0000-00-00 00:00:00" AND DATE_FORMAT(b.endDate, "%Y-%m-%d %H:%i:%s") = "0000-00-00 00:00:00"  ')
                            ->or_where('DATE_FORMAT(b.startDate, "%Y-%m-%d %H:%i:%s") = "0000-00-00 00:00:00"  AND NOW() <= DATE_FORMAT(b.endDate, "%Y-%m-%d %H:%i:%s")')
                            ->or_where('NOW() >= DATE_FORMAT(b.startDate, "%Y-%m-%d %H:%i:%s")  AND DATE_FORMAT(b.endDate, "%Y-%m-%d %H:%i:%s") = "0000-00-00 00:00:00" ')
                ->group_end();
        $query = $this->db
                        ->select('a.*')
                        ->select('b.*')
                        ->from('promotion_content a')
                        ->join('promotion b', 'a.promotionId = b.promotionId', 'left')
                        ->where('a.contentId',$id)
                        ->get();
        return $query;
    }

    public function plus_view($id)
    {
        $sql = "UPDATE content SET view = (view+1) WHERE contentId=?";
        $this->db->query($sql, array($id));
    }

}

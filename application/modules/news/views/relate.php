<div class="blog_right_sidebar p-25">
 <aside class="single_sidebar_widget popular_post_widget">
        <h3 class="widget_title">คอร์สเรียนที่เกี่ยวข้อง</h3>
        <?php foreach ($relate_info as $key => $value) { ?>
        <div class="media post_item">
            <img src="<?php echo $value['image']; ?>" alt="post" style="width: 100px;">
            <div class="media-body">
                <a href="<?php echo site_url("news/detail/{$value['linkId']}");?>"><h3><?php echo $value['title']; ?></h3></a>
                <p><?php echo $value['createDate']; ?></p>
            </div>
        </div>
       <?php } ?>
        <div class="br"></div>
    </aside>
 </div>	
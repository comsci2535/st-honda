<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MX_Controller {
    
    

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('search_m');
       
    }
    
   

    public function index()
    {
        Modules::run('track/front','');
        $this->load->module('front');

        $input = $this->input->get();
        $input['grpContent'] = $input['type'];
        $input['statusBuy'] = 0;

        
        if(isset($input['range']) && $input['range']!=""){
           
            $input['min_price'] = substr($input['range'], 0 ,strpos($input['range'], ";"));
            $input['max_price'] = substr($input['range'],strpos($input['range'], ";")+1);

            $data['min_price2']=$input['min_price'];
            $data['max_price2']=$input['max_price'];
            $data['p']=', ช่วงราคา : </font>'.(price_th(str_replace(",","",$data['min_price2']))).' - '.(price_th(str_replace(",","",$data['max_price2'])));
        }else{
            $input['range']="";
            $input['min_price'] = "";
            $input['max_price'] = "";

            $data['min_price2']="";
            $data['max_price2']="";
            $data['p']='';
        }

        $data['type']="ทั้งหมด";
        if($input['type']!=""){
            //$data['type']=$input['type'];
                if($input['type']=='house'){
                    $data['type']='บ้านมือสอง';
                }elseif ($input['type']=='rental') {
                    $data['type']='บ้านเช่า';
                }elseif ($input['type']=='land') {
                    $data['type']='ที่ดิน';
                }elseif ($input['type']=='new_house') {
                    $data['type']='บ้านใหม่';
                }
        }
        $data['location']="ทั้งหมด";
        if($input['location']!=""){
            $data['location']=$input['location'];
        }
        
        
        

        ///pagination///
        $uri="search?type={$input['type']}&location={$input['location']}&range={$input['range']}";
        $total=$this->search_m->get_count($input);
        $segment=3;
        $per_page = 9;
        $data["links"] = $this->pagin2($uri, $total, $segment, $per_page);
        ///pagination///
        
        $page = ($this->input->get('per_page')) ? ($per_page*($this->input->get('per_page')-1)) : 0;

        $input['length']=$per_page;
        $input['start']=$page;

        $info=$this->search_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $input['grpContent'] = $input['type'];
                $input['contentId'] = $rs['contentId'];
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $rs['image'] = base_url("assets/images/no_image2.png");
                
                $file = $this->search_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].'thumbnail/'.$file['filename'])) {
                    $rs['image'] = base_url($file['path'].'thumbnail/'.$file['filename']);
                }

                if($rs['grpContent']=='house'){
                    $rs['typeName']='บ้านมือสอง';
                }elseif ($rs['grpContent']=='rental') {
                    $rs['typeName']='บ้านเช่า';
                }elseif ($rs['grpContent']=='land') {
                    $rs['typeName']='ที่ดิน';
                }elseif ($rs['grpContent']=='new_house') {
                    $rs['typeName']='บ้านใหม่';
                }

                $Today = date("Y-m-d H:i:s");
                $NewDate = date ("Y-m-d H:i:s", strtotime("+15 day", strtotime($rs['createDate'])));
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');

                
                if($Today < $NewDate){
                    $rs['IconNew'] = ' <div class="ribbon3">NEW</div>';
                }else{
                    $rs['IconNew'] = "";
                }

                $promotion = $this->search_m->get_promotion_by_id2($rs['contentId'])->row_array();
                $rs['discount'] ="";
                $rs['newPrice'] = '<strong>ราคา '.price_th(str_replace(",","",$rs['price'])).'</strong>';
                if(count($promotion) > 0){
                    if($promotion['discount']!=0 && $promotion['type']==1 ){
                        $rs['promotion'] = '<div class="ribbon"><span>Sale</span></div>';
                        $rs['newPrice'] = '<strong style="text-decoration: line-through; color:#ccc;">ราคา '.price_th(str_replace(",","",$rs['price'])).'</strong>';
                        $rs['discount'] = '<br> ราคา '.(price_th(str_replace(",","",$rs['price'])-str_replace(",","",$promotion['discount'])));
                    }else if($promotion['discount']!=0 && $promotion['type']==2){
                         $rs['promotion'] = '<div class="ribbon"><span>ลด '.$promotion['discount'].'%</span></div>';
                        $rs['newPrice'] = '<strong style="text-decoration: line-through; color:#ccc;">ราคา '.price_th(str_replace(",","",$rs['price'])).'</strong>';
                        $n= ($promotion['discount']*$rs['price'])/100;
                        $rs['discount'] = '<br> ราคา '.(price_th(str_replace(",","",$rs['price'])-$n));

                    }else{
                          $rs['promotion'] = '<div class="ribbon"><span style="font-size:16px;">โปรของแถม</span></div>';
                       
                    }
                }else{
                    $rs['promotion'] = "";
                }
                

            }
         
            $data['info'] = $info;
           
        }
       // print"<pre>"; print_r($data['info']); exit();
        
        //$data['contentHeader'] = "search/header";
        $data['contentView'] = "search/list";
        //$data['sideBarView'] = "search/sideBar";
        $data['metaTitle'] = "ค้นหา";
        $data['metaDescription'] = "ค้นหา";
        $data['metaKeyword'] = "ค้นหา";

        $share['ogTitle']="";
        $share['ogDescription']="";
        $share['ogUrl']= '';
        $share['ogImage']= config_item('metaOgImage');
        $this->_social_share($share);

        $this->front->layout_blog2($data);
        // $data['info'] = '';
        // $this->load->view('index', $data); 
    }

    public function getTrack(){
        $input = $this->input->post(null, true);
        Modules::run('track/front',$input['contentId'],'detail',$this->_grpContent);
        // print_r($input);exit();
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    } 

    public function pagin($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul class="pagination center-align">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE; //'<span aria-hidden="true">«</span>';
        $config['last_link'] = FALSE; //'<span aria-hidden="true">»</span>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<span aria-hidden="true"><i class="fa fa-angle-left"></i></span>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<span aria-hidden="true"><i class="fa fa-angle-right"></i></span>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    } 

    public function pagin2($uri, $total, $segment, $per_page = 30) {

        $config['base_url'] = site_url($uri);
        $config['per_page'] = $per_page;
        $config['total_rows'] = $total;
        $config['uri_segment'] = $segment;
        //$config['num_links'] = $num_links;
        $config['use_page_numbers'] = TRUE;
        $config['page_query_string'] = TRUE;
        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = FALSE; //'<span aria-hidden="true">«</span>';
        $config['last_link'] = FALSE; //'<span aria-hidden="true">»</span>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '<i class="fa fa-angle-left"></i>';
        $config['prev_tag_open'] = '<li >';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '<i class="fa fa-angle-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li ><span>';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->load->library('pagination');
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }
    
}

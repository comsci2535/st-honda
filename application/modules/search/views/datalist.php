<?php foreach ($info as $key => $rs) { ?>
<div data-panel="panel-<?=$rs['contentId'] ?>">
    <div class="row">
      <!-- Project Main Content Starts -->
      <div class="col s12 l6 xl6">
        <div class="carousel carousel-slider">
          <?php foreach ($rs['image'] as $key => $value) { ?>
          <a class="carousel-item" href="#one!"><img class="responsive-img" src="<?php echo $value ?>" alt="project" ></a>
        <?php } ?>
        
        </div>
      </div>
      <!-- Project Main Content Ends -->
      <!-- Project Details Starts -->
      <div class="col s12 l6 xl6">
        <h3 class="font-weight-600 white-text uppercase"><?php echo $rs['title'] ?></h3>

        <ul class="project-details white-text second-font">
           <li><i class="fa fa-edit"></i><span class="font-weight-600"> สถานที่ </span>: <span class="font-weight-400 uppercase"><?php echo $rs['location'] ?></span></li>
          <li><i class="fa fa-user"></i><span class="font-weight-600"> ผู้โพสต์ </span>: <span class="font-weight-400 uppercase"><?php echo $rs['username'] ?></span></li>
          <li><i class="fa fa-calendar"></i><span class="font-weight-600"> วันที่โพสต์ </span>: <span class="font-weight-400 uppercase"><?php echo $rs['createDate'] ?></span></li>
          <li><i class="fa fa-money"></i> <span class="font-weight-600"> ราคา</span> : <span class="font-weight-400 uppercase"><?php echo $rs['price'] ?> บาท</span></li>
        </ul>
        <hr>
        <p class="white-text second-font"><?php echo $rs['detail'] ?></p>
        <!-- <a href="#" class="waves-effect waves-light btn font-weight-500">Preview <i class="fa fa-external-link"></i></a> -->
        
      </div>
      <!-- Project Details Ends -->
    </div>
  </div>
  <!-- Project Ends -->
   <?php } ?>


<!-- Portfolio Starts -->
<section id="bl-work-section">
  <!-- Portfolio Title Starts -->
  <div class="bl-box valign-wrapper">
    <div class="page-title center-align">
      <span class="title-bg">HOUSE</span>
      <h2 class="center-align">
        <span data-hover="บ้าน">บ้าน </span>
        <span data-hover="มือสอง">มือสอง</span>
      </h2>
    </div>
  </div>
  <!-- Portfolio Title Ends -->
  <!-- Portfolio Expanded Starts -->
  <div class="bl-content">
    <!-- Main Heading Starts -->
    <div class="container page-title center-align">
      <h2 class="center-align">
        <span data-hover="บ้าน">บ้าน </span>
        <span data-hover="มือสอง">มือสอง</span>
      </h2>
      <span class="title-bg">HOUSE</span>
    </div>
    <!-- Main Heading Ends -->
    <div class="container">
      <!-- Divider Starts -->
      <div class="divider center-align">
        <span class="outer-line"></span>
        <span class="fa fa-suitcase" aria-hidden="true"></span>
        <span class="outer-line"></span>
      </div>
      <!-- Divider Ends -->
      <div class="row center-align da-thumbs" >

        <?php if(!empty($info)){ foreach ($info as $key => $rs) { ?>
         
        <!-- Project Starts -->
        <div class="col s12 m6 l6 xl6" style="padding-bottom: 15px;">
          <!-- Picture Starts -->
           <div class="col s12 m5 l5 xl5 profile-picture">
            <img src="<?php echo $rs['image'] ?>" class="responsive-img my-picture" alt="My Photo">
           </div>
          <!-- Picture Ends -->
          <div class="col s12 m7 l7 xl7 personal-info ">
            <h6 class="second-font left-align"><a href="<?php echo site_url("house/detail/{$rs['linkId']}");?>" style="color: #fa5b0f;"><?php echo $rs['title'] ?></a></h6>
            <p class="left-align"><?php echo iconv_substr($rs['excerpt'],0,70, "UTF-8").'...'; ?></p>
             <p class="left-align">สถานที่ : <?php echo $rs['location'] ?></p>
             <p class="left-align"><b>ราคา : <?php echo price_th(str_replace(",","",$rs['price'])) ?></b></p>
          
           <div style="margin-top: 15px;">
          <a href="<?php echo site_url("house/detail/{$rs['linkId']}");?>" class="col s12 m12 l6 xl6 waves-effect waves-light btn font-weight-500">
           รายละเอียด ...</i>
         </a>
          </div>
        
       </div>
       </div>
       <!-- Project Ends -->
     <?php } } ?>
     

       
     <!-- Project Ends -->
     
</div>
</div>
</div>
<!-- Portfolio Expanded Ends -->
<img alt="close" src="<?php echo base_url("assets/website/") ?>images/close-button.png" class="bl-icon-close" />
</section>
<!-- Portfolio Section Ends -->




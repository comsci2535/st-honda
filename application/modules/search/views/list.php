<section class="title-head">
    <div class="container page-title center-align">
      <h2 class="center-align">
        <span>ค้น</span>
        <span>หา</span>
      </h2>
      <span class="title-bg">SEARCH</span>
    </div>
    <div class="divider center-align">
      <span class="outer-line"></span>
     <?php echo $header;?>      
     <span class="outer-line"></span>
    </div>
</section>

 <div class="col-xs-12">                  
    <div class="pagination-wrap text-center">
     <h4><font color="#555">ประเภท :</font> <?=$type?> <font color="#555">, ทำเลที่ตั้ง : </font><?=$location?> <font color="#555"> <?php echo $p; ?> </h4>                      
    </div> 
</div>
<section class="features">
    <div class="container">
      <div class="row">
          

           
            <?php if(!empty($info)){ foreach ($info as $key => $rs) { ?>
                <div class="blog-box col-md-4 col-sm-6 col-xs-12 " >
                    <div class="blog-wrap">
                        <?php echo $rs['IconNew']; ?>
                        <?php echo $rs['promotion']; ?>
                        <div class="blog-img container-img">
                            <a href="<?php echo site_url("{$rs['grpContent']}/detail/{$rs['linkId']}");?>">
                                <img src="<?php echo $rs['image'] ?>" class="responsive-img container_blog image_h" alt="">
                            <div class="overlay">
                                <div class="text"><?php echo $rs['title']?></div>
                            </div>
                            </a>
                            <h3><a href="<?php echo site_url("{$rs['grpContent']}/detail/{$rs['linkId']}");?>"><?php echo $rs['title'] ?> (<?php echo $rs['typeName'] ?>)</a></h3>
                        </div>
                        <div class="blog-content">

                            <ul class="blog-meta">
                                <!-- <li><a href="#"><i class="fa fa-clock-o"></i> <?php echo $rs['createDate'] ?></a></li> -->
                                <li><a href="<?php echo site_url("search?type={$rs['grpContent']}&location={$rs['location']}&range={$min_price}%3B{$max_price}");?>"><i class="fa fa-map-marker"></i> <?php echo $rs['location'] ?></a></li>
                                 <!--<li><a href="#"><i class="fa fa-comments"></i> 4</a></li> -->
                            </ul>
                            
                            
                            <span class="txt start"><?php echo $rs['newPrice'] ?>  <strong style="color: red;"><?php echo $rs['discount'];?> </strong></span>
                            <p><?php if($rs['excerpt']!=""){ echo iconv_substr($rs['excerpt'],0,119, "UTF-8").'...'; ?><span class="read-more"><a href="<?php echo site_url("{$rs['grpContent']}/detail/{$rs['linkId']}");?>" >อ่านต่อ</a></span><?php } ?></p>
                            <span class="more">
                            <a  href="<?php echo site_url("{$rs['grpContent']}/detail/{$rs['linkId']}");?>">รายละเอียด <i class="fa fa-long-arrow-right"></i></a>
                            </span>
                        </div>
                    </div>
                </div>
                 
                 <?php  if(($key+1)%3==0){ 
                    echo $clearfix="<div class='clearfix'></div>"; 
                 } ?>

                 <?php } }else{ ?>
                  <div class="col-xs-12">                  
                     <div class="pagination-wrap text-center">
                     <h2>ไม่พบข้อมูลที่ค้นหา</h2>                      
                    </div> 
                 </div>
                 <?php } ?> 

              <div class="col-xs-12">
                                    
                 <div class="pagination-wrap text-center">
                  <?php echo $links ;?>                         
                </div> 
            </div>
                   
        </div>
    </div>
</section>
<div class="clearfix" style="padding-bottom: 25px"></div>




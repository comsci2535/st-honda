<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>บริการและอะไหล่</h1>
				<span>ฮอนด้าพรีเมียร์แพรกษา</span>
				<ol class="breadcrumb">
				<li><a href="<?php echo site_url(); ?>">หน้าแรก</a></li>
					<li class="active">บริการและอะไหล่ ฮอนด้าพรีเมียร์แพรกษา</li>
				</ol>
			</div>
		</section>

		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="col_full" style="padding-bottom:30px;">
						<div class="heading-block center nobottomborder">
							<h2 style="padding-top:30px;"><span>ฮอนด้าพรีเมียร์</span> <font style="color:#c7161d;">แพรกษา</font></h2>
							<span>บริการด้วยรอยยิ้มและความใส่ใจ</span>
						</div>

						<div class="col_one_third">
							<div class="heading-block fancy-title nobottomborder title-bottom-border">
								<h4><span>ซื้อขายรถมือสอง</span></h4>
							</div>
							<p>รับซื้อขายรถยนต์มือสองทุกรุ่นทุกยี่ห้อให้ราคาพิเศษไม่เหมือนใครและไม่มีใครเหมือนด้วยสถานที่บริการอย่างดี</p>
						</div>

						<div class="col_one_third">
							<div class="heading-block fancy-title nobottomborder title-bottom-border">
								<h4><span>ซ่อมตัวถังและสี</span></h4>
							</div>
							<p>บริการซ่อมตัวถังและสีรถยนต์ครบวงจรด้วยอุปกรณ์ทันสมัย คุณภาพเงางามชั้นเลิศเหมือนรถใหม่</p>
						</div>

						<div class="col_one_third col_last">
							<div class="heading-block fancy-title nobottomborder title-bottom-border">
								<h4><span>เช็คระยะ</span></h4>
							</div>
							<p>เปลี่ยนถ่ายน้ำมันเครื่อง เช็คระยะ เปลี่ยนอะไหล่ แก้ไขปัญหาการใช้งาน ด้วยความรวดเร็วแม่นยำและอุปกรณ์สุดล้ำ</p>
						</div>

						<div class="fslider" data-pagi="false" data-animation="fade">
							<div class="flexslider">
								<div class="slider-wrap">
									<div class="slide"><a href="#"><img src="<?php echo base_url("assets/website/") ?>images/about/4.jpg" alt="เกี่ยวกับเรา ฮอนด้าพรีเมียร์แพรกษา"></a></div>
									<div class="slide"><a href="#"><img src="<?php echo base_url("assets/website/") ?>images/about/5.jpg" alt="เกี่ยวกับเรา ฮอนด้าพรีเมียร์แพรกษา"></a></div>
									<div class="slide"><a href="#"><img src="<?php echo base_url("assets/website/") ?>images/about/6.jpg" alt="เกี่ยวกับเรา ฮอนด้าพรีเมียร์แพรกษา"></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				

			</div>
		</section>
<!-- Open Graph data -->
<meta property="og:url" content="<?php echo $this->ogUrl ?>" />
<meta property="og:type" content="<?php echo $this->ogType ?>" />
<meta property="og:title" content="<?php echo $this->ogTitle ?>" />
<meta property="og:description" content="<?php echo $this->ogDescription ?>"/>
<meta property="og:image" content="<?php echo $this->ogImage ?>" />
<meta property="og:site_name" content="<?php echo $this->ogSitename ?>" />
<!-- Twitter Card data -->
<meta name="twitter:card" content="<?php echo $this->ogDescription ?>">
<meta name="twitter:site" content="<?php echo $this->ogSitename ?>">
<meta name="twitter:title" content="<?php echo $this->ogTitle ?>">
<meta name="twitter:description" content="<?php echo $this->ogDescription ?>">
<meta name="twitter:creator" content="<?php echo $this->ogSitename ?>">
<!-- Twitter Summary card images must be at least 120x120 -->
<meta name="twitter:image" content="<?php echo $this->twImage ?>"> 
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Used_car extends MX_Controller {

     private $_grpContent = 'products';

    public function __construct() {
        parent::__construct();
        $this->load->model("products_m");
         $this->load->model('category/category_m');
         $this->load->model("admin/tags_m");
         $this->load->model('seo_m');

    }

    public function index() {
        Modules::run('track/front','');
        $this->load->module('front');
        

        $input_c['categoryType'] = $this->_grpContent;
        $info = $this->category_m->get_rows($input_c)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $input['grpContent'] = 'category';
                $input['contentId'] = $rs['categoryId'];
                $rs['linkId'] = str_replace(" ","-",$rs['nameLink']);
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->products_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }   

            }
         
            $data['info'] = $info;
            //arr($data['info']);exit();
           
        }

        $data['used_car_act'] = 'current';
        $data['contentView'] = 'used_car/index';
        $data['pageHeader'] = 'รถยนต์มือสอง';
        
        //echo CI_VERSION ; exit();

        $share['ogTitle']="";
        $share['ogDescription']="";
        $share['ogUrl']= 'used_car';
        $share['ogImage']= config_item('metaOgImage');
        $this->_social_share($share);
        
        $this->front->layout($data);
    }

    public function getContentMore()
    {
       
        $input = $this->input->get();
        
        $length = 8;
        $page =  $_GET['page'];
        $start = $page*$length;

        
        // $input['categoryId'] =  $_GET['category'];
        // $input['keyword'] =  $_GET['keyword'];
        $input['length'] = $length;
        $input['start'] = $start;
        $input['categoryType'] = $this->_grpContent;
        $info = $this->category_m->get_rows($input)->result_array();
        //$info = $this->products_m->get_rows($input)->result_array();
        $info_c = $this->category_m->get_count($input);

        $t=$info_c;
        $t= 0 ? 1 : ceil($t/$length);
        
        if($t==($page+1)){
            $t=0;
        }
        $txt='';
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $input['grpContent'] = 'category';
                $input['contentId'] = $rs['categoryId'];
                $rs['linkId'] = str_replace(" ","-",$rs['nameLink']);
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->products_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }   
            }

            $info_=$info;
            $txt.='<div id="category" class="row">';
            foreach ($info_ as $key2 =>  $rs2) { 

                $txt.='<div class="col-12 col-md-6 col-lg-3 blog-img" >
                                    <div class="entry-image">
                                        <a href="'.site_url("used_car/category/{$rs2['linkId']}").'" data-lightbox="image"><img class="image_fade" src="'.$rs2['image'].'" alt="Standard Post with Image"></a>
                                    </div>
                                </div>';
                  
            } 
            $txt.=' </div>';
        }else{
            if($page==0){
                $txt.='<div class="text-center" style="color:red;padding-top:50px"><h3>ไม่พบข้อมูล</h3></div>';
            }
        }
        //$data['info'] = $info;
       


        $txt2='';
        $txt2.='<div class="col-xs-12 col-sm-12  rm-xs1 products_m-2 text-right">ทั้งหมด '.$info_c.' รายการ</div>';
        
        $arrayName = array('text_' => $txt , 'text2' => $txt2 , 't' => $t);

         echo json_encode($arrayName);
         exit;
    }

    public function category($categoryId)
    {
        // $this->session->unset_userdata('urlreffer');
        // $urlreffer['url'] = site_url('article/category/'.$categoryId);
        // $this->session->set_userdata('urlreffer', $urlreffer);

        $this->load->module('front');
        $input = $this->input->get();
        $input['grpContent'] = $this->_grpContent;
        $input['nameLink'] = $categoryId;
        $data['nameLink']=$categoryId;

        
        $input_c['categoryType'] = $this->_grpContent;
        $data['category'] = $this->category_m->get_rows($input_c)->result_array();

   

        $info=$this->products_m->get_rows($input)->result_array();
        if (!empty($info)) {
            foreach ( $info as $key=>&$rs ) {
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $rs['contentId'];
                $rs['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->products_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $rs['image'] = base_url($file['path'].$file['filename']);
                }
                
                
            }
         
            $data['info'] = $info;
           
        }
       
        $data['used_car_act'] = 'current';
        $data['contentView'] = "used_car/category";
        
       

        $input_c['nameLink'] = $categoryId;
        $categorySeo = $this->category_m->get_rows($input_c)->row();
        $data['category_info']=$categorySeo;
        // arr($data['category_info']);exit();
        //arr($seo_);exit();

        $imgSeo = config_item('metaOgImage');
        

        $metaTitle=$categorySeo->metaTitle;
        $metaDescription=$categorySeo->metaDescription;
        $metaKeyword=$categorySeo->metaKeyword;
        
        $data['metaTitle'] = $metaTitle;
        $data['metaDescription_'] = $metaDescription;
        $data['metaKeyword_'] = $metaKeyword;

        $share['ogTitle']=$metaTitle;
        $share['ogDescription']=$metaDescription;
        $share['ogUrl']= 'used_car/category/'.$categoryId;
        $share['ogImage']= $imgSeo;
        $this->_social_share($share);
        
        $this->front->layout($data);
        // $data['info'] = '';
        // $this->load->view('index', $data); 
    }

    public function getCategoryMore()
    {
       
        $input = $this->input->get();
        
        $length = 8;
        $page =  $_GET['page'];
        $start = $page*$length;

        
        $input['categoryId'] =  $_GET['category'];
        // $input['keyword'] =  $_GET['keyword'];
        $input['length'] = $length;
        $input['start'] = $start;
        $input['categoryType'] = $this->_grpContent;
        $info = $this->products_m->get_rows($input)->result_array();
        //$info = $this->products_m->get_rows($input)->result_array();
        $info_c = $this->products_m->get_count($input);

        $t=$info_c;
        $t= 0 ? 1 : ceil($t/$length);
        
        if($t==($page+1)){
            $t=0;
        }
        $txt='';
        if (!empty($info)) {
            foreach ($info as &$rs) {
                $input_u['contentId'] = $rs['contentId'];
                $input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->products_m->get_uplode($input_u)->result_array();
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    foreach ($uplode as $key => $value) {
                        if($value['grpType']=='coverImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'] = base_url($coverImage_path.$coverImage_filename);
                        }
                    }
                }
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');
            }

            $info_=$info;
            $txt.='<div style="padding-top:10px;"><div id="posts" class="post-grid grid-container post-masonry clearfix">';
            foreach ($info_ as $key2 =>  $rs2) { 

                $txt.='<div class="entry clearfix">
                                    <div class="entry-image">
                                        <a href="'.site_url("used_car/detail/{$rs2['linkId']}").'" data-lightbox="image"><img class="image_fade" src="'.$rs2['image'].'" alt="Standard Post with Image"></a>
                                    </div>
                                    <div class="entry-title">
                                        <h2><a href="'.site_url("used_car/detail/{$rs2['linkId']}").'">'.$rs2['title'].'</a></h2>
                                    </div>
                                    <ul class="entry-meta clearfix">
                                        <li><i class="icon-calendar3"></i> '.$rs2['createDate'].'</li>
                                        <li><i class="icon-eye"></i> '.$rs2['view'].'</li>
                                        
                                    </ul>
                                    <div class="entry-content">
                                        <p>'.$rs2['excerpt'].'</p>
                                        <a href="'.site_url("used_car/detail/{$rs2['linkId']}").'"class="more-link">อ่านต่อ</a>
                                    </div>
                                </div>';
                  
            } 
            $txt.='</div></div>';
        }else{
            if($page==0){
                $txt.='<div class="text-center" style="color:red;padding-top:50px"><h3>ไม่พบข้อมูล</h3></div>';
            }
        }
        //$data['info'] = $info;
       


        $txt2='';
        $txt2.='<div class="col-xs-12 col-sm-12  rm-xs1 products_m-2 text-right">ทั้งหมด '.$info_c.' รายการ</div>';
        
        $arrayName = array('text_' => $txt , 'text2' => $txt2 , 't' => $t);

         echo json_encode($arrayName);
         exit;
    }

    public function detail($id=0)
    {
        
        $this->load->module('front');

        // $this->session->unset_userdata('urlreffer');
        // $urlreffer['url'] = site_url('products_m/detail/').$id;
        // $this->session->set_userdata('urlreffer', $urlreffer);
        $data['title_link']=$id;
        $input['title_link'] = $id;
        //$input['keyword']=
        $input['grpContent'] = $this->_grpContent;
        $data['keyword'] = "";
      


        $info=$this->products_m->get_rows($input)->row_array();
        if (empty($info)) show_error ("ขออภัยค่ะ ไม่พบหน้าที่ต้องการ", 404, "MeBook");

        
        if (!empty($info)) {
            //foreach ( $info as $key=>&$rs ) {

                $input['grpContent'] = $this->_grpContent;
                $input['contentId'] = $info['contentId'];
                $info['image'] = base_url("assets/images/no_image3.png");
                
                $file = $this->products_m->get_uplode($input)->row_array();
               
                if (!empty($file) && is_file($file['path'].$file['filename'])) {
                    $info['image'] = base_url($file['path'].$file['filename']);
                    $imageSeo = base_url($file['path'].$file['filename']);
                }


                // $info['tags']="";
                // if($info['tagsId']!=''){
                //      $tags=explode(',', $info['tagsId']);
                //      $info['tags']  = array( );
                //      foreach ($tags as $key => $value) {
                //         $input_t['tagsID'] = $value;
                //         $t=$this->tags_m->get_rows($input_t)->row_array();
                //         $rstags[$value]=$t['tagsName'];
                //      }

                //      $info['tags']=$rstags;
                     
                // }

            $data['info'] = $info;

           // }

        }

        $this->products_m->plus_view($info['contentId']);
        Modules::run('track/front',$info['contentId']);

        $input_relate['length']=10;
        $input_relate['start']=0;
        $input_relate['exclude']=$info['contentId'];
        // arr($contentId);exit();
        
       $relate_info = $this->products_m->get_rows($input_relate)->result_array();

        if (!empty($relate_info)) {
            foreach ($relate_info as &$rs) {
                $input_u['contentId'] = $rs['contentId'];
                $input_u['grpContent'] = $this->_grpContent;
                $uplode = $this->products_m->get_uplode($input_u)->result_array();
                $rs['linkId'] = str_replace(" ","-",$rs['title_link']);
                $rs['image']=base_url("assets/images/no_image2.png");
                if (!empty($uplode)){
                    foreach ($uplode as $key => $value) {
                        if($value['grpType']=='coverImage'){
                            $coverImage_path=$value['path'];
                            $coverImage_filename=$value['filename'];
                            $rs['image'] = base_url($coverImage_path.$coverImage_filename);
                        }
                    }
                }
                
                $rs['createDate'] = date_language($rs['createDate'], TRUE, 'th');
            }
        }
        $data['relate_info'] = $relate_info;
        //print"<pre>";print_r( $data['relate_info']); exit(); 

        $input_c['categoryId'] = $info['categoryId']; //$categoryId;
        $categorySeo = $this->category_m->get_rows($input_c)->row();
        $data['category_info']=$categorySeo;
        
        $data['used_car_act'] = 'current';
        $data['title']=$info['title'];
       
        $data['contentView'] = "used_car/detail";
        $data['pageHeader'] = $info['metaTitle'];
        
        $data['metaTitle'] = $info['metaTitle'];
        $data['metaDescription_'] = $info['metaDescription'];
        $data['metaKeyword_'] = $info['metaKeyword'];

        $share['ogTitle']=$info['metaTitle'];
        $share['ogDescription']=$info['metaDescription'];
        $share['ogUrl']= 'used_car/detail/'.$id;
        $share['ogImage']= $imageSeo;
        $this->_social_share($share);
        $this->front->layout($data);
    }
      public function product_home()
    {
        $data['']="";
        $this->load->view('product_home', $data);
    }

    public function _social_share($input)
    {
        $param['ogType'] = "webiste";
        $param['ogUrl'] = $input['ogUrl'];
        $param['ogTitle'] = $input['ogTitle'];
        $param['ogDescription'] = $input['ogDescription'];
        $param['ogImage'] = $input['ogImage'];
        $param['twImage'] = $input['ogImage'];
        
        Modules::run('social/set_share', $param);
    }  

}

<section id="page-title" class="page-title-mini">
            <div class="container clearfix">
                <h1>รถยนต์มือสอง</h1>
                <span>ฮอนด้าพรีเมียร์แพรกษา</span>
                <ol class="breadcrumb">
                   <li><a href="<?php echo site_url(); ?>">หน้าแรก</a></li>
                    <li ><a href="<?php echo site_url('used_car'); ?>">รถยนต์มือสอง ฮอนด้าพรีเมียร์แพรกษา</a></li>
                    <li ><a href="<?php echo site_url('used_car/category/'.$category_info->nameLink); ?>"><?php echo $category_info->name;?></a></li>
                     <li class="active"><?php echo $title;?></li>
                </ol>
            </div>
        </section>

<!--================Blog Area =================-->
        <section class="blog_area single-post-area p-25">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 posts-list">
                        <div class="single-post row">
                            <div class="col-lg-12">
                                <div class="feature-img" style="padding-top: 15px;">
                                    <img class="img-fluid" src="<?php echo $info['image']; ?>" alt="">
                                </div>  
                                                            
                            </div>
                            
                            <div class="col-lg-9 col-md-9 blog_details" style="padding-top: 10px;">
                                <h3><?php echo $info['title'] ?></h3>
                                <p class="excert">
                                    <?php echo $info['excerpt']?> 
                                </p>
                               
                            </div>
                            <div class="col-lg-12 col-md-12 blog_details">
                                
                                <p>
                                     <?php echo html_entity_decode($info['detail']); ?>
                                </p>
                                <div class="socila-link" style="padding-top: 10px;padding-bottom: 50px;">
                                    <div class="sharethis-inline-share-buttons"></div>
                                </div>  
                            </div>
                            
                        </div>
                        
                    </div>
                    <div class="col-lg-4">
                        <?php if(!empty($relate_info)){ ?>
                        <div class="blog_right_sidebar p-25"  style="padding-top: 15px;">
                             <aside class="single_sidebar_widget popular_post_widget">
                                <h3 class="widget_title">รุ่นรถอื่นๆ</h3>
                                <div class="row">
                                    <?php foreach ($relate_info as $key => $value) { ?>
                                    <div class="media post_item">
                                        <div class="col-lg-4">
                                            <img src="<?php echo $value['image']; ?>" alt="post" style="width: 100px;">
                                        </div>
                                        <div class="col-lg-8">
                                            <a href="<?php echo site_url("used_car/detail/{$value['linkId']}");?>"><h4><?php echo $value['title']; ?></h4></a>
                                            <p style="margin-top: -27px;margin-bottom: 5px;"><?php echo $value['createDate']; ?></p>
                                        </div>
                                    </div>
                                   <?php  } ?>
                                   </div>
                            </aside>
                         </div> 
                         <?php  } ?>
                    </div>
                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
    <input type="hidden" name="linkId" id="linkId" value="<?php echo $title_link; ?>">
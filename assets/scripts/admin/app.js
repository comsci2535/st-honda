"use strict";

var bootstrapToggleOpt = {
    on: '<i class="fa fa-check"></i>',
    off: '<i class="fa fa-times"></i>',
    style: 'android',
    size: 'small'
}

var iCheckboxOpt = {
    checkboxClass: 'icheckbox_square-grey',
    radioClass: 'iradio_square-grey'
}

var tbCheckAll = 0;
var csrfToken = get_cookie('csrfCookie')
var dataList = $('#data-list')
var arrayId = []

$(document).ready(function () {

    $(".sidebar-menu").tree();

    //Initialize Select2 Elements
    $(".select2").select2({
        width: "100%", language: "th"
    });

    $.validator.setDefaults({
        highlight: function (element) {
            $(element).parent('div').addClass('has-error')
        },
        unhighlight: function (element) {
            $(element).parent('div').removeClass('has-error')
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            if ( $(element).hasClass('select2') ) {
                error.insertAfter(element.next('span'))
            } else {
                error.insertAfter(element)
            }
        }
    });

    $.validator.addMethod('isEmail', function (value, element) {
        return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
    }, 'โปรดระบุรูปแบบอีเมล์ที่ถูกต้อง');

    $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt)
    $('input.icheck').iCheck(iCheckboxOpt);
    toastr.options = {closeButton: true, progressBar: true, showMethod: 'slideDown', timeOut: 2000};

    // กำหนดสถานะจากตาราง
    var errorToggle = false
    $('#data-list').on('change', '.bootstrapToggle', function () {
        var bootstrapToggle = $(this)
        var id = $(this).closest('tr').attr('id')
        var status = $(this).prop('checked')
        if (!errorToggle) {
            arrayId.push(id);
            $('#overlay-box').removeClass('hidden');
            $.post($(this).data('method'), {id: arrayId, status: status, csrfToken: csrfToken})
                    .done(function (data) {
                        $('#overlay-box').addClass('hidden');
                        if (data.success === true) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        } else if (data.success === false) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                            errorToggle = true
                            bootstrapToggle.bootstrapToggle('toggle')
                            errorToggle = false
                        }
                        arrayId = []
                    })
                    .fail(function () {
                        $('#overlay-box').addClass('hidden');
                        toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "ระบบคลังความรู้")
                        errorToggle = true
                        bootstrapToggle.bootstrapToggle('toggle')
                        errorToggle = false
                        arrayId = []
                    })
        }
    });

    //ย้ายรายการลงถังขยะ กู้รายการจากถังขยะ ลบรายการถาวร 
    $('#data-list').on('click', '.tb-action', function () {
         //BOOTBOX
       var id = $(this).closest('tr').attr('id')
       var url = $(this).data('method');
      bootbox.dialog({
      message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
      className : "my_width",
      buttons:
      {
        "success" :
        {
          "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-success",
          "callback": function() {
            
                    $("#modal_formRemove").modal({
                        "backdrop"  : "static",
                        "keyboard"  : true,
                        "show"      : true
                    });

            $('#overlay-box').removeClass('hidden');        
            arrayId.push(id)
            //console.log(url);
            //$.post($(this).data('method'), {id: arrayId, csrfToken: csrfToken})
            $.ajax({
                  url: url,
                  type: 'POST',
                  dataType: 'json',
                  headers: {
                    'X-CSRF-TOKEN': csrfToken
                  },
                  data: {id: arrayId, csrfToken: csrfToken},
              })
                .done(function (data) {
                    if (data.success === true) {
                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        $('#data-list .tb-check-all').iCheck('uncheck');
                        dataList.DataTable().draw()
                    } else if (data.success === false) {
                        $('#overlay-box').addClass('hidden');
                        toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                    }
                    arrayId = []
                })
                .fail(function () {
                    toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "ระบบคลังความรู้")
                    arrayId = []
                })

            }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
        }
      }
    });
        //if (confirm("กรุณายืนยันการทำรายการ")) {
            
        //}
    })

    // เลือกทั้งหมดจากตาราง
    $('#data-list').on('ifChecked', '.tb-check-all', function () {
        $('#data-list .tb-check-single').iCheck('check');
        $(this).parents('tbody tr').addClass('selected')
        tbCheckAll = 1;
    })
    $('#data-list').on('ifUnchecked', '.tb-check-all', function () {
        if (tbCheckAll == 1) {
            $(this).parents('tbody tr').removeClass('selected')
            $('#data-list .tb-check-single').iCheck('uncheck');
        }
    })
    $('#data-list').on('ifChecked', '.tb-check-single', function () {
        tbCheckAll = 0;
        $(this).parents('tr').addClass('selected')
    });
    $('#data-list').on('ifUnchecked', '.tb-check-single', function () {
        tbCheckAll = 0;
        $(this).parents('tr').removeClass('selected')
        $('#data-list .tb-check-all').iCheck('uncheck');
    });


    //ลบทีละหลายๆรายการจากตาราง
    $('.box-tools').on('click', '.trash-multi', function () {
        var set = $('#data-list .tb-check-single')
        $(set).each(function () {
            if ($(this).is(":checked")) {
                arrayId.push($(this).closest('tr').attr('id'))
            }
        })
        if (arrayId.length > 0) {
            //if (confirm("กรุณายืนยันการทำรายการ")) {
               var id = $(this).closest('tr').attr('id')
               var url = $(this).data('method');
              bootbox.dialog({
              message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
              className : "my_width",
              buttons:
              {
                "success" :
                {
                  "label" : "<i class='fa fa-check'></i> ตกลง",
                  "className" : "btn-sm btn-success",
                  "callback": function() {
                    
                            $("#modal_formRemove").modal({
                                "backdrop"  : "static",
                                "keyboard"  : true,
                                "show"      : true
                            });


                $('#overlay-box').removeClass('hidden');
                $.post(url, {id: arrayId, csrfToken: csrfToken})
                        .done(function (data) {
                            if (data.success === true) {
                                toastr["success"]("บันทึการเปลี่ยนแปลงเรียบร้อย", "ระบบคลังความรู้")
                                dataList.DataTable().draw()
                                arrayId = []
                            }
                        })
                        .fail(function () {
                            toastr["error"]("พบข้อผิดพลาดด้านการสื่อสาร", "ระบบคลังความรู้")
                        })

                }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
        }
      }
    });
           // }
        } else {
            //alert('กรุณาเลือกรายการที่ต้องการลบ')
            alert_box('กรุณาเลือกรายการที่ต้องการลบ');
        }
    })
    
    // ฟิตเตอร์ตารางใน
    $('.box-filter').click(function(e){
        e.preventDefault();
        $('div.filter').slideToggle()
    })
    $('.btn-filter').click(function(){
        dataList.DataTable().draw()
    })
    
    // create Daterange ใช้ที่ filter ที่หน้าตาราง
    $('input[name="createDateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="createDateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $('input[name="createStartDate"]').val(picker.startDate.format('YYYY-MM-DD'))
        $('input[name="createEndDate"]').val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="createDateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('input[name="createStartDate"]').val('')
        $('input[name="createEndDate"]').val('')        
    })  
    
    // update Daterange ใช้ที่ filter ที่หน้าตาราง
    $('input[name="updateDateRange"]').daterangepicker({
        locale: { cancelLabel: 'ยกเลิก', applyLabel:'ตกลง', format: 'DD-MM-YYYY' },
        autoUpdateInput: false,
    })   
    $('input[name="updateDateRange"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' ถึง ' + picker.endDate.format('DD-MM-YYYY'))
        $('input[name="updateStartDate"]').val(picker.startDate.format('YYYY-MM-DD'))
        $('input[name="updateEndDate"]').val(picker.endDate.format('YYYY-MM-DD'))
    })
    $('input[name="updateDateRange"]').on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
        $('input[name="updateStartDate"]').val('')
        $('input[name="updateEndDate"]').val('')        
    })     

})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})


function SetLocalStorage(name, value)
{
    if (typeof (Storage) !== "undefined") {
        localStorage.setItem(name, value);
    } else {
        alert_box('Web browser ของท่านไม่สนับสนุนการใช้งานฟังก์ชั้นนี้!');
    }
}

function GetLocalStorage(name)
{
    if (typeof (Storage) !== "undefined") {
        var data;
        data = localStorage.getItem(name);
        if (data === null && name === "sa-theme") {
            data = 5;
        }
        return data;
    } else {
        alert_box('Web browser ของท่านไม่สนับสนุนการใช้งานฟังก์ชั้นนี้!');
    }
}

function get_cookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function alert_box(text){
  bootbox.dialog({
    message: "<span class='bigger-110'><i class='fa fa-exclamation-circle text-warning'></i> "+text+"</span>",
    className : "my_width",
    buttons:
    {
      "success" :
        {
        "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-default",
          "callback": function() {
          }
       }
    }
  });
}
"use strict";

// Upload and image cropper
var dataTmpl = {}
var tmplName
var container
var objUploadId
var objUpload
var single
var $image = $('#image-crop')
var cropData = {}
var options = {
    aspectRatio: 4 / 3,
    autoCropArea: 0.8,
    minContainerHeight: 480,
    dragMode: 'move'
}
var set_container = function(obj, select, tmpl){
   container = $(obj)
   tmplName = tmpl
   single = true
   if ( select === 'multiple' )
       single = false
}
var set_cropper = function(obj){
   objUpload = obj.parents('div#action').prevAll().eq(1).find('img')
   objUploadId = obj.parents('div#action').prevAll().eq(0).find('input#uploadId')
}
var remove_upload = function(obj){


    bootbox.dialog({
      message: "<span class='bigger-110'><i class='fa fa-question-circle text-primary'></i> กรุณายืนยันการทำรายการ</span>",
      className : "my_width",
      buttons:
      {
        "success" :
        {
          "label" : "<i class='fa fa-check'></i> ตกลง",
          "className" : "btn-sm btn-success",
          "callback": function() {
            
            obj.parents('div#action').prevAll().eq(1).remove()
            obj.parents('div#action').prevAll().eq(0).remove()
            obj.parents('div#action').remove()

            }

        },
        "cancel" :
        {
          "label" : "<i class='fa fa-times'></i> ยกเลิก",
          "className" : "btn-sm btn-white",
        }
      }
    });
}

// END upload and image cropper

$(document).ready(function () {
    //$('.form-horizontal').validate();

    $("#cover-image").html(tmpl("tmpl-cover-image", dataCoverImage))
    $('.form-horizontal').validate({
        rules: {
            name: {
                remote: {
                    url: "admin/category/checkTitle",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        },categoryType: function () {
                            return $('#input-catetory-type').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            }
        },
        messages: {
            name: {remote: 'ชื่อหมวดหมู่ซ้ำ'}
        }
        
    });
    
    $('.select2').change(function () {
        $('.form-horizontal').validate().element('.select2');
    })


    // Upload and image cropper
    $('tbody.files').on('click', 'button.select',  function (){
        dataTmpl[0] = {
            uploadId: $(this).attr('id'),
            thumbnailUrl: $(this).data('thumbnail'),
            url: $(this).data('url')
        }
        if ( single ) {
            $(container).html(tmpl(tmplName, dataTmpl));
        } else {
            $(container).append(tmpl(tmplName, dataTmpl));
        }
    })
    $('#modal-crop').on('shown.bs.modal', function () {
        $.get('admin/upload/get_image', {uploadId:objUploadId.val()}, function(data){
            $image.cropper('destroy').attr('src', data.url)
            $image.cropper(options)
            $image.on({
                crop: function (e) {
                    cropData.x = e.x
                    cropData.y = e.y
                    cropData.width = e.width
                    cropData.height = e.height
                    cropData.rotate = e.rotate
                    cropData.scaleX = e.scaleX
                    cropData.scaleY = e.scaleY
                }
            })
        })
    });
// END upload and image cropper
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

"use strict";
// Upload and image cropper
var container
var uploadId
var $image = $('#image-crop')
var cropData = {}
var options = {
    aspectRatio: 4 / 3,
    autoCropArea: 0.8,
    minContainerHeight: 480,
    dragMode: 'move'
}
var set_container = function(obj){
   container = $(obj).parent().prev()
}
var set_cropper = function(obj){
   container = $(obj).parent().prev()
   uploadId = $(container).find('input').val()
}
var modal_crop = function(callback){
 
  $("#modal-yes").on("click", function(){
    callback(true);
    $("#modal-crop").modal('hide');
  });
  
  $("#modal-no").on("click", function(){
    callback(false);
    $("#modal-crop").modal('hide');
  });
  
};
modal_crop(function(confirm){
  if( confirm ){
    $image.cropper('destroy')
    $.ajax({
        url: 'admin/upload/cropper',
        type: 'POST',
        data: {cropData:cropData,uploadId:uploadId,csrfToken:csrfToken},
        success: function (data) {
            $(container).find('img').attr('src', data.thumbnailUrl)
            $('label.ajax-upload').addClass('hidden')
            $image.cropper('destroy').attr('src', data.url)
        },
        error: function () {
            $('label.ajax-upload').addClass('hidden')
            toastr["error"]("พบข้อผิดพลาดด้านเทคนิค", "ระบบแอดมิน oem")
        },
        beforeSend: function () {
            $('label.ajax-upload').removeClass('hidden')
        }
    });
  }
});
// END upload and image cropper

$(document).ready(function () {
    
    $('.frm-create').validate({
        rules: {
            email: {
                remote: {
                    url: "admin/user/check_email",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            },
            rePassword: {equalTo: "#input-password"}
        },
        messages: {
            email: {remote: 'พบอีเมล์ซ้ำในระบบ'},
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    })

    $('.frm-edit').validate({
        rules: {
            email: {
                remote: {
                    url: "admin/user/check_email",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            }
        },
        messages: {
            email: {remote: 'พบอีเมล์ซ้ำในระบบ'}
        }
    });

    $('.frm-change-password').validate({
        rules: {
            oldPassword: {
                remote: {
                    url: "admin/user/check_password",
                    type: "post",
                    data: {id: function () {
                            return $('#input-id').val();
                        }, mode: function () {
                            return $('#input-mode').val();
                        }, csrfToken: csrfToken}
                }
            },
            rePassword: {equalTo: "#input-new-password"}
        },
        messages: {
            oldPassword: {remote: 'รหัสผ่านปัจจุบันไม่ถูกต้อง'},
            rePassword: {equalTo: 'โปรดระบุรหัสผ่านอีกครั้ง'}
        }
    });

    $('.select2').change(function () {
        $('.form-horizontal').validate().element('.select2')
    })
    
// Upload and image cropper
    $('tbody.files').on('click', 'button.select',  function (){
        var uploadId = $(this).attr('id')
        var thumbnailUrl = $(this).data('thumbnail')
        var url = $(this).data('url')
        $(container).find('img').attr('src', thumbnailUrl)
        $(container).find('input').val(uploadId)
        $('#modal-upload').modal('hide')
        $(this).closest('tr').remove();
    })
    $('#modal-crop').on('shown.bs.modal', function () {
        $.get('admin/upload/get_image', {uploadId:uploadId}, function(data){
            $image.cropper('destroy').attr('src', data.url)
            $image.cropper(options)
            $image.on({
                crop: function (e) {
                    cropData.x = e.x
                    cropData.y = e.y
                    cropData.width = e.width
                    cropData.height = e.height
                    cropData.rotate = e.rotate
                    cropData.scaleX = e.scaleX
                    cropData.scaleY = e.scaleY
                }
            })
        })
    });
// END upload and image cropper


})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

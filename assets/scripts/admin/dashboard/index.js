"use strict";

$(document).ready(function () {
    
  // Make the dashboard widgets sortable Using jquery UI
  $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');    
    


Highcharts.chart('chart_economic', {
    chart: {
        type: 'column'
    },
    title: {
        text: '5 อันดับผู้เข้าชมคลังสื่อ/หลักสูตร ประจำปี 2017'
    },
    subtitle: {
        text: 'โรงพยาบาลพระรามเก้า'
    },
    xAxis: {
        categories: [
            'มค.', 'กพ.', 'มีค.', 'มย.', 'พค.', 'มิย.', 'กค.', 'สค.', 'กย.', 'ตค.', 'พย.', 'ธค.'
        ],
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'จำนวน(ครั้ง)'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} บาท</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.0,
            borderWidth: 0
        }
    },
    series: [{
            name: 'ความรู้ในงาน/วิชาชีพ',
            data: [106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4, 176.0, 135.6,]

        }, {
            name: 'การคิดเชิงวิเคราะห์ และการจัดระบบงาน',
            data: [98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3, 60.4, 47.6]

        }, {
            name: 'การบริหารเชิงกลยุทธ์',
            data: [39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2, 176.0, 135.6,]

        }, {
            name: 'การบริหารองค์กร',
            data: [34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1, 59.0, 59.6]

        }, {
            name: 'การคิดเชิงวิเคราะห์',
            data: [39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2, 176.0, 135.6,]

        }]
});

})

$(function () {
        
        report_monthly();
       
        
});

function report_monthly()
    {
        
        
        $.post(siteUrl+"admin/report_website/monthly_page_data", {'csrfToken': get_cookie('csrfCookie'),year:$('.monthly-year').val()})
                .done(function(data){
                    monthly_page_chart(data);
                    $("#monthly-page-data").html(draw_table(data,'เดือน','หน้า'))
                })
                .fail(function(){});  
        
               
    }
function monthly_page_chart(response) {
        Highcharts.chart('monthly-page', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'รายงานเข้าชมเว็บไซต์รายเดือนประจำปี '+$('.monthly-year').val()
            },
            subtitle: {
                text: 'แบ่งแยกตามหน้าเข้าชม'
            },
            xAxis: {
                categories: ['มค.', 'กพ.', 'มีค.', 'มย.', 'พค.', 'มิย.', 'กค.', 'สค.', 'กย.', 'ตค.', 'พย.', 'ธค.'],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'จำนวนหน้าเข้าชม'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    },
                    formatter: function () {
                        if ( this.total == 0 ) { 
                            return "";
                        } else {
                            return this.total;
                        }
                    }                    
                },
                
            },
            tooltip: {
                headerFormat: '<b>เดือน {point.x}</b><br/>',
                pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                    style: {
                        fontWeight: 'normal',
                    },                         
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                },               
            },
            series: response.series
        });
    }  

$(window).on("load", function () {
    
})

$(window).on("scroll", function () {
   
})

"use strict";

$(document).ready(function () {
    
  // Make the dashboard widgets sortable Using jquery UI
  $('.connectedSortable').sortable({
    placeholder         : 'sort-highlight',
    connectWith         : '.connectedSortable',
    handle              : '.box-header, .nav-tabs',
    forcePlaceholderSize: true,
    zIndex              : 999999
  });
  $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');    
    
var data = [
    ['th-ct', 0],
    ['th-4255', 1],
    ['th-pg', 0],
    ['th-st', 3],
    ['th-kr', 4],
    ['th-sa', 5],
    ['th-tg', 6],
    ['th-tt', 7],
    ['th-pl', 8],
    ['th-ps', 9],
    ['th-kp', 10],
    ['th-pc', 11],
    ['th-sh', 12],
    ['th-at', 13],
    ['th-lb', 14],
    ['th-pa', 15],
    ['th-np', 16],
    ['th-sb', 17],
    ['th-cn', 18],
    ['th-bm', 19],
    ['th-pt', 20],
    ['th-no', 21],
    ['th-sp', 22],
    ['th-ss', 23],
    ['th-sm', 24],
    ['th-pe', 25],
    ['th-cc', 26],
    ['th-nn', 27],
    ['th-cb', 28],
    ['th-br', 29],
    ['th-kk', 30],
    ['th-ph', 31],
    ['th-kl', 32],
    ['th-sr', 33],
    ['th-nr', 34],
    ['th-si', 35],
    ['th-re', 36],
    ['th-le', 37],
    ['th-nk', 38],
    ['th-ac', 39],
    ['th-md', 40],
    ['th-sn', 41],
    ['th-nw', 42],
    ['th-pi', 43],
    ['th-rn', 44],
    ['th-nt', 45],
    ['th-sg', 46],
    ['th-pr', 47],
    ['th-py', 48],
    ['th-so', 49],
    ['th-ud', 50],
    ['th-kn', 51],
    ['th-tk', 52],
    ['th-ut', 53],
    ['th-ns', 54],
    ['th-pk', 55],
    ['th-ur', 56],
    ['th-sk', 57],
    ['th-ry', 58],
    ['th-cy', 59],
    ['th-su', 60],
    ['th-nf', 61],
    ['th-bk', 62],
    ['th-mh', 63],
    ['th-pu', 64],
    ['th-cp', 65],
    ['th-yl', 66],
    ['th-cr', 67],
    ['th-cm', 68],
    ['th-ln', 69],
    ['th-na', 70],
    ['th-lg', 71],
    ['th-pb', 0],
    ['th-rt', 73],
    ['th-ys', 74],
    ['th-ms', 0],
    ['th-un', 0],
    ['th-nb', 77]
];
var pieColors = (function () {
    var colors = [],
        base = Highcharts.getOptions().colors[0],
        i;

    for (i = 0; i < 10; i += 1) {
        // Start out with a darkened base color (negative brighten), and end
        // up with a much brighter color
        colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
    }
    return colors;
}());

Highcharts.mapChart('chart_map_volumn', {
    chart: {
        map: 'countries/th/th-all',
    },
    title: {
        text: 'ผลผลิตการเกษตรปี 2560/61'
    },
    subtitle: {
        text: 'โรงพยาบาลพระรามเก้า กระทรวงเกษตรและสหกรณ์'
    },
    mapNavigation: {
        enabled: true,
        buttonOptions: {
            verticalAlign: 'bottom'
        }
    },
    colorAxis: {
            min: 1,
            minColor: '#e2f2e1',
            maxColor: '#065602',
    },
    series: [{
        data: data,
        name: 'Random data',
        color: '#ccc',
        states: {
            hover: {
                color: '#BADA55'
            }
        },
        dataLabels: {
            enabled: true,
            format: '{point.name}'
        }
    }]
});

Highcharts.chart('chart_volumn', {
    chart: {
        type: 'area'
    },
    title: {
        text: 'ปริมาณผลผลิตการเกษตร'
    },
    subtitle: {
        text: 'โรงพยาบาลพระรามเก้า กระทรวงเกษตรและสหกรณ์'
    },
    xAxis: {
        allowDecimals: false,
        labels: {
            formatter: function () {
                return this.value; // clean, unformatted number for year
            }
        }
    },
    yAxis: {
        max: 25000,
        min: 22000,
        title: {
            text: 'ปริมาณพันตัน'
        },
        labels: {
            formatter: function () {
                return this.value / 1000 + 'k';
            }
        }
    },
    tooltip: {
        pointFormat: '{series.name} ปริมาณผลผลิต <b>{point.y:,.0f}</b>'
    },
    plotOptions: {
        area: {
            pointStart: 2007,
            marker: {
                enabled: false,
                symbol: 'circle',
                radius: 2,
                states: {
                    hover: {
                        enabled: true
                    }
                }
            }
        }
    },
    series: [{
        name: 'ผลผลิตการเกษตร',
        data: [
            23722, 23826, 24605,24304, 23464, 23708, 24099, 24357, 24237, 24401
        ]
    }]
});

// Build the chart
Highcharts.chart('chart_pie', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'ผลผลิตการเกษตรปี 2561'
    },
    subtitle: {
        text: 'โรงพยาบาลพระรามเก้า กระทรวงเกษตรและสหกรณ์'
    },    
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            colors: pieColors,
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                distance: -50,
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 4
                }
            }
        }
    },
    series: [{
        name: 'Share',
        data: [
            { name: 'พ๊ชผล', y: 61.41 },
            { name: 'ปศุสัตว์', y: 11.84 },
            { name: 'ประมง', y: 5.85 },
        ]
    }]
});

})



$(window).on("load", function () {
    
})

$(window).on("scroll", function () {
   
})

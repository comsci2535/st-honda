"use strict";

$(document).ready(function () {
    
    $('input#filter-category').keyup(function () {
        var that = this, $allListElements = $('ul.filter-category > li');
        var $matchingListElements = $allListElements.filter(function (i, li) {
            var listItemText = $(li).text().toUpperCase(), searchText = that.value.toUpperCase();
            return ~listItemText.indexOf(searchText);
        });
        $allListElements.hide();
        $matchingListElements.show();
    });
    
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

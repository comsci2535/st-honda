"use strict";
//console.log("It's work!");

$(document).ready(function () {
    
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideDown("400");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).slideUp("400");
            $(this).toggleClass('open');       
        }
    );

    var data = [
        ['th-ct', 0],
        ['th-4255', 1],
        ['th-pg', 0],
        ['th-st', 3],
        ['th-kr', 4],
        ['th-sa', 5],
        ['th-tg', 6],
        ['th-tt', 7],
        ['th-pl', 8],
        ['th-ps', 9],
        ['th-kp', 10],
        ['th-pc', 11],
        ['th-sh', 12],
        ['th-at', 13],
        ['th-lb', 14],
        ['th-pa', 15],
        ['th-np', 16],
        ['th-sb', 17],
        ['th-cn', 18],
        ['th-bm', 19],
        ['th-pt', 20],
        ['th-no', 21],
        ['th-sp', 22],
        ['th-ss', 23],
        ['th-sm', 24],
        ['th-pe', 25],
        ['th-cc', 26],
        ['th-nn', 27],
        ['th-cb', 28],
        ['th-br', 29],
        ['th-kk', 30],
        ['th-ph', 31],
        ['th-kl', 32],
        ['th-sr', 33],
        ['th-nr', 34],
        ['th-si', 35],
        ['th-re', 36],
        ['th-le', 37],
        ['th-nk', 38],
        ['th-ac', 39],
        ['th-md', 40],
        ['th-sn', 41],
        ['th-nw', 42],
        ['th-pi', 43],
        ['th-rn', 44],
        ['th-nt', 45],
        ['th-sg', 46],
        ['th-pr', 47],
        ['th-py', 48],
        ['th-so', 49],
        ['th-ud', 50],
        ['th-kn', 51],
        ['th-tk', 52],
        ['th-ut', 53],
        ['th-ns', 54],
        ['th-pk', 55],
        ['th-ur', 56],
        ['th-sk', 57],
        ['th-ry', 58],
        ['th-cy', 59],
        ['th-su', 60],
        ['th-nf', 61],
        ['th-bk', 62],
        ['th-mh', 63],
        ['th-pu', 64],
        ['th-cp', 65],
        ['th-yl', 66],
        ['th-cr', 67],
        ['th-cm', 68],
        ['th-ln', 69],
        ['th-na', 70],
        ['th-lg', 71],
        ['th-pb', 0],
        ['th-rt', 73],
        ['th-ys', 74],
        ['th-ms', 0],
        ['th-un', 0],
        ['th-nb', 77]
    ];
    var pieColors = (function () {
        var colors = [],
            base = Highcharts.getOptions().colors[0],
            i;

        for (i = 0; i < 10; i += 1) {
            // Start out with a darkened base color (negative brighten), and end
            // up with a much brighter color
            colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
        }
        return colors;
    }());

    Highcharts.mapChart('chart_map_volumn', {
        chart: {
            map: 'countries/th/th-all',
        },
        title: {
            text: 'แผนที่แสดงจังหวัดแหล่งผลิตลำไยรายภาค ปี 2561'
        },
        subtitle: {
            text: 'โรงพยาบาลพระรามเก้า กระทรวงเกษตรและสหกรณ์'
        },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },
        colorAxis: {
                min: 1,
                minColor: '#e2f2e1',
                maxColor: '#065602',
        },
        series: [{
            data: data,
            name: 'Random data',
            color: '#ccc',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }]
    });



    Highcharts.chart('container', {
      chart: {
        type: 'bar'
      },
       colors: ['#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263',      '#6AF9C4'],
      title: {
        text: 'สถิติการผลิตลำไยของไทย ปี 2559 - 2561'
      },
      xAxis: {
        categories: [
        'ครัวเรือน (ล้านครัวเรือน)',
         'เนื้อที่ให้ผล (ล้านไร่)', 
         'ผลผลิตลำไย (ล้านตัน)', 
         'ผลผลิตต่อไร่ (พัน กก./ไร่)', 
         'ต้นทุนรวมต่อไร (บาท)',
         'ต้นทุนรวม (บาท/กก.)',
         'ราคา ณ ไร่นา ลำไยสดทั้งช่อ A (บาท/กก.)'
         ]
      },
       yAxis: {
        min: 0,
        title: {
            text: '',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' '
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
      series: [{
        name: '2559',
        data: [2.31, 1.05, 0.76, 0.719, 6,13.20,33.24]
      }, {
        name: '2560',
        data: [ 0,1.09, 1.02, 0.932, 7,10.70,22.35]
      }, {
        name: '2561',
        data: [ 0,1.14, 1.06, 0.927, 7,0,29.38]
      }]
    });


    Highcharts.chart('container2', {
            chart: {
                type: 'column'
            },
           
            title: {
                text: 'มูลค่าการส่งออกลำไยสดและผลิตภัณฑ์ ปี 2560'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'ล้านบาท'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.1f}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}</b> ล้านบาท<br/>'
            },

            "series": [
                {
                    "name": "ประเภท",
                    "colorByPoint": true,
                    "data": [
                        {
                            "name": "ลำไยสด",
                            "y": 20970.39,
                            "drilldown": "ลำไยสด"
                        },
                        {
                            "name": "ลำไยอบแห้ง",
                            "y": 11104.55,
                            "drilldown": "ลำไยอบแห้ง"
                        },
                        {
                            "name": "ลำไยบรรจุภาชนะอัดลม",
                            "y": 753.21,
                            "drilldown": "ลำไยบรรจุภาชนะอัดลม"
                        },
                        {
                            "name": "ลำไยแช่แข็ง",
                            "y": 2.35,
                            "drilldown": "ลำไยแช่แข็ง"
                        }
                    ]
                }
            ],
            "drilldown": {
                "series": [
                    {
                        "name": "ลำไยสด",
                        "id": "ลำไยสด",
                        "data": [
                            [
                                "เวียดนาม",
                                11679.78
                            ],
                            [
                                "จีน",
                                5225.50
                            ],
                            [
                                "อินโดเนเซีย",
                                2826.06
                            ],
                            [
                                "ฮ่องกง",
                                767.33
                            ],
                            [
                                "มาเลเซีย",
                                195.91
                            ]
                        ]
                    },
                    {
                        "name": "ลำไยอบแห้ง",
                        "id": "ลำไยอบแห้ง",
                        "data": [
                            [
                                "เวียดนาม",
                                7534.79
                            ],
                            [
                                "จีน",
                                2954.56
                            ],
                            [
                                "เมียนมา",
                                231.72
                            ],
                            [
                                "ฮ่องกง",
                                143.25
                            ],
                            [
                                "อินโดเนเซีย",
                                41.72
                            ]
                        ]
                    },
                    {
                        "name": "ลำไยบรรจุภาชนะอัดลม",
                        "id": "ลำไยบรรจุภาชนะอัดลม",
                        "data": [
                            [
                                "มาเลเซีย",
                                214.58
                            ],
                            [
                                "เวียดนาม",
                                187.17
                            ],
                            [
                                "สิงค์โปร์",
                                101.07
                            ],
                            [
                                "อินโดเนเซีย",
                                85.73
                            ],
                            [
                                "สหรัฐอเมริกา",
                                77.10
                            ]
                        ]
                    },
                    {
                        "name": "ลำไยแช่แข็ง",
                        "id": "ลำไยแช่แข็ง",
                        "data": [
                            [
                                "ฮ่องกง",
                                1.47
                            ],
                            [
                                "ญี่ปุ่น",
                                0.79
                            ],
                            [
                                "มาเก๊า",
                                0.09
                            ]
                        ]
                    }
                ]
            }
        });

        Highcharts.chart('container3', {

            chart: {
                type: 'column'
            },
            colors: ['#24CBE5', '#64E572', '#FF9655'],
            title: {
                text: 'แสดงเนื้อที่ให้ผลและผลิตลำไย จำแนกรายภาค ปี 2561'
            },

            subtitle: {
                text: ''
            },

            legend: {
                align: 'right',
                verticalAlign: 'middle',
                layout: 'vertical'
            },

            xAxis: {
                categories: ['เนื้อที่ให้ผล (ไร่)', 'ผลผลิต (ตัน)'],
                labels: {
                    x: -10
                }
            },

            yAxis: {
                allowDecimals: false,
                title: {
                    text: 'Amount'
                }
            },
            series: [{
                name: 'ภาคเหนือ',
                data: [863395, 659266]
            }, {
                name: 'ภาคตะวันออกเฉียงเหนือ',
                data: [26631, 16293]
            }, {
                name: 'ภาคกลาง',
                data: [254926, 385763]
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            align: 'center',
                            verticalAlign: 'bottom',
                            layout: 'horizontal'
                        },
                        yAxis: {
                            labels: {
                                align: 'left',
                                x: 0,
                                y: -5
                            },
                            title: {
                                text: null
                            }
                        },
                        subtitle: {
                            text: null
                        },
                        credits: {
                            enabled: false
                        }
                    }
                }]
            }
        });

        Highcharts.chart('container4', {
            chart: {
                zoomType: 'xy'
            },
            title: {
                text: 'ผลผลิตและราคา รายเดือน'
            },
            subtitle: {
                text: ''
            },
            xAxis: [{
                categories: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
                crosshair: true
            }],
            yAxis: [{ // Primary yAxis
                labels: {
                    format: '{value} ร้อยละ',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                },
                title: {
                    text: 'Temperature',
                    style: {
                        color: Highcharts.getOptions().colors[1]
                    }
                }
            }, { // Secondary yAxis
                title: {
                    text: 'Rainfall',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                labels: {
                    format: '{value} ร้อยละ',
                    style: {
                        color: Highcharts.getOptions().colors[0]
                    }
                },
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 120,
                verticalAlign: 'top',
                y: 100,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            series: [{
                name: 'Rainfall',
                type: 'column',
                yAxis: 1,
                data: [13.30,9.63,5.63,2.71,1.47,0.76,10.53 ,24.84,4.05,3.94,9.12,14.02],
                tooltip: {
                    valueSuffix: ' %'
                }

            }, {
                name: 'Temperature',
                type: 'spline',
                data: [13.30,9.63,5.63,2.71,1.47,0.76,10.53 ,24.84,4.05,3.94,9.12,14.02],
                tooltip: {
                    valueSuffix: '%'
                }
            }]
        });
  
})

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

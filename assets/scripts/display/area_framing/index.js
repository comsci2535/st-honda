"use strict";
$(document).ready(function () {

});



function get_region(obj)
{
  //  var pieChartCanvas1 = $('#pieChart-1').get(0).getContext('2d'); 
    var type = obj.val();
    $.get(siteUrl + "display/area_framing/region?year=2014&type="+type, function (data) {
        console.log(data.series);
       
        
        Highcharts.chart('container', {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45
                }
            },
            title: {
                text: ''
            },
            // subtitle: {
            //     text: '3D donut in Highcharts'
            // },
            plotOptions: {
                pie: {
                    innerSize: 100,
                    depth: 45
                }
            },
            series: [{
                name: 'ขนาดฟาร์ม',
                data: data.series
            }]
        });
    });

    
}
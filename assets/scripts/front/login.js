"use strict";
$(document).ajaxStart(function() {
    $("i.fa").removeClass('fa-sign-in').addClass('fa-circle-o-notch fa-spin')
});
$(document).ajaxStop(function() {
    $("i.fa").removeClass('fa-circle-o-notch fa-spin').addClass('fa-sign-in')
});

$(document).ready(function () {
    console.log('login');
    $.validator.setDefaults({
        highlight: function (element) {
            $(element).addClass('has-error');
            $(element).parent('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).removeClass('has-error');
            $(element).parent('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'help-block',
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        }
    });

    $.validator.addMethod('isEmail', function (value, element) {
        return this.optional(element) || /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value);
    }, 'โปรดระบุรูปแบบอีเมล์ที่ถูกต้อง');

    $('#frm-login').validate({
        rules: {
            email: {required: true, isEmail: true},
            password: {required: true}
        },
        submitHandler: function (form) {
            $("i.fa").removeClass('fa-sign-in')
            $(".login-error").removeClass('animated bounceIn')
            $(".login-error").addClass('hidden')
            var message
            $(form).ajaxSubmit({
                success: function (res) {
                    console.log(res);
                    if (res.error) {
                        message = res.message;
                        $(".login-error").html(message)
                        $(".login-error").removeClass('hidden')
                        $(".login-error").addClass('animated bounceIn')
                    } else {
                        window.location.href = res.message
                    }
                },
                error: function () {
                    message = "Contection error!"
                    $(".login-error").html(message)
                    $(".login-error").removeClass('hidden')
                    $(".login-error").addClass('animated bounceIn')
                }
            })
        }
    });

    $('#frm-register').validate({
        rules: {
            email: {required: true, isEmail: true},
            password: {required: true},
            rePassword: {required: true, equalTo: '#password'}
        }
    });

    $('#frm-forgot').validate({
        rules: {
            email: {required: true, isEmail: true},
        }
    });

   
});

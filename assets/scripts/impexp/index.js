"use strict";


$(document).ready(function () {
    
    var a = get_quantity();
    var b = get_value();
  
  
})

function get_quantity()
{
  
    $.get(siteUrl + "impexp/get_quantity", function (data) {
        console.log(data);

        Highcharts.chart('chart_price_index', {
            chart: {
        //        backgroundColor:'rgba(255, 255, 255, 0.65)'
            },
            title: {
                text: 'ภาพรวมปริมาณนำเข้า-ส่งออก'
            },

            subtitle: {
                text: 'โรงพยาบาลพระรามเก้า กระทรวงเกษตรและสหกรณ์'
            },

            yAxis: {
                title: {
                    text: 'จำนวน(ล้านตัน)'
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: data.quantity_start_y
                }
            },

            series: [{
                name: 'นำเข้า',
                color: '#00a65a',
                data: data.quantity_imp
            }, {
                name: 'ส่งออก',
                color: '#dd4b39',
                data: data.quantity_exp
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });  
    });
}

function get_value()
{
  
    $.get(siteUrl + "impexp/get_value", function (data) {
        console.log(data);

        Highcharts.chart('chart_product_index', {
            chart: {
        //        backgroundColor:'rgba(255, 255, 255, 0.65)'
            },
            title: {
                text: 'ภาพรวมมูลค่านำเข้า-ส่งออก'
            },

            subtitle: {
                text: 'โรงพยาบาลพระรามเก้า กระทรวงเกษตรและสหกรณ์'
            },

            yAxis: {
                title: {
                    text: 'ราคา(ล้านบาท)'
                }
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'bottom'
            },

            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    pointStart: data.value_start_y
                }
            },

            series: [{
                name: 'นำเข้า',
                color: '#00a65a',
                data: data.value_imp
            }, {
                name: 'ส่งออก',
                color: '#dd4b39',
                data: data.value_exp
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }

        });  
    });
}

$(window).on("load", function () {
})

$(window).on("scroll", function () {
})

$(document).ready(function() {
	$('.fancybox').fancybox();

	initColorbox();

	function initColorbox()
	{
		if($('.gallery_item').length)
		{
			$('.colorbox').colorbox(
			{
				rel:'colorbox',
				photo: true,
				maxWidth: '90%'
			});
		}
	}
});



$("#category").change(function(e){
  
    getContentHtml(0);
    $('#page').val(0);

});

$("#keyword").keyup(function(e){
   
    getContentHtml(0);
   $('#page').val(0);

});

$("#keyword-b").click(function(e){
   
    getContentHtml(0);
    $('#page').val(0);
});

$(document).ready(function(){
   getContentHtml(0);
   $('#page').val(0);
});

$("#views-more").click(function(e){
    var start = $('#page').val();
    var page = parseInt(start)+1;
    $('#page').val(page);
    getContent(page);
});

var getContent = function(page){
    
    // var category = $("#category").val();
    // var keyword = $("#keyword").val();
    // ,category:category,keyword:keyword
    //alert(category);
     $('#form-img-div').html('<img src="'+baseUrl+'assets/images/loading.gif" alt="loading" title="loading" style="display:inline" width="25">');
    $.ajax({
        url:siteUrl+"used_car/getContentMore/",
        type:'GET',
        data: {page:page}
    }).done(function(response){
        var obj = JSON.parse(response);
        $('#form-img-div').html('');
        $("#get_media").append(obj.text_); 
        //$("#get_media_all").html(obj.text2);
        if(obj.t==0){
            $('#load-next-posts').hide(100);
        }else{
            $('#load-next-posts').show(100);
        } 
    });
};

var getContentHtml = function(page){
    
    // var category = $("#category").val();
    // var keyword = $("#keyword").val();
    // ,category:category,keyword:keyword
    //alert(category);
   // $('#get_media_all').html('<img src="'+baseUrl+'assets/images/loading.gif" alt="loading" title="loading" style="display:inline" >');
    $.ajax({
        url:siteUrl+"used_car/getContentMore/",
        type:'GET',
        data: {page:page}
    }).done(function(response){
        var obj = JSON.parse(response);
        $('#form-img-div').html('');
        $("#get_media").html(obj.text_);
        //$("#get_media_all").html(obj.text2);
        if(obj.t==0){
            $('#load-next-posts').hide(100);
        }else{
            $('#load-next-posts').show(100);
        }
    });
};
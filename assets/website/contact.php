<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Honda Premier Phraeksa" />
	<?php include('include/meta.php'); ?>
	<title>ศูนย์บริการรถยนต์ฮอนด้าครบวงจร | ติดต่อเรา ฮอนด้าพรีเมียร์แพรกษา</title>

</head>

<body class="stretched">

	<div id="wrapper" class="clearfix">

		<header id="header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png" data-sticky-logo="images/logo-media.png" data-mobile-logo="images/logo-media.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png" data-sticky-logo="images/logo-media@2x.png" data-mobile-logo="images/logo-media@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div>
					<nav id="primary-menu">
						<ul>
							<li><a href="index.php"><div>ฮอนด้าพรีเมียร์</div></a></li>
							<li><a href="about.php"><div>เกี่ยวกับเรา</div></a></li>
							<li><a href="services.php"><div>บริการและอะไหล่</div></a></li>
							<li><a href="used-car.php"><div>รถยนต์มือสอง</div></a></li>
							<li><a href="news.php"><div>ข่าวสาร</div></a></li>
							<li class="current"><a href="contact.php"><div>ติดต่อเรา</div></a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>

		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>ติดต่อเรา</h1>
				<span>ฮอนด้าพรีเมียร์แพรกษา</span>
				<ol class="breadcrumb">
					<li><a href="#">หน้าแรก</a></li>
					<li class="active">ติดต่อเรา ฮอนด้าพรีเมียร์แพรกษา</li>
				</ol>
			</div>
		</section>

		<section class="gmap slider-parallax">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3878.567907591314!2d100.69064911482832!3d13.562077590470423!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x311d596bfff82229%3A0xf938a345cfce6015!2zSG9uZGEgUHJlbWllciBQaHJhZWtzYSDguK7guK3guJnguJTguYnguLIg4Lie4Lij4Li14LmA4Lih4Li14Lii4Lij4LmMIOC5geC4nuC4o-C4geC4qeC4sg!5e0!3m2!1sth!2sth!4v1546872443369" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</section>

		<section id="content">
			<div class="content-wrap">
				<div class="col_full common-height">
					<div class="col-md-4 dark col-padding ohidden" style="background-color: #1abc9c;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">ข่าวสาร</h3>
							<p style="line-height: 1.8;">กิจกรรมดีๆกับฮอนด้า พรีเมียร์ แพรกษา บอกเลยว่าอย่าพลาดกับกิจกรรมดีๆ พร้อมของรางวัลมากมาย ... แล้วมาพบกันครับ</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">ดูข่าวสารทั้งหมด</a>
							<i class="icon-bulb bgicon"></i>
						</div>
					</div>

					<div class="col-md-4 dark col-padding ohidden" style="background-color: #34495e;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">บริการและอะไหล่</h3>
							<p style="line-height: 1.8;">บริการซ่อมตัวถังและสี รอรับได้ใน 1 วัน<br>มั่นใจในบริการและการตรวจสอบคุณภาพ<font style="color:#c7161d;">"ตามมาตรฐานฮอนด้า"</font>
							</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">บริการและอะไหล่</a>
							<i class="icon-cog bgicon"></i>
						</div>
					</div>

					<div class="col-md-4 dark col-padding ohidden" style="background-color: #e74c3c;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">ติดต่อเรา</h3>
							<p>
								<a href="https://facebook.com/hondapremier/"><img data-animate="fadeInLeftBig" src="images/fblike.png" alt="เฟสบุ๊ค ฮอนด้าพรีเมียร์ แพรกษา"></a>
								<a href="#"><img data-animate="fadeInLeftBig" src="images/line_add.png" alt="ไลน์ ฮอนด้าพรีเมียร์ แพรกษา"></a>
								<a href="#"><img data-animate="fadeInLeftBig" src="images/instagram.png" alt="อินสตาแกรม ฮอนด้าพรีเมียร์ แพรกษา"></a>
							</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">ติดต่อเรา</a>
							<i class="icon-thumbs-up bgicon"></i>
						</div>
					</div>

					<div class="clear"></div>
				</div>
			</div>
		</section>

		<?php include('view/footer.php'); ?>

	</div>

	<?php include('include/script.php'); ?>

</body>
</html>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Honda Premier Phraeksa" />
	<?php include('include/meta.php'); ?>
	<title>ศูนย์บริการรถยนต์ฮอนด้าครบวงจร | ข่าวสาร ฮอนด้าพรีเมียร์แพรกษา</title>

</head>

<body class="stretched">

	<div id="wrapper" class="clearfix">

		<header id="header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png" data-sticky-logo="images/logo-media.png" data-mobile-logo="images/logo-media.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png" data-sticky-logo="images/logo-media@2x.png" data-mobile-logo="images/logo-media@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div>
					<nav id="primary-menu">
						<ul>
							<li><a href="index.php"><div>ฮอนด้าพรีเมียร์</div></a></li>
							<li><a href="about.php"><div>เกี่ยวกับเรา</div></a></li>
							<li class="current"><a href="services.php"><div>บริการและอะไหล่</div></a></li>
							<li><a href="used-car.php"><div>รถยนต์มือสอง</div></a></li>
							<li><a href="news.php"><div>ข่าวสาร</div></a></li>
							<li><a href="contact.php"><div>ติดต่อเรา</div></a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>

		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>บริการและอะไหล่</h1>
				<span>ฮอนด้าพรีเมียร์แพรกษา</span>
				<ol class="breadcrumb">
					<li><a href="#">หน้าแรก</a></li>
					<li class="active">บริการและอะไหล่ ฮอนด้าพรีเมียร์แพรกษา</li>
				</ol>
			</div>
		</section>

		<section id="content">
			<div class="content-wrap">
				<div class="container clearfix">
					<div class="container clearfix">

						<div style="padding-top:40px;">
							<div id="posts" class="post-grid grid-container post-masonry clearfix">

								<div class="entry clearfix">
									<div class="entry-image">
										<a href="images/about/1.jpg" data-lightbox="image"><img class="image_fade" src="images/about/1.jpg" alt="Standard Post with Image"></a>
									</div>
									<div class="entry-title">
										<h2><a href="blog-single.html">ฮอนด้าพรีเมียร์แพรกษา</a></h2>
									</div>
									<ul class="entry-meta clearfix">
										<li><i class="icon-calendar3"></i> 10th Feb 2014</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 13</a></li>
										<li><a href="#"><i class="icon-camera-retro"></i></a></li>
									</ul>
									<div class="entry-content">
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate, asperiores quod est tenetur in. Eligendi, deserunt, blanditiis est quisquam doloribus.</p>
										<a href="blog-single.html"class="more-link">Read More</a>
									</div>
								</div>

							   <div class="entry clearfix">
									<div class="entry-image">
										<iframe src="http://player.vimeo.com/video/87701971" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
									</div>
									<div class="entry-title">
										<h2><a href="blog-single-full.html">ฮอนด้าพรีเมียร์แพรกษา</a></h2>
									</div>
									<ul class="entry-meta clearfix">
										<li><i class="icon-calendar3"></i> 16th Feb 2014</li>
										<li><a href="blog-single-full.html#comments"><i class="icon-comments"></i> 19</a></li>
										<li><a href="#"><i class="icon-film"></i></a></li>
									</ul>
									<div class="entry-content">
										<p>Asperiores, tenetur, blanditiis, quaerat odit ex exercitationem pariatur quibusdam veritatis quisquam laboriosam esse beatae hic perferendis velit deserunt!</p>
										<a href="blog-single-full.html"class="more-link">Read More</a>
									</div>
								</div>

								<div class="entry clearfix">
								 <div class="entry-image">
									 <iframe src="http://player.vimeo.com/video/87701971" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
								 </div>
								 <div class="entry-title">
									 <h2><a href="blog-single-full.html">ฮอนด้าพรีเมียร์แพรกษา</a></h2>
								 </div>
								 <ul class="entry-meta clearfix">
									 <li><i class="icon-calendar3"></i> 16th Feb 2014</li>
									 <li><a href="blog-single-full.html#comments"><i class="icon-comments"></i> 19</a></li>
									 <li><a href="#"><i class="icon-film"></i></a></li>
								 </ul>
								 <div class="entry-content">
									 <p>Asperiores, tenetur, blanditiis, quaerat odit ex exercitationem pariatur quibusdam veritatis quisquam laboriosam esse beatae hic perferendis velit deserunt!</p>
									 <a href="blog-single-full.html"class="more-link">Read More</a>
								 </div>
							 </div>

								<div class="entry clearfix">
									<div class="entry-image">
										<blockquote>
											<p>"When you are courting a nice girl an hour seems like a second. When you sit on a red-hot cinder a second seems like an hour. That's relativity."</p>
											<footer>Albert Einstein</footer>
										</blockquote>
									</div>
									<ul class="entry-meta clearfix">
										<li><i class="icon-calendar3"></i> 3rd Mar 2014</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 23</a></li>
										<li><a href="#"><i class="icon-quote-left"></i></a></li>
									</ul>
								</div>

								<div class="entry clearfix">
									<div class="entry-image">
										<blockquote>
											<p>"When you are courting a nice girl an hour seems like a second. When you sit on a red-hot cinder a second seems like an hour. That's relativity."</p>
											<footer>Albert Einstein</footer>
										</blockquote>
									</div>
									<ul class="entry-meta clearfix">
										<li><i class="icon-calendar3"></i> 3rd Mar 2014</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 23</a></li>
										<li><a href="#"><i class="icon-quote-left"></i></a></li>
									</ul>
								</div>

								<div class="entry clearfix">
									<div class="entry-image">
										<a href="https://www.sakidlocode.com" class="entry-link" target="_blank">
											HONDA PREMIER PHRAEKSA
											<span>- https://www.sakidlocode.com</span>
										</a>
									</div>
									<ul class="entry-meta clearfix">
										<li><i class="icon-calendar3"></i> 17th Mar 2014</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 26</a></li>
										<li><a href="#"><i class="icon-link"></i></a></li>
									</ul>
								</div>

								<div class="entry clearfix">
									<div class="entry-image">
										<div class="panel panel-default">
											<div class="panel-body">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia, fuga optio voluptatibus saepe tenetur aliquam debitis eos accusantium!
											</div>
										</div>
									</div>
									<ul class="entry-meta clearfix">
										<li><i class="icon-calendar3"></i> 21st Mar 2014</li>
										<li><a href="blog-single.html#comments"><i class="icon-comments"></i> 11</a></li>
										<li><a href="#"><i class="icon-align-justify2"></i></a></li>
									</ul>
								</div>

							</div>
						</div>

						<div id="load-next-posts" class="center">
							<a href="blog-masonry-page-2.html" class="button button-3d button-dark button-large button-rounded">Load more..</a>
						</div>


					</div>
				</div>

				<div class="col_full common-height">
					<div class="col-md-4 dark col-padding ohidden" style="background-color: #1abc9c;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">ข่าวสาร</h3>
							<p style="line-height: 1.8;">กิจกรรมดีๆกับฮอนด้า พรีเมียร์ แพรกษา บอกเลยว่าอย่าพลาดกับกิจกรรมดีๆ พร้อมของรางวัลมากมาย ... แล้วมาพบกันครับ</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">ดูข่าวสารทั้งหมด</a>
							<i class="icon-bulb bgicon"></i>
						</div>
					</div>
					<div class="col-md-4 dark col-padding ohidden" style="background-color: #34495e;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">บริการและอะไหล่</h3>
							<p style="line-height: 1.8;">บริการซ่อมตัวถังและสี รอรับได้ใน 1 วัน<br>มั่นใจในบริการและการตรวจสอบคุณภาพ<font style="color:#c7161d;">"ตามมาตรฐานฮอนด้า"</font>
							</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">บริการและอะไหล่</a>
							<i class="icon-cog bgicon"></i>
						</div>
					</div>
					<div class="col-md-4 dark col-padding ohidden" style="background-color: #e74c3c;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">ติดต่อเรา</h3>
							<p>
								<a href="https://facebook.com/hondapremier/"><img data-animate="fadeInLeftBig" src="images/fblike.png" alt="เฟสบุ๊ค ฮอนด้าพรีเมียร์ แพรกษา"></a>
								<a href="#"><img data-animate="fadeInLeftBig" src="images/line_add.png" alt="ไลน์ ฮอนด้าพรีเมียร์ แพรกษา"></a>
								<a href="#"><img data-animate="fadeInLeftBig" src="images/instagram.png" alt="อินสตาแกรม ฮอนด้าพรีเมียร์ แพรกษา"></a>
							</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">ติดต่อเรา</a>
							<i class="icon-thumbs-up bgicon"></i>
						</div>
					</div>
					<div class="clear"></div>
				</div>

			</div>
		</section>

		<?php include('view/footer.php'); ?>

	</div>

	<?php include('include/script.php'); ?>
	<script type="text/javascript">

		jQuery(window).load(function(){

			var $container = $('#posts');

			$container.infinitescroll({
				loading: {
					finishedMsg: '<i class="icon-line-check"></i>',
					msgText: '<i class="icon-line-loader icon-spin"></i>',
					img: "images/preloader-dark.gif",
					speed: 'normal'
				},
				state: {
					isDone: false
				},
				nextSelector: "#load-next-posts a",
				navSelector: "#load-next-posts",
				itemSelector: "div.entry"
			},
			function( newElements ) {
				$container.isotope( 'appended', $( newElements ) );
				var t = setTimeout( function(){ $container.isotope('layout'); }, 2000 );
				SEMICOLON.initialize.resizeVideos();
				SEMICOLON.widget.loadFlexSlider();
				SEMICOLON.widget.masonryThumbs();
			});

		});
	</script>

</body>
</html>

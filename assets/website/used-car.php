<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Honda Premier Phraeksa" />
	<?php include('include/meta.php'); ?>
	<title>ศูนย์บริการรถยนต์ฮอนด้าครบวงจร | รถยนต์มือสอง ฮอนด้าพรีเมียร์แพรกษา</title>

</head>

<body class="stretched">

	<div id="wrapper" class="clearfix">

		<header id="header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
					<div id="logo">
						<a href="index.php" class="standard-logo" data-dark-logo="images/logo-dark.png" data-sticky-logo="images/logo-media.png" data-mobile-logo="images/logo-media.png"><img src="images/logo.png" alt="Canvas Logo"></a>
						<a href="index.php" class="retina-logo" data-dark-logo="images/logo-dark@2x.png" data-sticky-logo="images/logo-media@2x.png" data-mobile-logo="images/logo-media@2x.png"><img src="images/logo@2x.png" alt="Canvas Logo"></a>
					</div>
					<nav id="primary-menu">
						<ul>
							<li><a href="index.php"><div>ฮอนด้าพรีเมียร์</div></a></li>
							<li><a href="about.php"><div>เกี่ยวกับเรา</div></a></li>
							<li><a href="services.php"><div>บริการและอะไหล่</div></a></li>
							<li class="current"><a href="used-car.php"><div>รถยนต์มือสอง</div></a></li>
							<li><a href="news.php"><div>ข่าวสาร</div></a></li>
							<li><a href="contact.php"><div>ติดต่อเรา</div></a></li>
						</ul>
					</nav>
				</div>
			</div>
		</header>

		<section id="page-title" class="page-title-mini">
			<div class="container clearfix">
				<h1>รถยนต์มือสอง</h1>
				<span>ฮอนด้าพรีเมียร์แพรกษา</span>
				<ol class="breadcrumb">
					<li><a href="#">หน้าแรก</a></li>
					<li class="active">รถยนต์มือสอง ฮอนด้าพรีเมียร์แพรกษา</li>
				</ol>
			</div>
		</section>

		<section id="content">

			<div class="content-wrap">

				<div class="col_full common-height">
					<div class="container clearfix">
						<div class="heading-block">
							<h2 style="padding-top:30px;">รุ่นรถ <span>ฮอนด้าพรีเมียร์</span> <font style="color:#c7161d;">แพรกษา</font></h2>
							<img src="images/car.jpg" alt="รุ่นรถ ฮอนด้าพรีเมียร์ แพรกษา">
						</div>
					</div>
				</div>

				<div class="col_full common-height">

					<div class="col-md-4 dark col-padding ohidden" style="background-color: #1abc9c;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">ข่าวสาร</h3>
							<p style="line-height: 1.8;">กิจกรรมดีๆกับฮอนด้า พรีเมียร์ แพรกษา บอกเลยว่าอย่าพลาดกับกิจกรรมดีๆ พร้อมของรางวัลมากมาย ... แล้วมาพบกันครับ</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">ดูข่าวสารทั้งหมด</a>
							<i class="icon-bulb bgicon"></i>
						</div>
					</div>

					<div class="col-md-4 dark col-padding ohidden" style="background-color: #34495e;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">บริการและอะไหล่</h3>
							<p style="line-height: 1.8;">บริการซ่อมตัวถังและสี รอรับได้ใน 1 วัน<br>มั่นใจในบริการและการตรวจสอบคุณภาพ<font style="color:#c7161d;">"ตามมาตรฐานฮอนด้า"</font>
							</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">บริการและอะไหล่</a>
							<i class="icon-cog bgicon"></i>
						</div>
					</div>

					<div class="col-md-4 dark col-padding ohidden" style="background-color: #e74c3c;">
						<div>
							<h3 class="uppercase" style="font-weight: 600;">ติดต่อเรา</h3>
							<p>
								<a href="https://facebook.com/hondapremier/"><img data-animate="fadeInLeftBig" src="images/fblike.png" alt="เฟสบุ๊ค ฮอนด้าพรีเมียร์ แพรกษา"></a>
								<a href="#"><img data-animate="fadeInLeftBig" src="images/line_add.png" alt="ไลน์ ฮอนด้าพรีเมียร์ แพรกษา"></a>
								<a href="#"><img data-animate="fadeInLeftBig" src="images/instagram.png" alt="อินสตาแกรม ฮอนด้าพรีเมียร์ แพรกษา"></a>
							</p>
							<a href="#" class="button button-border button-light button-rounded uppercase nomargin">ติดต่อเรา</a>
							<i class="icon-thumbs-up bgicon"></i>
						</div>
					</div>

					<div class="clear"></div>

				</div>

			</div>

		</section>

		<?php include('view/footer.php'); ?>

	</div>

	<?php include('include/script.php'); ?>

</body>
</html>

<footer id="footer" class="dark">
  <div id="copyrights">
    <div class="container clearfix">
      <div class="col_half">
        Copyrights &copy; <?php echo date("Y"); ?> All Rights Reserved by HONDY PREMIER PHRAEKSA<br>
        <i class="icon-envelope2"></i> hondapremier@gmail.com <span class="middot">&middot;</span> <i class="icon-headphones"></i> 02 136 2155 <span class="middot">&middot;</span> <i class="icon-add"></i> @hondapremier
      </div>
      <div class="col_half col_last tright">
        <div class="fright clearfix">
          <a href="https://facebook.com/hondapremier/"><img src="images/fblike.png" alt="เฟสบุ๊ค ฮอนด้าพรีเมียร์ แพรกษา"></a>
          <a href="#"><img src="images/line_add.png" alt="ไลน์ ฮอนด้าพรีเมียร์ แพรกษา"></a>
          <a href="#"><img src="images/instagram.png" alt="อินสตาแกรม ฮอนด้าพรีเมียร์ แพรกษา"></a>
        </div>

        <div class="clear"></div>
      </div>
    </div>
  </div>
</footer>

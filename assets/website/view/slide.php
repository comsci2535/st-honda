<section id="slider" class="slider-parallax swiper_wrapper clearfix">
  <div class="slider-parallax-inner">
    <div class="swiper-container swiper-parent">
      <div class="swiper-wrapper">
        <div class="swiper-slide dark" style="background-image: url('images/slider/1.jpg');"></div>
        <div class="swiper-slide dark" style="background-image: url('images/slider/2.jpg');"></div>
        <div class="swiper-slide" style="background-image: url('images/slider/3.jpg'); background-position: center top;"></div>
      </div>
      <div id="slider-arrow-left"><i class="icon-angle-left"></i></div>
      <div id="slider-arrow-right"><i class="icon-angle-right"></i></div>
      <div id="slide-number"><div id="slide-number-current"></div><span>/</span><div id="slide-number-total"></div></div>
    </div>
  </div>
</section>
